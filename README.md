# LEIA-ME #

Testes automatizados não foram implementados, caso seja do interesse fico a disposição.

Quanto aos critérios:
Segue um teste para que seja realizado por você como parte do processo seletivo a vaga de Analista – desenvolvedor.
 
Desenvolver uma aplicação de “Blog” aonde o usuário possa fazer posts e comentar os posts.
Utilizando a stack do Spring (Spring Data e Spring Boot) e Angular.
--
Todos foram atendidos, ficando pendentes alguns tratamentos que eu considero importante mas não foram realizados devido ao tempo disponível.
São eles:
* Autenticação OAuth2 (Criei uma fake sem criptografia que é inteira validada no client)
* Interceptação das mensagens de erro vindas do server
* Testes automatizados (Todos arquivos teste angular estão quebrados, no server, criei apenas um teste)

Qualquer duvida ou necessidade de alteração, ficarei a disposição.
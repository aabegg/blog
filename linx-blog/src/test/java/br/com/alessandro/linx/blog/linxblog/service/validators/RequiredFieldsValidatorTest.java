package br.com.alessandro.linx.blog.linxblog.service.validators;

import org.junit.Test;

public class RequiredFieldsValidatorTest {

	private RequiredFieldsValidator requiredFieldsValidator = new RequiredFieldsValidator();
	
	@Test(expected = RuntimeException.class)
	public void deveLancarErroQuandoValorNull() {
		requiredFieldsValidator
				.add(null, "Nome")
				.validate();
	}
	
	@Test(expected = RuntimeException.class)
	public void deveLancarErroQuandoValorStringVazia() {
		requiredFieldsValidator
				.add("", "Nome")
				.validate();
	}
	
	@Test
	public void naoDeveLancarErroQuandoValorInformado() {
		requiredFieldsValidator
				.add(" ", "Nome")
				.validate();
	}

}

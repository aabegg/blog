package br.com.alessandro.linx.blog.linxblog.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.repository.PostRepository;
import br.com.alessandro.linx.blog.linxblog.service.validators.PostValidator;

@Service
public class PostService {
	
	private PostRepository postRepository;
	
	@Autowired
	private PostValidator postValidator;
	
	public PostService(PostRepository postRepository) {
		this.postRepository = postRepository;
	}

	public void update(Post post) {
		postValidator.validateUpdate(post);
		
		postRepository.findOneById(post.getId()).ifPresent(managed -> {
			managed.setBody(post.getBody());
			managed.incrementNumberOfEdits();
			managed.setTitle(post.getTitle());
		});
	}

	public Post insert(Post post) {
		post.setDatePost(new Date());
		
		postValidator.validateInsert(post);
		
		return postRepository.save(post);
	}

	@Bean
	public PostValidator getPostValidator() {
		return new PostValidator();
	}
}

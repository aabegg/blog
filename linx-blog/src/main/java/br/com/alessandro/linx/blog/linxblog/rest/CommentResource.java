package br.com.alessandro.linx.blog.linxblog.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.alessandro.linx.blog.linxblog.model.Comment;
import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.model.builder.CommentBuilder;
import br.com.alessandro.linx.blog.linxblog.repository.CommentRespository;
import br.com.alessandro.linx.blog.linxblog.repository.PostRepository;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;
import br.com.alessandro.linx.blog.linxblog.rest.dto.CommentDTO;
import br.com.alessandro.linx.blog.linxblog.rest.util.PaginationUtil;
import br.com.alessandro.linx.blog.linxblog.service.CommentService;

@RestController
@Transactional
@RequestMapping("/post/{idPost}/comment")
public class CommentResource {

	private final CommentService commentService;
	
	private final CommentRespository commentRespository;
	
	private final UsersRepository usersRepository;
	
	private final PostRepository postRepository;
	
	public CommentResource(CommentService commentService, CommentRespository commentRespository, 
			UsersRepository usersRepository, PostRepository postRepository) {
		this.commentService = commentService;
		this.commentRespository = commentRespository;
		this.usersRepository = usersRepository;
		this.postRepository = postRepository;
	}
	
	@GetMapping
	public ResponseEntity<List<CommentDTO>> all(@PathVariable Long idPost, Pageable pageable) {
		final Page<Comment> page = commentRespository.findAllByPostOrderByCommentDateDesc(new Post(idPost), pageable);
		
		HttpHeaders headers = PaginationUtil.generatePaginationHeader(page, "/api/user");
		
		List<CommentDTO> comments = page.getContent()
				.stream()
				.map(CommentDTO::new)
				.collect(Collectors.toList());
		
		return new ResponseEntity<>(comments, headers, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public @ResponseBody CommentDTO find(@PathVariable Long id, @PathVariable Long idComment) throws URISyntaxException {
		Comment comment = commentRespository.findOne(id);
		return new CommentDTO(comment);
	}
	
	@PutMapping("/{icommentd}")
	public void update(@PathVariable Long idPost, @PathVariable Long idComment, @Valid @RequestBody CommentDTO commentDTO) throws URISyntaxException {
		Comment comment = converDTO(idComment, commentDTO);
		comment.setId(idPost);
		
		commentService.update(comment);
	}
	
	@PostMapping
	public ResponseEntity<Comment> insert(@PathVariable Long idPost, @Valid @RequestBody CommentDTO commentDTO) throws URISyntaxException {
		Comment comment = converDTO(idPost, commentDTO);
		
		comment = commentService.insert(comment);
		
		return ResponseEntity
				.created(new URI("/comment"))
				.body(comment);
	}
	
	public Comment converDTO(Long idPost, CommentDTO commentDTO) {
		Long idUser = commentDTO.getIdUser();
		
		return new CommentBuilder()
				.withBody(commentDTO.getBody())
				.withCommentDate(commentDTO.getCommentDate())
				.withId(commentDTO.getId())
				.withUser(usersRepository.findOne(idUser))
				.withPost(postRepository.findOne(idPost))
				.build();
	}
}

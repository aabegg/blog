package br.com.alessandro.linx.blog.linxblog.model.builder;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Users;

public class UsersBuilder implements ModelBuilder<Users> {
	private Long id;

	private String name;

	private String lastName;

	private String login;

	private String password;

	private Date registerDate;

	private Boolean active;

	private String imageUrl;

	private String email;

	public UsersBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public UsersBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public UsersBuilder withLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public UsersBuilder withLogin(String login) {
		this.login = login;
		return this;
	}

	public UsersBuilder withPassword(String password) {
		this.password = password;
		return this;
	}

	public UsersBuilder withRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
		return this;
	}

	public UsersBuilder withActive(Boolean active) {
		this.active = active;
		return this;
	}

	public UsersBuilder withImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
		return this;
	}

	public UsersBuilder withEmail(String email) {
		this.email = email;
		return this;
	}
	
	@Override
	public Users build() {
		return new Users(id, name, lastName, login, password, registerDate, active, imageUrl, email);
	}
}

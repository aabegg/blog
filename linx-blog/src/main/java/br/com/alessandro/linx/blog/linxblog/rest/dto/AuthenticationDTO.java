package br.com.alessandro.linx.blog.linxblog.rest.dto;

public class AuthenticationDTO {
	private String user;
	
	private String password;
	
	public AuthenticationDTO() { }

	public AuthenticationDTO(String user, String password) {
		super();
		this.user = user;
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

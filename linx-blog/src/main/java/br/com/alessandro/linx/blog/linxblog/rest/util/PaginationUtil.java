package br.com.alessandro.linx.blog.linxblog.rest.util;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

public final class PaginationUtil {

    private PaginationUtil() { }

    public static HttpHeaders generatePaginationHeader(Page<?> page, String baseUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", Long.toString(page.getTotalElements()));

        String link = "";
        link += processLinkNextPage(page, baseUrl);
        link += processLinkPrevPage(page, baseUrl);
        link += processLinkLastPage(page, baseUrl);
        link += processLinkFirstPage(page, baseUrl);
        
        headers.add(HttpHeaders.LINK, link);
        
        return headers;
    }

	private static String processLinkNextPage(Page<?> page, String baseUrl) {
		if ((page.getNumber() + 1) < page.getTotalPages()) {
            return "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
        }
		
		return "";
	}

	private static String processLinkFirstPage(Page<?> page, String baseUrl) {
		return "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";
	}

	private static String processLinkPrevPage(Page<?> page, String baseUrl) {
		if (page.getNumber() > 0) {
            return "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
        }
		
		return "";
	}

	private static String processLinkLastPage(Page<?> page, String baseUrl) {
		int totalPages = page.getTotalPages();
		
		int lastPage = totalPages > 0 ? totalPages - 1 : 0;
		
        return "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
	}

    private static String generateUri(String baseUrl, int pagina, int tamanho) {
        return UriComponentsBuilder
        		.fromUriString(baseUrl)
        		.queryParam("page", pagina)
        		.queryParam("size", tamanho)
        		.toUriString();
    }
}

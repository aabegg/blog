package br.com.alessandro.linx.blog.linxblog.service.validators;

import br.com.alessandro.linx.blog.linxblog.model.Users;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;

public class UserValidator {
	
	private UsersRepository userRepository;
	
	public UserValidator(UsersRepository userRepository) {
		this.userRepository  = userRepository;
	}
	
	public void validateInsert(Users user) {
		validateInsertOrUpdate(user);
	}
	
	public void validateUpdate(Users user) {
		validateInsertOrUpdate(user);
	}
	
	private void validateInsertOrUpdate(Users user) {
		validateRequiredFields(user);
		validateFieldsSize(user);
		validateDuplicateEmail(user);
		validateDuplicateLogin(user);
	}
	
	private void validateRequiredFields(Users user) {
		new RequiredFieldsValidator()
				.add(user.getActive(), "Ativo")
				.add(user.getEmail(), "Email")
				.add(user.getLastName(), "Sobrenome")
				.add(user.getName(), "Nome")
				.add(user.getPassword(), "Senha")
				.add(user.getRegisterDate(), "Data de Registro")
				.validate();
	}
	
	private void validateFieldsSize(Users user) {
		FieldSizeValidator.validate(user.getEmail(), "Email", 10, 250);
		FieldSizeValidator.validate(user.getName(), "Nome", 2, 50);
		FieldSizeValidator.validate(user.getLastName(), "Nome", 2, 50);
		FieldSizeValidator.validate(user.getPassword(), "Senha", 5, 20);
	}
	
	private void validateDuplicateLogin(Users user) {
		String login = user.getLogin();
		
		userRepository.findByLogin(login)
				.ifPresent(userLocalized -> {
					throw new RuntimeException("O login informado já está em uso");
				});
	}
	
	private void validateDuplicateEmail(Users user) {
		String email = user.getEmail();
		
		userRepository.findByEmail(email)
				.ifPresent(userLocalized -> {
					throw new RuntimeException("O Email informado já está cadastrado para o usuário " + userLocalized.getLogin());
				});
	}
}

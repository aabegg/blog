package br.com.alessandro.linx.blog.linxblog.rest.dto;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Comment;
import br.com.alessandro.linx.blog.linxblog.model.Users;

public class CommentDTO {
	private Long id;
	
	private String body;
	
	private Long idUser;
	
	private Date commentDate;
	
	private String userName;
	
	public CommentDTO() { }
	
	public CommentDTO(Comment comment) {
		Users user = comment.getUser();
		
		setId(comment.getId());
		setBody(comment.getBody());
		setIdUser(user.getId());
		setUserName(user.getName().concat(" ").concat(user.getLastName()));
		setCommentDate(comment.getCommentDate());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}

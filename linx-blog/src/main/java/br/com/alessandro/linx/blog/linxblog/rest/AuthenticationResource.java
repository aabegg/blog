package br.com.alessandro.linx.blog.linxblog.rest;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alessandro.linx.blog.linxblog.model.Users;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;
import br.com.alessandro.linx.blog.linxblog.rest.dto.AuthenticationDTO;

@RestController
@Transactional
@RequestMapping("/authentication")
public class AuthenticationResource {
	
	private final static Set<Users> usersAutenticated = new HashSet<>(); 
	
	private final UsersRepository usersRepository;
	
	public AuthenticationResource(UsersRepository usersRepository) {
		this.usersRepository = usersRepository;
	}
	
	@PostMapping("/login")
	public ResponseEntity<Users> login(@RequestBody AuthenticationDTO authenticationBean) {
		Optional<Users> user = findUser(authenticationBean);
		
		ResponseEntity<Users> responseEntity;
		
		if (user.isPresent()) {
			responseEntity = new ResponseEntity<>(user.get(), HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		
		return responseEntity;
	}

	@GetMapping("/logout/{user}")
	public void logout(@PathVariable String user) {
		userAuthenticatedAndCached(user)
				.ifPresent(usersAutenticated::remove);
	}
	
	private Optional<Users> findUser(AuthenticationDTO authenticationBean) {
		String user = authenticationBean.getUser();
		String password = authenticationBean.getPassword();
		
		Optional<Users> userModel = userAuthenticatedAndCached(user);
		
		if (!userModel.isPresent()) {
			userModel = usersRepository.findOneByLoginAndPassword(user, password);
		}
		
		return userModel;
	}
	
	private Optional<Users> userAuthenticatedAndCached(final String login) {
		return usersAutenticated.stream()
				.filter(user -> user.getLogin().equals(login))
				.findFirst();
	}
}

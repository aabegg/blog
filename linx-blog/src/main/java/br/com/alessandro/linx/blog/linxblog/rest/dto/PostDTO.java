package br.com.alessandro.linx.blog.linxblog.rest.dto;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.model.Users;

public class PostDTO {
	private Long id;
	
	private String title;
	
	private String body;

	private Long idUser;
	
	private String userName;
	
	private Date datePost;
	
	private Long numberOfEdits;
	
	public PostDTO() { }
	
	public PostDTO(Post post) {
		Users user = post.getUser();
		
		setId(post.getId());
		setTitle(post.getTitle());
		setBody(post.getBody());
		setIdUser(user.getId());
		setUserName(user.getName().concat(" ").concat(user.getLastName()));
		setDatePost(post.getDatePost());
		setNumberOfEdits(post.getNumberOfEdits());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Date getDatePost() {
		return datePost;
	}

	public void setDatePost(Date datePost) {
		this.datePost = datePost;
	}

	public Long getNumberOfEdits() {
		return numberOfEdits;
	}

	public void setNumberOfEdits(Long numberOfEdits) {
		this.numberOfEdits = numberOfEdits;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}

package br.com.alessandro.linx.blog.linxblog.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.model.builder.PostBuilder;
import br.com.alessandro.linx.blog.linxblog.repository.PostRepository;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;
import br.com.alessandro.linx.blog.linxblog.rest.dto.PostDTO;
import br.com.alessandro.linx.blog.linxblog.rest.dto.PostSummaryDTO;
import br.com.alessandro.linx.blog.linxblog.rest.util.PaginationUtil;
import br.com.alessandro.linx.blog.linxblog.service.PostService;

@RestController
@Transactional
@RequestMapping("/post")
public class PostResource {
	
	private final PostService postService;
	
	private final PostRepository postRepository;
	
	private final UsersRepository usersRepository;
	
	public PostResource(PostService postService, PostRepository postRepository, UsersRepository usersRepository) {
		this.postService = postService;
		this.postRepository = postRepository;
		this.usersRepository = usersRepository;
	}
	
	@GetMapping
	public ResponseEntity<List<Post>> all(Pageable pageable) {
		final Page<Post> page = postRepository.findAll(pageable);
		
		HttpHeaders headers = PaginationUtil.generatePaginationHeader(page, "/api/post");
		
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public @ResponseBody PostDTO find(@PathVariable Long id) throws URISyntaxException {
		Post post = postRepository.findOne(id);
		return new PostDTO(post);
	}
	
	@PutMapping("/{id}")
	public void update(@PathVariable Long id, @Valid @RequestBody PostDTO postDTO) throws URISyntaxException {
		Post post = convertDTO(postDTO);
		post.setId(id);
		
		postService.update(post);
	}
	
	@PostMapping
	public ResponseEntity<Post> insert(@Valid @RequestBody PostDTO postDTO) throws URISyntaxException {
		Post post = convertDTO(postDTO);
		
		post = postService.insert(post);
		
		return ResponseEntity
				.created(new URI("/post"))
				.body(post);
	}
	
	@GetMapping("/summary")
	public List<PostSummaryDTO> summary() {
		return postRepository.findAllByOrderByDatePostDesc()
				.stream()
				.map(this::convertSumary)
				.collect(Collectors.toList());
	}
	
	@GetMapping("/lastpost")
	public PostDTO findLastPost() {
		Optional<Post> first = postRepository.findFirstByOrderByDatePostDesc();
		
		PostDTO postDTO = null;
		
		if (first.isPresent()) {
			postDTO =  new PostDTO(first.get());
		}
		
		return postDTO;
	}
	
	private PostSummaryDTO convertSumary(Post post) {
		Long id = post.getId();
		String title = post.getTitle();
		Date datePost = post.getDatePost();
		
		return new PostSummaryDTO(id, title, datePost);
	}
	
	private Post convertDTO(PostDTO postDTO) {
		Long idUser = postDTO.getIdUser();
		
		return new PostBuilder()
				.withBody(postDTO.getBody())
				.withDatePost(postDTO.getDatePost())
				.withId(postDTO.getId())
				.withNumberOfEdits(postDTO.getNumberOfEdits())
				.withTitle(postDTO.getTitle())
				.withUser(usersRepository.findOne(idUser))
				.build();
	}
}

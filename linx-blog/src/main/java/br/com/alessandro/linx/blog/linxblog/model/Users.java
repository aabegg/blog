package br.com.alessandro.linx.blog.linxblog.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Users extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "SEQ_ID", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, initialValue = 0, name = "SEQ_ID", sequenceName="idUsers")
	private Long id;
	
	private String name;
	
	private String lastName;
	
	private String login;
	
	private String password;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;
	
	private Boolean active;
	
	private String imageUrl;
	
	private String email;
	
	public Users() { }
	
	public Users(Long id) {
		setId(id);
	}

	public Users(Long id, String name, String lastName, String login, String password, Date registerDate,
			Boolean active, String imageUrl, String email) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.registerDate = registerDate;
		this.active = active;
		this.imageUrl = imageUrl;
		this.email = email;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}

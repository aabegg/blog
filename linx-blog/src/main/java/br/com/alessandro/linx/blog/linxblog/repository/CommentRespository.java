package br.com.alessandro.linx.blog.linxblog.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.alessandro.linx.blog.linxblog.model.Comment;
import br.com.alessandro.linx.blog.linxblog.model.Post;

@Repository
public interface CommentRespository extends JPABlogRepository<Comment, Long> {

	Page<Comment> findAllByPostOrderByCommentDateDesc(Post post, Pageable pageable);

}

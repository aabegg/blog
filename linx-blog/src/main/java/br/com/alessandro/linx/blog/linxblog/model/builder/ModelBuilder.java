package br.com.alessandro.linx.blog.linxblog.model.builder;

import br.com.alessandro.linx.blog.linxblog.model.AbstractEntity;

public interface ModelBuilder<E extends AbstractEntity> {
	E build();
}

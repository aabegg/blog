package br.com.alessandro.linx.blog.linxblog.rest.dto;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Users;

public class UserDTO {
	private Long id;
	
	private String name;
	
	private String lastName;
	
	private String login;
	
	private Date registerDate;
	
	private Boolean active;
	
	private String imageUrl;
	
	private String email;
	
	public UserDTO() { }
	
	public UserDTO(Users user) {
		setId(user.getId());
		setName(user.getName());
		setLogin(user.getLogin());
		setRegisterDate(user.getRegisterDate());
		setActive(user.getActive());
		setImageUrl(user.getImageUrl());
		setEmail(user.getEmail());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}

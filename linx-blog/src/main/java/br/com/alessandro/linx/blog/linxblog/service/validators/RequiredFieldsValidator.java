package br.com.alessandro.linx.blog.linxblog.service.validators;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class RequiredFieldsValidator {
	private Map<String, Object> required = new HashMap<>();
	
	public RequiredFieldsValidator add(Object value, String label) {
		required.put(label, value);
		
		return this;
	}
	
	public void validate() {
		String fields = "";
		
		for(Entry<String, Object> item : required.entrySet()) {
			final Object value = item.getValue();
			final String key = item.getKey();
			
			boolean isNull = value == null;
			
			if (!isNull && value instanceof String) {
				isNull = ((String) value).isEmpty();
			}
			
			if (isNull) {
				fields = fields.concat(key).concat("\n");
			}
		}
		
		if (!fields.isEmpty()) {
			throw new RuntimeException("Os seguintes campos são de preenchimento obirgatório:\n\n" + fields);
		}
	}
}

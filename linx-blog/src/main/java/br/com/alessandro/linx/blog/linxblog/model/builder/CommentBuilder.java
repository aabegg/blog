package br.com.alessandro.linx.blog.linxblog.model.builder;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Comment;
import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.model.Users;

public class CommentBuilder implements ModelBuilder<Comment> {
	private Long id;
	private String body;
	private Users user;
	private Date commentDate;
	private Post post;

	public CommentBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public CommentBuilder withBody(String body) {
		this.body = body;
		return this;
	}

	public CommentBuilder withUser(Users user) {
		this.user = user;
		return this;
	}

	public CommentBuilder withCommentDate(Date commentDate) {
		this.commentDate = commentDate;
		return this;
	}

	public CommentBuilder withPost(Post post) {
		this.post = post;
		return this;
	}
	
	@Override
	public Comment build() {
		return new Comment(id, body, user, commentDate, post);
	}
}

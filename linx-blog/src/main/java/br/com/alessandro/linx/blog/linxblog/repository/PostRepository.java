package br.com.alessandro.linx.blog.linxblog.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.alessandro.linx.blog.linxblog.model.Post;

@Repository
public interface PostRepository extends JPABlogRepository<Post, Long> {
	
	Optional<Post> findFirstByOrderByDatePostDesc();

	List<Post> findAllByOrderByDatePostDesc();
	
}

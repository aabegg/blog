package br.com.alessandro.linx.blog.linxblog.service.validators;

import br.com.alessandro.linx.blog.linxblog.model.Post;

public class PostValidator {
	
	public void validateInsert(Post post) {
		validateRequiredFields(post);
		validateSizeFields(post);
	}
	
	public void validateUpdate(Post post) {
		validateRequiredFields(post);
		validateSizeFields(post);
	}
	
	private void validateRequiredFields(Post post) {
		new RequiredFieldsValidator()
				.add(post.getDatePost(), "Data do post")
				.add(post.getTitle(), "Titulo")
				.add(post.getBody(), "Corpo do post")
				.add(post.getUser(), "Usuário")
				.validate();
	}
	
	private void validateSizeFields(Post post) {
		FieldSizeValidator.validateMax(post.getTitle(), "Título", 100);
	}
}

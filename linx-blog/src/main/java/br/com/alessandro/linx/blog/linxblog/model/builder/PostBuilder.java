package br.com.alessandro.linx.blog.linxblog.model.builder;

import java.util.Date;

import br.com.alessandro.linx.blog.linxblog.model.Post;
import br.com.alessandro.linx.blog.linxblog.model.Users;

public class PostBuilder implements ModelBuilder<Post> {
	private Long id;

	private String title;

	private String body;

	private Users user;

	private Date datePost;

	private Long numberOfEdits;

	public PostBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public PostBuilder withTitle(String title) {
		this.title = title;
		return this;
	}

	public PostBuilder withBody(String body) {
		this.body = body;
		return this;
	}

	public PostBuilder withUser(Users user) {
		this.user = user;
		return this;
	}

	public PostBuilder withDatePost(Date datePost) {
		this.datePost = datePost;
		return this;
	}

	public PostBuilder withNumberOfEdits(Long numberOfEdits) {
		this.numberOfEdits = numberOfEdits;
		return this;
	}
	
	@Override
	public Post build() {		
		return new Post(id, title, body, user, datePost, numberOfEdits);
	}
}

package br.com.alessandro.linx.blog.linxblog.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import br.com.alessandro.linx.blog.linxblog.model.Comment;
import br.com.alessandro.linx.blog.linxblog.repository.CommentRespository;
import br.com.alessandro.linx.blog.linxblog.service.validators.CommentValidator;

@Service
public class CommentService {
	
	private CommentRespository commentRespository;
	
	@Autowired
	private CommentValidator commentValidator;
	
	public CommentService(CommentRespository commentRespository) {
		this.commentRespository = commentRespository;
	}

	public void update(Comment comment) {
		commentValidator.validateUpdate(comment);
		
		commentRespository.findOneById(comment.getId()).ifPresent(managed -> {
			managed.setBody(comment.getBody());
		});
	}

	public Comment insert(Comment comment) {
		comment.setCommentDate(new Date());
		
		commentValidator.validateInsert(comment);
		
		return commentRespository.save(comment);
	}

	@Bean
	public CommentValidator getCommentValidator() {
		return new CommentValidator();
	}
}

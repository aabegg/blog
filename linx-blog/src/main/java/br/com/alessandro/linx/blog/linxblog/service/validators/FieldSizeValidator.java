package br.com.alessandro.linx.blog.linxblog.service.validators;

public class FieldSizeValidator {
	private FieldSizeValidator() {}
	
	public static void validate(String value, String label, int min, int max) {
		validateMin(value, label, min);
		validateMax(value, label, max);
	}
	
	public static void validateMax(String value, String label, int max) {
		if (value.length() > max) {
			throw new RuntimeException("O campo " + value + " deve possuir no máximo " + max + " caracteres");
		}
	}
	
	public static void validateMin(String value, String label, int min) {
		if (value.length() < min) {
			throw new RuntimeException("O campo " + value + " deve possuir no mínimo " + min + " caracteres");
		}
	}
}

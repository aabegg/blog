package br.com.alessandro.linx.blog.linxblog.service.validators;

import br.com.alessandro.linx.blog.linxblog.model.Comment;

public class CommentValidator {
	
	public void validateInsert(Comment comment) {
		validateRequiredFields(comment);
	}
	
	public void validateUpdate(Comment comment) {
		validateRequiredFields(comment);
	}
	
	private void validateRequiredFields(Comment comment) {
		new RequiredFieldsValidator()
				.add(comment.getBody(), "Mensagem")
				.add(comment.getPost(), "Post")
				.add(comment.getUser(), "Usuário")
				.add(comment.getCommentDate(), "Data do comentário")
				.validate();
	}
}

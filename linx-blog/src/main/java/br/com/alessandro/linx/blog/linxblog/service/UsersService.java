package br.com.alessandro.linx.blog.linxblog.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import br.com.alessandro.linx.blog.linxblog.model.Users;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;
import br.com.alessandro.linx.blog.linxblog.service.validators.UserValidator;

@Service
public class UsersService {
	
	private UsersRepository userRepository;
	
	@Autowired
	private UserValidator userValidator;
	
	public UsersService(UsersRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void update(Users user) {
		if (user.getActive() == null) {
			user.setActive(false);
		}
		
		userValidator.validateUpdate(user);
		
		userRepository.findOneById(user.getId()).ifPresent(managed -> {
			managed.setActive(user.getActive());
			managed.setImageUrl(user.getImageUrl());
			managed.setName(user.getName());
			managed.setLastName(user.getLastName());
			managed.setLogin(user.getLogin());
			managed.setPassword(user.getPassword());
			managed.setEmail(user.getEmail());
		});
	}

	public Users insert(Users user) {
		if (user.getActive() == null) {
			user.setActive(false);
		}
		user.setRegisterDate(new Date());
		
		userValidator.validateUpdate(user);
		
		return userRepository.save(user);
	}

	@Bean
	public UserValidator getUserValidator() {
		return new UserValidator(userRepository);
	}
}

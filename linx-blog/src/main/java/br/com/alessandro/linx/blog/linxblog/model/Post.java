package br.com.alessandro.linx.blog.linxblog.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Post extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "SEQ_ID", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, initialValue = 0, name = "SEQ_ID", sequenceName="idPost")
	private Long id;
	
	private String title;
	
	private String body;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idUser")
	private Users user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datePost;
	
	private Long numberOfEdits;
	
	public Post() { }
	
	public Post(Long id) {
		this.id = id;
	}
	
	public Post(Long id, String title, String body, Users user, Date datePost, Long numberOfEdits) {
		this.id = id;
		this.title = title;
		this.body = body;
		this.user = user;
		this.datePost = datePost;
		this.numberOfEdits = numberOfEdits;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Date getDatePost() {
		return datePost;
	}

	public void setDatePost(Date datePost) {
		this.datePost = datePost;
	}

	public Long getNumberOfEdits() {
		return numberOfEdits;
	}

	public void setNumberOfEdits(Long numberOfEdits) {
		this.numberOfEdits = numberOfEdits;
	}
	
	public void incrementNumberOfEdits() {
		if (this.numberOfEdits != null) {
			this.numberOfEdits = this.numberOfEdits.longValue() + 1;
		}
		
		this.numberOfEdits = 1L;
	}
}

package br.com.alessandro.linx.blog.linxblog.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.alessandro.linx.blog.linxblog.model.Users;
import br.com.alessandro.linx.blog.linxblog.model.builder.UsersBuilder;
import br.com.alessandro.linx.blog.linxblog.repository.UsersRepository;
import br.com.alessandro.linx.blog.linxblog.rest.dto.UserDTO;
import br.com.alessandro.linx.blog.linxblog.rest.dto.UserExtendedDTO;
import br.com.alessandro.linx.blog.linxblog.rest.util.PaginationUtil;
import br.com.alessandro.linx.blog.linxblog.service.UsersService;

@RestController
@Transactional
@RequestMapping("/user")
public class UsersResource {
	
	private final UsersService usersService;
	
	private final UsersRepository usersRepository;
	
	public UsersResource(UsersService usersService, UsersRepository usersRepository) {
		this.usersService = usersService;
		this.usersRepository = usersRepository;
	}
	
	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		usersRepository.delete(id);
	}
	
	@GetMapping
	public ResponseEntity<List<Users>> all(Pageable pageable) {
		final Page<Users> page = usersRepository.findAll(pageable);
		
		HttpHeaders headers = PaginationUtil.generatePaginationHeader(page, "/api/user");
		
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public @ResponseBody UserDTO find(@PathVariable Long id) throws URISyntaxException {
		Users users = usersRepository.findOne(id);
		return new UserDTO(users);
	}
	
	@PutMapping("/{id}")
	public void update(@PathVariable Long id, @Valid @RequestBody UserExtendedDTO userExtendedDTO) throws URISyntaxException {
		Users user = convertDTO(userExtendedDTO);
		user.setId(id);
		
		usersService.update(user);
	}
	
	@PostMapping
	public ResponseEntity<Users> insert(@Valid @RequestBody UserExtendedDTO userExtendedDTO) throws URISyntaxException {
		Users user = convertDTO(userExtendedDTO);
		user = usersService.insert(user);
		
		return ResponseEntity
				.created(new URI("/user"))
				.body(user);
	}
	
	private Users convertDTO(UserExtendedDTO userDTO) {
		return new UsersBuilder()
				.withActive(userDTO.getActive())
				.withEmail(userDTO.getEmail())
				.withId(userDTO.getId())
				.withImageUrl(userDTO.getImageUrl())
				.withLastName(userDTO.getLastName())
				.withLogin(userDTO.getLogin())
				.withName(userDTO.getName())
				.withPassword(userDTO.getPassword())
				.withRegisterDate(userDTO.getRegisterDate())
				.build();
	}
}

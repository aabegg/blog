package br.com.alessandro.linx.blog.linxblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LinxBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinxBlogApplication.class, args);
	}
	
	@Bean
    public FilterRegistrationBean corsFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new CorsFilter());
		filterRegistrationBean.setName("Cors");
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.setOrder(1);
		
		return filterRegistrationBean;
    }
}

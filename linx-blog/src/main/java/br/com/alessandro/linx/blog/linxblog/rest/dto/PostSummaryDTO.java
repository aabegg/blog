package br.com.alessandro.linx.blog.linxblog.rest.dto;

import java.util.Date;

public class PostSummaryDTO {
	private Long idPost;
	
	private String description;
	
	private Date datePost;
	
	public PostSummaryDTO() { }

	public PostSummaryDTO(Long id, String title, Date datePost) {
		this.idPost = id;
		this.description = title;
		this.datePost = datePost;
	}

	public Long getIdPost() {
		return idPost;
	}

	public void setIdPost(Long idPost) {
		this.idPost = idPost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDatePost() {
		return datePost;
	}

	public void setDatePost(Date datePost) {
		this.datePost = datePost;
	}
}

package br.com.alessandro.linx.blog.linxblog.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Comment extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "SEQ_ID", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, initialValue = 0, name = "SEQ_ID", sequenceName="idComment")
	private Long id;
	
	private String body;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idUser")
	private Users user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date commentDate;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idPost")
	private Post post;

	public Comment() { }
	
	public Comment(Long idComment) {
		this.id = idComment;
	}
	
	public Comment(Long id, String body, Users user, Date commentDate, Post post) {
		this.id = id;
		this.body = body;
		this.user = user;
		this.commentDate = commentDate;
		this.post = post;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
}

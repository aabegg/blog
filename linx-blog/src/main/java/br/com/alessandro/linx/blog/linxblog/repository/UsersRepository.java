package br.com.alessandro.linx.blog.linxblog.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.alessandro.linx.blog.linxblog.model.Users;

@Repository
public interface UsersRepository extends JPABlogRepository<Users, Long> {

	Optional<Users> findOneByLoginAndPassword(String login, String password);

	Optional<Users> findByEmail(String email);

	Optional<Users> findByLogin(String login);
	
}

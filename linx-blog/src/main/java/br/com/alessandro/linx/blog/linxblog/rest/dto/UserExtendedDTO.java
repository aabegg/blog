package br.com.alessandro.linx.blog.linxblog.rest.dto;


import br.com.alessandro.linx.blog.linxblog.model.Users;

public class UserExtendedDTO extends UserDTO {
	private String password;
	
	public UserExtendedDTO() { }
	
	public UserExtendedDTO(Users user) {
		super(user);
		setPassword(user.getPassword());
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

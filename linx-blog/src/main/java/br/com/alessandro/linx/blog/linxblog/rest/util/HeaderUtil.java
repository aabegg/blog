package br.com.alessandro.linx.blog.linxblog.rest.util;

import java.text.MessageFormat;

import org.springframework.http.HttpHeaders;

public final class HeaderUtil {

    private HeaderUtil() { }

    public static HttpHeaders createAlert(String entityName, String param) {
    	MessageFormat.format(entityName, param);
    	
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-blog-alert", entityName);
        headers.add("X-blog-param", param);
        
        return headers;
    }

    public static HttpHeaders createAlertEntityCreated(String entityName, String param) {
        return createAlert(entityName + " criado(a) com identificador {0}", param);
    }

    public static HttpHeaders createAlertEntityUpdated(String entityName, String param) {
        return createAlert(entityName + " atualizado(a) com identificador {0}", param);
    }

    public static HttpHeaders createAlertEntityDeleted(String entityName, String param) {
        return createAlert(entityName + " excluído(a) com identificador {0}", param);
    }

    public static HttpHeaders createAlertError(String entityName, String message) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-blog-error", message);
        headers.add("X-blog-params", entityName);
        
        return headers;
    }
}

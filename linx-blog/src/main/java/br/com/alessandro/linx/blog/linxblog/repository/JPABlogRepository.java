package br.com.alessandro.linx.blog.linxblog.repository;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface JPABlogRepository<T, ID extends Serializable> extends JpaRepository<T, ID>{
	
	Optional<T> findOneById(ID id);
	
}

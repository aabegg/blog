package br.com.alessandro.linx.blog.linxblog.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("br.com.alessandro.linx.blog.linxblog.repository")
@EnableTransactionManagement
public class RepositoryConfiguration {

}

drop table comment;
drop table post;
drop table users;

drop sequence idusers;
drop sequence idpost;
drop sequence idcomment;

create sequence idusers cache 1;
create sequence idpost cache 1;
create sequence idcomment cache 1;

create table users(
  id numeric(10) primary key,
  login varchar(20) not null unique,
  name varchar(50) not null,
  email varchar(100) not null unique,
  lastName varchar(50) not null,
  password varchar(32) not null,
  registerdate timestamp not null,
  active boolean default false not null,
  imageurl varchar(250)
);

create table post(
  id numeric(10) primary key,
  title varchar(100) not null,
  body text not null,
  idUser numeric(10) not null references users(id),
  datePost timestamp not null,
  numberOfEdits numeric(3)
);

create table comment(
  id numeric(10) primary key,
  idUser numeric(10) not null references users(id),
  commentDate timestamp not null,
  idpost numeric(10) not null references post(id),
  body text not null
);

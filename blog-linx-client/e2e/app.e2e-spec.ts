import { BlogLinxClientPage } from './app.po';

describe('blog-linx-client App', () => {
  let page: BlogLinxClientPage;

  beforeEach(() => {
    page = new BlogLinxClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { AuthenticationParams } from './authentication.model';
import { AuthenticationResource } from './authentication.resource';
import { User } from '../model/user/user.model';

@Injectable()
export class AuthenticationService {

  get isAuthenticated(): boolean {
    return !!this.userAuthenticated;
  }

  userAuthenticated: User;

  constructor(
    private authenticationResource: AuthenticationResource,
    private router: Router
  ) { }

  login(authentication: AuthenticationParams, isAutenticated: (boolean) => void) {
    this.authenticationResource.login(authentication, (data) => {
        this.userAuthenticated = data;
        isAutenticated(!!data);
    });
  }

  logout() {
    if (this.userAuthenticated) {
      this.authenticationResource.logout({
        login: this.userAuthenticated.login
      }, () => {
        this.userAuthenticated = null;
        this.router.navigate(['post']);
      });
    }
  }
}

import { EnumAlert } from '../custom/alert/alert.enum';
import { AlertService } from './../custom/alert/alert.service';
import { AuthenticationService } from './authentication.service';
import { Resource, ResourceActionBase, ResourceAction, ResourceMethod, ResourceParams } from '../../assets/lib/ngx-resource';
import { Observable, Subscriber } from 'rxjs/Rx';
import { AuthenticationParams } from './authentication.model';
import { User } from './../model/user/user.model';
import { Http, RequestMethod, Request } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
@ResourceParams({
  url: 'http://localhost:8080/api/authentication'
})
export class AuthenticationResource extends Resource {

  @ResourceAction({
    method: RequestMethod.Post,
    path: '/login'
  })
  login: ResourceMethod<AuthenticationParams, User>;

  @ResourceAction({
    path: '/logout/{!login}'
  })
  logout: ResourceMethod<{login: string}, void>;

  constructor(
    http: Http,
    private alertService: AlertService
  ) {
    super(http);
  }

  $responseInterceptor(observable: Observable<any>, request: Request, methodOptions: ResourceActionBase): Observable<any> {
    return Observable.create((subscriber: Subscriber<any>) => {

      observable.subscribe(
        (res: Response) => {
          this.processHeaders(res);
          subscriber.next((<any>res)._body ? res.json() : null);
        },
        (error: Response) => {
          this.processError(error);
          subscriber.error(error);
        },
        () => subscriber.complete()
      )
    });
  }

  private processHeaders(res: Response) {
    if (res.headers) {
        // Quando fazer autenticação aqui pegar token
    }
  }

  private processError(error: Response) {
    switch (error.status) {
      case 401: this.process401(); break;
    }
  }

  private process401() {
    this.alertService.namedAlert('user', {
      type: EnumAlert.WARNING,
      message: 'Usuário sem permissão!'
    });
  }
}

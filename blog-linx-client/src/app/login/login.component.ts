import { Component, OnInit, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/ng2-bootstrap';

import { AuthenticationParams } from './authentication.model';
import { AuthenticationService } from './authentication.service';
import { User } from './../model/user/user.model';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('childModal') childModal: ModalDirective;

  private _callback: (boolean) => void;

  model = <AuthenticationParams> {
    user: '',
    password: ''
  };

  constructor(
    private loginService: LoginService,
    private authenticationService: AuthenticationService
  ) {
    loginService.registerLoginView.subscribe((callback: (boolean) => void) => {
      this.childModal.show();
      this._callback = callback;
    });
  }

  ngOnInit() {
  }

  login() {
    const isLogged = this.authenticationService.login(this.model, (logged: boolean) => {
      if (logged) {
        this._callback(true);
        this.cancel();
      } else {
        this._callback(true);
      }
    });
  }

  cancel() {
    this.childModal.hide();
  }
}

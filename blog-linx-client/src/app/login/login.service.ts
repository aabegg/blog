import { Injectable, EventEmitter } from '@angular/core';

import { User } from './../model/user/user.model';

@Injectable()
export class LoginService {

  registerLoginView = new EventEmitter<(boolean) => void>();

  constructor() { }

  openLogin(callBack: (boolean) => void) {
    this.registerLoginView.emit(callBack);
  }
}

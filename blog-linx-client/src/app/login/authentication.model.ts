export interface AuthenticationParams {
  user: string,
  password: string
}

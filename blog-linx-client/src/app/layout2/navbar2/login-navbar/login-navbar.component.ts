import { AuthenticationService } from '../../../login/authentication.service';
import { LoginService } from '../../../login/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-navbar',
  templateUrl: './login-navbar.component.html',
  styleUrls: ['./login-navbar.component.scss']
})
export class LoginNavbarComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  login() {
    this.loginService.openLogin(() => {});
  }

  logout() {
    this.authenticationService.logout();
  }

  get isAuthenticated() {
    return this.authenticationService.isAuthenticated;
  }

  get userName(): string {
    const user = this.authenticationService.userAuthenticated;

    if (user) {
      return user.name;
    }
  }
}

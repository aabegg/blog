import { AuthenticationService } from '../../login/authentication.service';
import { EnumAlert } from '../../custom/alert/alert.enum';
import { AlertService } from '../../custom/alert/alert.service';
import { LoginService } from './../../login/login.service';
import { MenuItem2 } from './menu-item2.model';
import { appRoutes } from './../../app-routing.module';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar2.component.html',
  styleUrls: ['./navbar2.component.scss']
})
export class Navbar2Component implements OnInit {

  constructor(
    private loginService: LoginService,
    private alertService: AlertService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  login() {
    this.loginService.openLogin(() => {});
  }

  logout() {

  }

  get isAuthenticated() {
    return this.authenticationService.isAuthenticated;
  }
}

import { PostService } from './../../views/post/post.service';
import { PostResource } from '../../model/post/post.resource';
import { Summary } from './../../model/post/summary.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-arquivos',
  templateUrl: './arquivos.component.html',
  styleUrls: ['./arquivos.component.scss']
})
export class ArquivosComponent implements OnInit {

  items: Array<Summary>;

  constructor(
    private postResource: PostResource,
    private postService: PostService
  ) { }

  ngOnInit() {
    this.fill();
    this.registerPostChange();
  }

  private registerPostChange() {
    this.postService.listener.subscribe(() => {
      this.fill();
    });
  }

  private fill() {
    this.postResource.getFullSummary({}, data => {
      this.items = data;
    });
  }

}

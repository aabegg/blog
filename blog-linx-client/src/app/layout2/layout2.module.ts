import { Navbar2Component } from './navbar2/navbar2.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomModule } from './../custom/custom.module';
import { LoginService } from '../login/login.service';
import { LoginModule } from '../login/login.module';
import { Template2Component } from './template2/template2.component';
import { LoginNavbarComponent } from './navbar2/login-navbar/login-navbar.component';
import { ArquivosComponent } from './arquivos/arquivos.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LoginModule,
    CustomModule
  ],
  exports: [
    Template2Component
  ],
  declarations: [
    Template2Component,
    Navbar2Component,
    LoginNavbarComponent,
    ArquivosComponent
  ],
  providers: [
    LoginService
  ]
})
export class Layout2Module { }

import { AlertService } from './../../../custom/alert/alert.service';
import { Router } from '@angular/router';
import { FormGroup, NgModel, FormBuilder } from '@angular/forms';
import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';

import { Observable, Subscriber, Subscription } from 'rxjs/Rx';

import { AuthenticationService } from './../../../login/authentication.service';
import { userRoutes } from './../user-routing.module';
import { UserResource } from './../../../model/user/user.resource';
import { AppValidators } from './../../../custom/validators/validators';
import { User } from './../../../model/user/user.model';
import { SubscriptionUtil } from './../../../util/subscription-util'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  providers: [ NgModel ]
})
export class UserFormComponent implements OnInit, OnDestroy {

  private _modelEditingSubscription: Subscription;

  public form: FormGroup;

  public user: User;

  constructor(
    private formBuilder: FormBuilder,
    private userResource: UserResource,
    private authenticationService: AuthenticationService,
    private router: Router,
    private alertService: AlertService
  ) {
    this.initForm();
  }

  ngOnInit() {
    if (this.router.url === '/user/edit') {
      this.user = this.authenticationService.userAuthenticated;
      this.form.patchValue(this.user);
    }
  }

  ngOnDestroy() {
    SubscriptionUtil.unsubscribe(this._modelEditingSubscription);
  }

  save(user: User) {
    if (this.user && this.user.id) {
      this.userResource.update(user, {id: this.user.id}, () => {
        this.alertService.notifySuccess('Sucesso', 'O usuário foi alterado');
      });
    } else {
      this.userResource.save(user, () => {
        this.alertService.notifySuccess('Sucesso', 'O usuário foi salvo. Por favor, efetue o login');
      });
    }
  }

  cancel() {
    this.form.reset();
  }

  private successMessage(message) {

  }

  private initForm() {
    this.form = this.formBuilder.group({
      login: [null, [AppValidators.minLength(3), AppValidators.maxLength(20), AppValidators.required()]],
      password: [null, [AppValidators.minLength(5), AppValidators.maxLength(20), AppValidators.required()]],
      name: [null, [AppValidators.minLength(2), AppValidators.maxLength(50), AppValidators.required()]],
      lastName: [null, [AppValidators.minLength(2), AppValidators.maxLength(50), AppValidators.required()]],
      registerDate: [{value: null, disabled: true}],
      email: [null, [AppValidators.maxLength(250), AppValidators.email(), AppValidators.required()]],
      active: [null]
    });
  }
}

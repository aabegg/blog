import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserResource } from './../../model/user/user.resource';
import { CustomModule } from '../../custom/custom.module';
import { UserRoutingModule } from './user-routing.module';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserFormComponent } from './user-form/user-form.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    CustomModule,
    ReactiveFormsModule
  ],
  declarations: [
    UserListingComponent,
    UserFormComponent
  ],
  providers: [
    UserResource
  ]
})
export class UserModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanActivateViaAuthGuard } from './../../login/can-activate-via-auth-guard.service';
import { UserFormComponent } from './user-form/user-form.component';

export const userRoutes: Routes = [
  { path: '', redirectTo: 'new', pathMatch: 'full' },
  { path: 'new', component: UserFormComponent, },
  { path: 'edit', component: UserFormComponent, canActivate: [ CanActivateViaAuthGuard ] }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule { }

import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

import { Subscription } from 'rxjs/Rx';

import { Action } from './../../../custom/table/action.model';
import { Column } from './../../../custom/table/column.model';
import { UserResource } from './../../../model/user/user.resource';
import { User } from './../../../model/user/user.model';
import { SubscriptionUtil } from './../../../util/subscription-util';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit, OnDestroy {

  @Output()
  public edit = new EventEmitter<User>();

  private subscriberResource: Subscription;
  public users: Array<User>;
  public columns: Array<Column>;
  public actions: Array<Action>;

  constructor(
    private userResource: UserResource
  ) {
    this.setUpColumns();
    this.setUpActions();
  }

  ngOnInit() {
    this.fill();
  }

  editFn(user: User) {

  }

  deleteFn(user: User) {

  }

  ngOnDestroy() {
    SubscriptionUtil.unsubscribe(this.subscriberResource)
  }

  private fill() {
    SubscriptionUtil.unsubscribe(this.subscriberResource)

    this.subscriberResource = this.userResource.query()
      .$observable
      .subscribe(data => {
        this.users = data;
      });
  }

  private setUpColumns() {
    this.columns = [
      { label: ['name', 'lastName'], alias: 'Nome' },
      { label: 'login', alias: 'Login' },
      { label: 'registerDate', alias: 'Data de Registro' },
      { label: 'active', alias: 'Ativo' },
      { label: 'email', alias: 'E-mail' }
    ];
  }

  private setUpActions() {
    this.actions = [
      {
        action: (user) => this.editFn(user),
        icon: 'fa-edit'
      },
      {
        action: (user) => this.deleteFn(user),
        icon: 'fa-trash'
      }
    ]
  }
}

import { Component, OnInit, Input } from '@angular/core';

import { User } from './../../../../model/user/user.model';
import { Comment } from '../../../../model/comment/comment.model';

@Component({
  selector: 'app-item-comment',
  templateUrl: './item-comment.component.html',
  styleUrls: ['./item-comment.component.scss']
})
export class ItemCommentComponent implements OnInit {

  @Input() data: Comment;

  user: User;

  constructor( ) { }

  ngOnInit() {

  }

}

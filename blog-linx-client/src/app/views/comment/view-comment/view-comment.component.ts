import { CommentService } from './../comment.service';
import { PostService } from './../../post/post.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { LoginService } from './../../../login/login.service';
import { AuthenticationService } from '../../../login/authentication.service';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { CommentResource } from './../../../model/comment/comment.resource';
import { SubscriptionUtil } from './../../../util/subscription-util';
import { Comment } from './../../../model/comment/comment.model'

@Component({
  selector: 'app-view-comment',
  templateUrl: './view-comment.component.html',
  styleUrls: ['./view-comment.component.scss']
})
export class ViewCommentComponent implements OnInit, OnDestroy {

  private urlParamSubscription: Subscription;

  comments: Array<Comment>;

  _newCommentOppened = false;

  constructor(
    private commentResource: CommentResource,
    private authenticationService: AuthenticationService,
    private loginService: LoginService,
    private route: ActivatedRoute,
    private postService: PostService,
    private commentService: CommentService
  ) { }

  get newCommentOppened() {
    return this._newCommentOppened && this.authenticationService.isAuthenticated;
  }

  novo() {
    if (!this.authenticationService.isAuthenticated) {
      this.loginService.openLogin(state => {
        this._newCommentOppened = state;
      });
    } else {
      this._newCommentOppened = true;
    }
  }

  dispose() {
    this._newCommentOppened = false;
  }

  ngOnInit() {
    const evt = () => {
      this.fill();
    }

    evt();

    this.commentService.commentChanged.subscribe(evt);

    this.postService.listener.subscribe(evt);
  }

  ngOnDestroy() {
    SubscriptionUtil.unsubscribe(this.urlParamSubscription);
  }

  private fill() {
    this.urlParamSubscription = this.route.params.subscribe(params => {
      const idPost = params['idPost'];

      if (idPost) {
        this.commentResource.query({idPost: idPost}, data => {
          this.comments = data;
        });
      }
    });
  }
}

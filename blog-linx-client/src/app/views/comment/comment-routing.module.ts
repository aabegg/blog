import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCommentComponent } from './create-comment/create-comment.component'
import { ViewCommentComponent } from './view-comment/view-comment.component'

const routes: Routes = [
  { path: '', component: ViewCommentComponent },
  { path: 'new', component: CreateCommentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CommentRoutingModule { }

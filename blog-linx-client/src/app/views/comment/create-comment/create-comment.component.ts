import { CommentService } from './../comment.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { AuthenticationService } from './../../../login/authentication.service';
import { AlertService } from './../../../custom/alert/alert.service';
import { Comment } from '../../../model/comment/comment.model';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';

import { CommentResource } from './../../../model/comment/comment.resource';
import { SubscriptionUtil } from './../../../util/subscription-util';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss']
})
export class CreateCommentComponent implements OnInit, OnDestroy {

  private idPost: number;

  @Output() dispose = new EventEmitter();

  private urlParamSubscription: Subscription;

  comment: Comment = {
    body: null
  };

  constructor(
    private commentResource: CommentResource,
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private commentService: CommentService
  ) { }

  ngOnInit() {
    this.urlParamSubscription = this.route.params.subscribe(params => {
      this.idPost = params['idPost'];
    });
  }

  ngOnDestroy() {
    SubscriptionUtil.unsubscribe(this.urlParamSubscription);
  }

  save() {
    this.comment.idUser = this.authenticationService.userAuthenticated.id;

    this.commentResource.save(this.comment, {idPost: this.idPost}, () => {
      this.alertService.notifySuccess('Obrigado pela sua opinião', 'Comentário salvo.');
      this.cancel();
      this.commentService.commentChanged.emit();
    });
  }

  cancel() {
    this.dispose.emit(this.idPost);
  }
}

import { CommentService } from './comment.service';
import { UserResource } from './../../model/user/user.resource';
import { FormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateCommentComponent } from './create-comment/create-comment.component';
import { ViewCommentComponent } from './view-comment/view-comment.component';
import { CommentResource } from './../../model/comment/comment.resource';
import { CustomModule } from './../../custom/custom.module';
import { CommentRoutingModule } from './comment-routing.module';
import { ItemCommentComponent } from './view-comment/item-comment/item-comment.component';

@NgModule({
  imports: [
    CommonModule,
    CommentRoutingModule,
    FormsModule,
    CustomModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    CreateCommentComponent,
    ViewCommentComponent,
    ItemCommentComponent
  ],
  exports: [
    CreateCommentComponent,
    ViewCommentComponent
  ],
  providers: [
    CommentResource,
    UserResource,
    CommentService
  ]
})
export class CommentModule { }

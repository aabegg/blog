import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class CommentService {

  private _commentChanged = new EventEmitter<void>();

  get commentChanged(): EventEmitter<void> {
    return this._commentChanged;
  }

  constructor() { }
}

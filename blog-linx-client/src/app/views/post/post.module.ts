import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { CommentModule } from './../comment/comment.module';
import { CanActivateViaAuthGuard } from './../../login/can-activate-via-auth-guard.service';
import { PostResource } from './../../model/post/post.resource';
import { CustomModule } from './../../custom/custom.module';
import { PostRoutingModule } from './post-routing.module';
import { CreatePostComponent } from './create-post/create-post.component';
import { ViewPostComponent } from './view-post/view-post.component';

@NgModule({
  imports: [
    CommonModule,
    PostRoutingModule,
    FormsModule,
    CustomModule,
    CommentModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    CreatePostComponent,
    ViewPostComponent
  ],
  providers: [
    PostResource,
    CanActivateViaAuthGuard
  ]
})
export class PostModule { }

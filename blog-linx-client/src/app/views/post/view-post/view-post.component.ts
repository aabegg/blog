import { PostService } from './../post.service';
import { AuthenticationService } from './../../../login/authentication.service';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { PostResource } from './../../../model/post/post.resource';
import { UserResource } from './../../../model/user/user.resource';
import { User } from '../../../model/user/user.model';
import { Post } from './../../../model/post/post.model';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {

  private urlParamSubscription: Subscription;

  post: Post

  constructor(
    private postResource: PostResource,
    private userResource: UserResource,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private postService: PostService
  ) { }

  ngOnInit() {
    this.initModels();

    this.urlParamSubscription = this.route.params.subscribe(params => {
      const id = params['idPost'];
      this.postService.refresh();
      this.fillPost(id);
    });
  }

  get isLastPost(): boolean {
    return this.router.url === '/post';
  }

  get isAuthenticated(): boolean {
    const user = this.authenticationService.userAuthenticated;

    if (user) {
      return user.id === this.post.idUser;
    }
  }

  edit() {
    this.router.navigate(['post', this.post.id, 'edit']);
  }

  private initModels() {
    this.post = {
      id: null,
      body: null,
      datePost: null,
      idUser: null,
      numberOfEdits: null,
      title: null
    }
  }

  private fillPost(idPost: number) {
    if (idPost) {
      this.postResource.get({id: idPost}, data => this.loadPost(data));
    } else {
      this.postResource.getLastPost({}, data => this.loadPost(data));
    }
  }

  private loadPost(post: Post) {
    this.post = post;

    const idUser = post.idUser;
  }
}

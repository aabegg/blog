import { Observable } from 'rxjs/Rx';
import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';

import { EnumAlert } from '../../../custom/alert/alert.enum';
import { AlertService } from './../../../custom/alert/alert.service';
import { Post } from './../../../model/post/post.model';
import { AuthenticationService } from './../../../login/authentication.service';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { PostResource } from './../../../model/post/post.resource';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

  post: Post;

  constructor(
    private postResource: PostResource,
    private authenticationService: AuthenticationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private alertService: AlertService
  ) {}

  save() {
    if (!this.post.idUser) {
      this.post.idUser = this.authenticationService.userAuthenticated.id;
    }

    const idPost = this.post.id;
    this.activateServer(idPost).subscribe(() => {
      this.postService.refresh();
      this.cancel();
      this.alertService.notify({
        type: EnumAlert.SUCCESS,
        title: 'Sucesso',
        message: 'Registro foi salvo'
      });
    });
  }

  cancel() {
    this.router.navigate(['/post']);
  }

  clear() {
    this.initPost(null);
  }

  ngOnInit() {
    this.initPost(null);

    this.activatedRoute.params.subscribe(params => {
      const id = params['idPost'];

      if (id) {
        this.loadToEdit(id);
      }
    });
  }

  private activateServer(idPost): Observable<any> {
    let obs: Observable<any>;

    if (idPost) {
      obs = this.postResource.update(this.post, {id: idPost}).$observable;
    } else {
      obs = this.postResource.save(this.post, data => {
        this.post = data;
      }).$observable;
    }

    return obs;
  }

  private loadToEdit(id: number) {
    this.postResource.get({id: id}, data => {
      this.initPost(data);
    });
  }

  private initPost(post: Post) {
    this.post = post ? post : {
      title: '',
      body: '',
      idUser: this.authenticationService.userAuthenticated.id
    };
  }
}

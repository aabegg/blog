import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class PostService {

  listener = new EventEmitter();

  constructor() { }

  refresh() {
    this.listener.emit();
  }
}

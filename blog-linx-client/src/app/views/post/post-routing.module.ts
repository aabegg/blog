import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanActivateViaAuthGuard } from './../../login/can-activate-via-auth-guard.service';
import { CreatePostComponent } from './create-post/create-post.component';
import { ViewPostComponent } from './view-post/view-post.component';

const routes: Routes = [
  {
    path: '',
    component: ViewPostComponent,
    pathMatch: 'full'
  },
  {
    path: 'new',
    component: CreatePostComponent,
    canActivate: [ CanActivateViaAuthGuard ]
  },
  {
    path: ':idPost/edit',
    component: CreatePostComponent,
    canActivate: [ CanActivateViaAuthGuard ]
  },
  {
    path: ':idPost',
    component: ViewPostComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PostRoutingModule { }

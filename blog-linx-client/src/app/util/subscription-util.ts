import { Subscription } from 'rxjs/Rx';

export abstract class SubscriptionUtil {

  public static unsubscribe(subscribe: Subscription) {
    if (subscribe && !subscribe.closed) {
      subscribe.unsubscribe();
    }
  }
}

export interface Action {
  cssClass?: string;
  action: (value) => void;
  icon: string;
}

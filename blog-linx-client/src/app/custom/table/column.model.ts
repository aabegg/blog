export interface Column {
  alias: string,
  label: string | Array<string>,
  separator?: string
}

import { Component, OnInit, Input } from '@angular/core';

import { Action } from './action.model';
import { Column } from './column.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() public columns: Array<Column>;

  @Input() public data: Array<any>;

  @Input() public defaultSeparator = ' ';

  @Input() public actions: Array<Action>;

  constructor() { }

  ngOnInit() { }

  public showActions(): boolean {
    return !!this.actions;
  }

  public getDataColumn(data: any, column: Column): string {
    const label = column.label;

    let value = '';

    if (label instanceof Array) {
      let addSeparator = false;
      label.forEach(lab => {
        if (addSeparator) { value += this.getSeparator(column) }
        addSeparator = true;
        value += this.getData(data, lab);
      });
    } else {
      value = this.getData(data, label);
    }

    return value;
  }

  private getData(data, label): string {
    return data[label];
  }

  private getSeparator(column: Column): string {
    const separator = column.separator;
    return separator && separator.length > 0 ? separator : this.defaultSeparator;
  }

}

import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { ColComponent } from './col/col.component';
import { ButtonsModule } from './buttons/buttons.module';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { ValidatorsModule } from './validators/validators.module';
import { RowComponent } from './row/row.component';
import { FieldValidatorComponent } from './field-validator/field-validator.component';
import { InputComponent } from './input/input.component';
import { TableComponent } from './table/table.component';
import { CardTitleComponent } from './card-title/card-title.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ValidatorsModule,
    ButtonsModule,
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    InputComponent,
    FieldValidatorComponent,
    TableComponent,
    RowComponent,
    CheckboxComponent,
    CardTitleComponent,
    ColComponent,
    AlertComponent
  ],
  exports: [
    InputComponent,
    FormsModule,
    TableComponent,
    RowComponent,
    ValidatorsModule,
    CheckboxComponent,
    ButtonsModule,
    CardTitleComponent,
    ColComponent,
    AlertComponent,
    TooltipModule
  ],
  providers: []
})
export class CustomModule { }

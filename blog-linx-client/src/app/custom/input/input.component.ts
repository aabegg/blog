import { Subscription } from 'rxjs/Rx';
import { FormControl, Validators, NG_VALUE_ACCESSOR, FormGroup } from '@angular/forms';
import { Component, OnInit, Input, ViewEncapsulation, ElementRef, ViewChild, HostListener, OnDestroy, forwardRef } from '@angular/core';

import { AbstractValueAccessor } from './../util/abstract-value-accessor';

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputComponent),
  multi: true
};

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [ CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR ]
})
export class InputComponent extends AbstractValueAccessor implements OnInit {

  @Input() readonly = false;

  @Input() required;

  @Input() control: FormControl;

  @Input() type = 'text';

  @Input() pype;

  constructor(elementRef: ElementRef) {
    super(elementRef);
    super.addClass('form-group');
  }

  changeValue(value) {
    this.value = value;
  }

  onBlur() {
    this.onTouched();
  }

  ngOnInit() {
    super.ngOnInit();
  }
}

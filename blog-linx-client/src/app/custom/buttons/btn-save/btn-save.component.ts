import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit, ElementRef, Input, Output, Renderer, EventEmitter, HostBinding } from '@angular/core';
import { AbstractComponent } from './../../util/abstract-component';

@Component({
  selector: 'app-btn-save',
  templateUrl: './btn-save.component.html',
  styleUrls: ['./btn-save.component.scss']
})
export class BtnSaveComponent extends AbstractComponent implements OnInit {

  @HostBinding() tabindex = 0;

  @Input() public form: FormGroup;

  @Output() public save = new EventEmitter<any>();

  constructor(private element: ElementRef, private renderer: Renderer) {
    super(element);
  }

  ngOnInit() {
    super.addClasses(['btn', 'btn-success']);

    const event = () => {
      if (!this.form) {
        this.save.emit();
      } else if (this.form.valid) {
        this.markAllTouched();
        this.save.emit(this.form.value);
      }
    };

    this.renderer.listen(this.element.nativeElement, 'click', event);
    this.renderer.listen(this.element.nativeElement, 'keypress', event);
  }

  private markAllTouched() {
    Object.keys(this.form.controls)
        .map(name => {
          this.form.get(name).markAsTouched({onlySelf: true});
        });
  }

}

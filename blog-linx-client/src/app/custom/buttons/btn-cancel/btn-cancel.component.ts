import { Component, OnInit, ElementRef, HostBinding, Input, Output, EventEmitter, Renderer } from '@angular/core';
import { AbstractComponent } from './../../util/abstract-component';
import { FormControl, NgModel } from '@angular/forms';

@Component({
  selector: 'app-btn-cancel',
  templateUrl: './btn-cancel.component.html',
  styleUrls: ['./btn-cancel.component.scss'],
  providers: [ NgModel ]
})
export class BtnCancelComponent extends AbstractComponent implements OnInit {

  @Input() label = 'Cancelar';

  @Output() action = new EventEmitter<void>();

  @HostBinding() tabindex = 0;

  constructor(private element: ElementRef, private renderer: Renderer) {
    super(element);
    super.addClasses(['btn', 'btn-danger']);
  }

  ngOnInit() {
    const event = () => {
      if (this.action) {
        this.action.emit();
      }
    };

    this.renderer.listen(this.element.nativeElement, 'click', event);
    this.renderer.listen(this.element.nativeElement, 'keypress', event);
  }

}

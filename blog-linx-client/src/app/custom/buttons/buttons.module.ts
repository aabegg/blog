import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnSaveComponent } from './btn-save/btn-save.component';
import { BtnCancelComponent } from './btn-cancel/btn-cancel.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BtnSaveComponent,
    BtnCancelComponent
  ],
  exports: [
    BtnSaveComponent,
    BtnCancelComponent
  ]
})
export class ButtonsModule { }

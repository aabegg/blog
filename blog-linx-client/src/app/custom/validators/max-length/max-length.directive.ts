import { Directive, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';

import { validateMaxLengthFactory } from './max-length.factory';

@Directive({
  selector: '[appMinLength][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: MaxLengthDirective, multi: true }
  ]
})
export class MaxLengthDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateMaxLengthFactory(0);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}

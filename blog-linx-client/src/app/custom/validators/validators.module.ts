import { EmailValidatorDirective } from './email/email-validator.directive';
import { MinDirective } from './min/min.directive';
import { MaxDirective } from './max/max.directive';
import { MaxLengthDirective } from './max-length/max-length.directive';
import { MinLengthDirective } from './min-length/min-length.directive';
import { RequiredValidatorDirective } from './required/required-validator.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RequiredValidatorDirective,
    MinLengthDirective,
    MaxLengthDirective,
    MaxDirective,
    MinDirective,
    EmailValidatorDirective
  ],
  exports: [
    RequiredValidatorDirective,
    MinLengthDirective,
    MaxLengthDirective,
    MaxDirective,
    MinDirective,
    EmailValidatorDirective
  ]
})
export class ValidatorsModule { }

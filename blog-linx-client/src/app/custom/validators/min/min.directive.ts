import { Directive, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';

import { validateMinFactory } from './min.factory';

@Directive({
  selector: '[appMin][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: MinDirective, multi: true }
  ]
})
export class MinDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateMinFactory(0);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}

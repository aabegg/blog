import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateMinFactory(minValue: number): ValidatorFn {
  return (c: AbstractControl) => {

    const value: number = c.value;

    const isValid = value ? value >= minValue : false;

    if (isValid) {
        return null;
    } else {
        return {
            required: {
                valid: false,
                message: 'O valor mínimo maximo para o campo é ' + minValue
            }
        }
    }
  }
}

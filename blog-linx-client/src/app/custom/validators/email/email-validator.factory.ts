import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateEmailFactory(): ValidatorFn {
  return (c: AbstractControl) => {
    const patern =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const value = c.value;

    const isValid = value ? patern.test(c.value) : true;

    if (isValid) {
        return null;
    } else {
        return {
            email: {
                valid: false,
                message: 'O Email informado não está num formato válido'
            }
        }
    }
  }
}

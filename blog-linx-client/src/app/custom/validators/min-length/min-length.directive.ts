import { Directive, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';

import { validateMinLengthFactory } from './min-length.factory';

@Directive({
  selector: '[appMinLength][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: MinLengthDirective, multi: true }
  ]
})
export class MinLengthDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateMinLengthFactory(0);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}

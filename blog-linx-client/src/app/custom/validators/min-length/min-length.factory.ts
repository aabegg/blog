import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateMinLengthFactory(size: number): ValidatorFn {
  return (c: AbstractControl) => {

    const value: String = c.value;

    const isValid = value ? value.length >= size : false;

    if (isValid) {
        return null;
    } else {
        return {
            required: {
                valid: false,
                message: 'O campo deve conter pelo menos ' + size + ' caracteres'
            }
        }
    }
  }
}

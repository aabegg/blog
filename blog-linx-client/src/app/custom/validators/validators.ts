import { validateMaxFactory } from './max/max.factory';
import { validateMinFactory } from './min/min.factory';
import { validateMaxLengthFactory } from './max-length/max-length.factory';
import { ValidatorFn } from '@angular/forms';
import { validateRequiredFactory } from './required/required-validator.factory';
import { validateMinLengthFactory } from './min-length/min-length.factory';
import { validateEmailFactory } from './email/email-validator.factory';
import { RequiredValidatorDirective } from './required/required-validator.directive';
export abstract class AppValidators {

  static required(): ValidatorFn {
    return validateRequiredFactory();
  }

  static minLength(size): ValidatorFn {
    return validateMinLengthFactory(size);
  }

  static maxLength(size): ValidatorFn {
    return validateMaxLengthFactory(size);
  }

  static min(minValue): ValidatorFn {
    return validateMinFactory(minValue);
  }

  static max(maxValue): ValidatorFn {
    return validateMaxFactory(maxValue);
  }

  static email(): ValidatorFn {
    return validateEmailFactory();
  }
}

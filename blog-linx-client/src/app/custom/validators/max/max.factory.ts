import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateMaxFactory(maxValue: number): ValidatorFn {
  return (c: AbstractControl) => {

    const value: number = c.value;

    const isValid = value ? value <= maxValue : false;

    if (isValid) {
        return null;
    } else {
        return {
            required: {
                valid: false,
                message: 'O valor máximo para o campo é ' + maxValue
            }
        }
    }
  }
}

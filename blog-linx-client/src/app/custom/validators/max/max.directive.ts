import { Directive, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';

import { validateMaxFactory } from './max.factory';

@Directive({
  selector: '[appMax][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: MaxDirective, multi: true }
  ]
})
export class MaxDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateMaxFactory(0);
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}

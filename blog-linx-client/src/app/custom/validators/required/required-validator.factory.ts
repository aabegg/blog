import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateRequiredFactory(): ValidatorFn {
  return (c: AbstractControl) => {

    const isValid = c.value !== undefined && c.value !== null && c.value !== '';

    if (isValid) {
        return null;
    } else {
        return {
            required: {
                valid: false,
                message: 'O Campo é obrigatório'
            }
        }
    }
  }
}

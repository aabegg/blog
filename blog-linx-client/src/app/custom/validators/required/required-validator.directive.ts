import { Directive, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';

import { validateRequiredFactory } from './required-validator.factory';

@Directive({
  selector: '[appRequired][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: RequiredValidatorDirective, multi: true }
  ]
})
export class RequiredValidatorDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateRequiredFactory();
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}

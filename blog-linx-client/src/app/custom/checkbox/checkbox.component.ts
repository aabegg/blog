import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ElementRef, forwardRef } from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';

import { UUID } from 'angular2-uuid';

import { AbstractValueAccessor } from './../util/abstract-value-accessor';

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CheckboxComponent),
  multi: true
};

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [ CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR ]
})
export class CheckboxComponent extends AbstractValueAccessor implements OnInit {

  @Input() public id = UUID.UUID();

  constructor(element: ElementRef) {
    super(element);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  change() {
    this.value = !this.value;
  }
}

import { FormControl } from '@angular/forms';
import { Component, OnInit, ElementRef, Input, ViewChild } from '@angular/core';

import { AbstractComponent } from './../util/abstract-component';

@Component({
  selector: 'app-field-validator',
  templateUrl: './field-validator.component.html',
  styleUrls: ['./field-validator.component.scss']
})
export class FieldValidatorComponent extends AbstractComponent implements OnInit {
  @Input() control: FormControl;

  @Input() promptField: string;

  messages: Array<string>;

  constructor(element: ElementRef) {
    super(element);
  }

  ngOnInit() {
    super.addClasses(['invalid-feedback']);
    this.registerMessagesProcessor();
    this.processMessages();
  }

  private registerMessagesProcessor() {
    this.control.valueChanges.subscribe(() => {
      this.processMessages();
    });
  }

  private isTouched() {
    return this.control.touched;
  }

  private processMessages() {
    const errors = this.control.errors;
    this.messages = new Array<string>();

    if (errors) {
      Object.keys(errors).forEach(error => {
        const message = errors[error].message;

        if (message) {
          this.messages.push(message);
        }
      });
    }
  }
}

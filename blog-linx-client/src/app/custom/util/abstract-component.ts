import { UUID } from 'angular2-uuid';
import { ElementRef, Input, OnInit } from '@angular/core';

export abstract class AbstractComponent implements OnInit {

  @Input() public prompt: string;

  @Input() public id = UUID.UUID();

  @Input() col = 12;

  @Input() right = false;

  private _nativeElement: any;

  constructor(elementRef: ElementRef) {
    this._nativeElement = elementRef.nativeElement;
  }

  ngOnInit() {
    this.addStyle('display', 'block');

    if (this.col) {
      this.addClass('col-md-' + this.col);
    }

    if (this.right) {
      this.addClass('pull-right');
    }
  }

  public addClass(cssClass: string) {
    this._nativeElement.classList.add(cssClass);
  }

  public addClasses(cssClasses: Array<string>) {
    cssClasses.forEach(cssClass => {
      this.addClass(cssClass);
    });
  }

  public removeClasss(cssClass: string) {
    this._nativeElement.classList.remove(cssClass);
  }

  public addStyle(styleProperty: string, valueProperty: string) {
    this._nativeElement.style[styleProperty] = valueProperty;
  }
}

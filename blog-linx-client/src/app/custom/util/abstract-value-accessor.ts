import { AbstractComponent } from './abstract-component';
import { Provider, forwardRef, EventEmitter, Output, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export abstract class AbstractValueAccessor extends AbstractComponent implements ControlValueAccessor {
  @Output('ngModelChange') public ngModelChange = new EventEmitter();

  private _value: any = '';
  public isDisabled = false;

  get value(): any { return this._value; };
  set value(value: any) {
    if (value !== this._value) {
      this._value = value;
      this.onChange(value);
    }
  }

  public setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  public emitValue(event) {
    this.ngModelChange.emit(event);
  }

  writeValue(value: any) {
    this._value = value;
    this.onChange(value);
  }

  onChange = (_) => {};
  onTouched = () => {};
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  constructor(elementRef: ElementRef) {
    super(elementRef);
  }
}

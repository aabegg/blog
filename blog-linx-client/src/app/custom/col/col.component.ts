import { element } from 'protractor';
import { Component, OnInit, ElementRef } from '@angular/core';

import { AbstractComponent } from './../util/abstract-component';

@Component({
  selector: 'app-col',
  templateUrl: './col.component.html',
  styleUrls: ['./col.component.scss']
})
export class ColComponent extends AbstractComponent implements OnInit {

  constructor(element: ElementRef) {
    super(element);
  }

  ngOnInit() {
    super.ngOnInit();
  }

}

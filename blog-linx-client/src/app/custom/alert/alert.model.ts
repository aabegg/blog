import { EnumAlert } from './alert.enum';

export interface Alert {
  title?: string,
  message: string,
  type: EnumAlert
}

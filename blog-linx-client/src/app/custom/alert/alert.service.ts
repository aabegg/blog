import { EventEmitter, Injectable, Input } from '@angular/core';

import { timeout } from 'rxjs/operator/timeout';

import { Alert } from './alert.model';
import { EnumAlert } from './alert.enum';

@Injectable()
export class AlertService {

  @Input() timeout = 3000;

  private namedAlerts = new Map<string, (Alert) => void>();

  alerts = new EventEmitter<Alert>();

  constructor() { }

  registerNamedAlert(nameAlert: string, method: (Alert) => void) {
    this.namedAlerts.set(nameAlert, method);
  }

  unregisterNamedAlert(name: string) {

  }

  namedAlert(alertName: string, data: Alert) {
    const method = this.namedAlerts.get(alertName);

    if (method) {
      method(data);
    }
  }

  notify(alert: Alert) {
    this.alerts.emit(alert);
  }

  notifyp(type: EnumAlert, title: string, message: string) {
    this.notify({
      type: type,
      title: title,
      message: message
    });
  }

  notifySuccess(title: string, message: string) {
    this.notifyp(EnumAlert.SUCCESS, title, message);
  }

  notifyError(title: string, message: string) {
    this.notifyp(EnumAlert.DANGER, title, message);
  }

  notifyInfo(title: string, message: string) {
    this.notifyp(EnumAlert.INFO, title, message);
  }

  notifyWarn(title: string, message: string) {
    this.notifyp(EnumAlert.WARNING, title, message);
  }
}

import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs/Rx';

import { EnumAlert } from './alert.enum';
import { Alert } from './alert.model';
import { AlertService } from './alert.service';
import { SubscriptionUtil } from './../../util/subscription-util';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

  private alertSubscriber: Subscription;

  @Input() alertName: string;

  @Input() cacheMessages = 10;

  @Input() timeOut = 2500;

  alerts = new Array<Alert>();

  constructor(
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.registerInService();
  }

  ngOnDestroy() {
    if (this.alertName) {
      this.alertService.unregisterNamedAlert(this.alertName);
    }

    SubscriptionUtil.unsubscribe(this.alertSubscriber);
  }

  getType(enumAlert: EnumAlert) {
    switch (enumAlert) {
      case EnumAlert.DANGER: return 'danger';
      case EnumAlert.INFO: return 'info';
      case EnumAlert.SUCCESS: return 'success';
      case EnumAlert.WARNING: return 'warning';
    }
  }

  private registerInService() {
    if (this.alertName) {
      this.alertService.registerNamedAlert(this.alertName, (alert) => this.show(alert));
    }

    this.alertService.alerts.subscribe(alert => {
      this.show(alert);
    });
  }

  private show(alert: Alert) {
    this.removeInitials();

    this.alerts.push(alert);
  }

  private removeInitials() {
    if (this.alerts.length >= this.cacheMessages) {
      this.alerts.shift();
    }
  }
}

import { element } from 'protractor';
import { Component, OnInit, ElementRef } from '@angular/core';
import { AbstractComponent } from './../util/abstract-component';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent extends AbstractComponent implements OnInit {

  constructor(public element: ElementRef) {
    super(element);
  }

  ngOnInit() {
    this.addStyle('display', 'block-in-line');
    this.addStyle('margin-top', '5px');
    super.addClass('row');
  }

}

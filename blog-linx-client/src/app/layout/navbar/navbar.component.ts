import { MenuItem } from './menu-item.model';
import { appRoutes } from './../../app-routing.module';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public menus = new Array<MenuItem>();

  constructor() { }

  ngOnInit() {
    appRoutes.forEach(item => {
      this.menus.push({
        title: item.data['menu'],
        link: '/' + item.path
      });
    });
  }

}

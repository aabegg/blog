import { HeaderModule } from '../header/header.module';
import { NavbarModule } from './../navbar/navbar.module';
import { RouterModule } from '@angular/router';
import { TemplateComponent } from './template.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavbarModule,
    HeaderModule
  ],
  declarations: [
    TemplateComponent
  ],
  exports: [
    TemplateComponent,
    HeaderModule
  ]
})
export class TemplateModule { }

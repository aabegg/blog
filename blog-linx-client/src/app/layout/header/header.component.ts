import { ActivatedRoute, NavigationEnd, Router, RoutesRecognized } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: string

  constructor(router: Router, route: ActivatedRoute) {
    router.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        const r = event.state.root.firstChild;
        this.header = r.data.header;
      }
    });
  }

  ngOnInit() {
  }

}

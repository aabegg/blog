import { HeaderModule } from './header/header.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarModule } from './navbar/navbar.module';
import { TemplateModule } from './template/template.module';

@NgModule({
  imports: [
    CommonModule,
    TemplateModule
  ],
  exports: [
    TemplateModule
  ],
})
export class LayoutModule { }

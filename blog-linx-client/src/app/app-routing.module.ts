import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CanActivateViaAuthGuard } from './login/can-activate-via-auth-guard.service';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'post', pathMatch: 'full' },
  { path: 'user', loadChildren: 'app/views/user/user.module#UserModule', data: { menu: 'Usuário', header: 'Cadastro de Usuários' } },
  { path: 'post', loadChildren: 'app/views/post/post.module#PostModule', data: { menu: 'Posts', header: 'Cadastro de Posts' } },
  { path: 'comment', loadChildren: 'app/views/comment/comment.module#CommentModule',
    data: { menu: 'Comentários', header: 'Cadastro de Comentários' }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    CanActivateViaAuthGuard
  ]
})
export class AppRoutingModule { }

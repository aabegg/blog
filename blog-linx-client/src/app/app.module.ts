import { AlertService } from './custom/alert/alert.service';
import { PostService } from './views/post/post.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AuthenticationService } from './login/authentication.service';
import { LoginService } from './login/login.service';
import { LoginModule } from './login/login.module';
import { Layout2Module } from './layout2/layout2.module';
import { ResourceModule } from '../assets/lib/ngx-resource';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { CustomModule } from './custom/custom.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    Layout2Module,
    LoginModule,
    ResourceModule.forRoot()
  ],
  providers: [
    AuthenticationService,
    PostService,
    AlertService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

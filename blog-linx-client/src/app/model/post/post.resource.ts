import { Summary } from './summary.model';
import { Post } from './post.model';
import { Http, RequestMethod } from '@angular/http';
import { Injectable } from '@angular/core';

import {Resource, ResourceParams, ResourceAction, ResourceMethod, ResourceMethodStrict } from '../../../assets/lib/ngx-resource';

@Injectable()
@ResourceParams({
  url: 'http://localhost:8080/api/post'
})
export class PostResource extends Resource {

  @ResourceAction({
    path: '/{id}'
  })
  get: ResourceMethod<{id: any}, Post>;

  @ResourceAction({
    isArray: true
  })
  query: ResourceMethod<any, Post[]>;

  @ResourceAction({
    method: RequestMethod.Delete,
    path: '/{!id}'
  })
  remove: ResourceMethod<{id: any}, any>;

  @ResourceAction({
    method: RequestMethod.Post
  })
  save: ResourceMethod<Post, Post>;

  @ResourceAction({
    method: RequestMethod.Put,
    path: '/{!id}'
  })
  update: ResourceMethodStrict<Post, {id: number}, void>;

  @ResourceAction({
    path: '/summary',
    isArray: true
  })
  getFullSummary: ResourceMethod<{}, Array<Summary>>;

  @ResourceAction({
    path: '/lastpost'
  })
  getLastPost: ResourceMethod<{}, Post>;

  constructor(http: Http) {
    super(http);
  }
}

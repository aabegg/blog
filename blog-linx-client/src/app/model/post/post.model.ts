import { User } from './../user/user.model';

export interface Post {
  id?: number,
  idUser?: number;
  userName?: string,
  title: string,
  body: string,
  datePost?: Date,
  numberOfEdits?: number;
}

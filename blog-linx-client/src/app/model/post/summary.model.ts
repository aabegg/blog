export interface Summary {
  idPost: number,
  description: string,
  datePost: Date
}

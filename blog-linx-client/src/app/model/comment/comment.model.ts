import { Post } from '../post/post.model';
import { User } from '../user/user.model';

export interface Comment {
  id?: number
  body: string
  idUser?: number,
  userName?: string,
  commentDate?: Date;
  idPost?: number;
}

import { Comment } from './comment.model';
import { Http, RequestMethod } from '@angular/http';
import { Injectable } from '@angular/core';

import {Resource, ResourceParams, ResourceAction, ResourceMethod, ResourceMethodStrict } from '../../../assets/lib/ngx-resource';

@Injectable()
@ResourceParams({
  url: 'http://localhost:8080/api/post/{idPost}/comment'
})
export class CommentResource extends Resource {

  @ResourceAction({
    path: '/{id}'
  })
  get: ResourceMethod<{id: any}, Comment>;

  @ResourceAction({
    isArray: true
  })
  query: ResourceMethod<{idPost: number}, Array<Comment>>;

  @ResourceAction({
    method: RequestMethod.Delete,
    path: '/{!id}'
  })
  remove: ResourceMethod<{id: any}, any>;

  @ResourceAction({
    method: RequestMethod.Post
  })
  save: ResourceMethodStrict<Comment, {idPost: number}, void>;

  @ResourceAction({
    method: RequestMethod.Put,
    path: '/{!id}'
  })
  update: ResourceMethodStrict<Comment, {id: number}, void>;

  constructor(http: Http) {
    super(http);
  }
}

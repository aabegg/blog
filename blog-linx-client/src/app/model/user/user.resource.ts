import { Http, RequestMethod } from '@angular/http';
import { Injectable } from '@angular/core';

import {Resource, ResourceParams, ResourceAction, ResourceMethod, ResourceMethodStrict } from '../../../assets/lib/ngx-resource';

import { User } from './user.model';

@Injectable()
@ResourceParams({
  url: 'http://localhost:8080/api/user'
})
export class UserResource extends Resource {

  @ResourceAction({
    path: '/{id}'
  })
  get: ResourceMethod<{id: any}, User>;

  @ResourceAction({
    isArray: true
  })
  query: ResourceMethod<any, User[]>;

  @ResourceAction({
    method: RequestMethod.Delete,
    path: '/{!id}'
  })
  remove: ResourceMethod<{id: any}, any>;

  @ResourceAction({
    method: RequestMethod.Post
  })
  save: ResourceMethod<User, number>;

  @ResourceAction({
    method: RequestMethod.Put,
    path: '/{!id}'
  })
  update: ResourceMethodStrict<User, {id: number}, void>;

  constructor(http: Http) {
    super(http);
  }
}

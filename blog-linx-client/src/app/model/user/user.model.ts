export interface User {
  id: number,
  name: string,
  lastName: string,
  login: string
  password: string,
  registerDate: Date,
  active: boolean,
  imageUrl: string,
  email: string
}

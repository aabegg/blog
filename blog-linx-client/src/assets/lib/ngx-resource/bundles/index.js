(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@angular/http"), require("@angular/core"), require("@angular/common"), require("rxjs/Observable"), require("rxjs/add/operator/map"), require("rxjs/add/operator/mergeMap"), require("rxjs/add/operator/publish"));
	else if(typeof define === 'function' && define.amd)
		define(["@angular/http", "@angular/core", "@angular/common", "rxjs/Observable", "rxjs/add/operator/map", "rxjs/add/operator/mergeMap", "rxjs/add/operator/publish"], factory);
	else if(typeof exports === 'object')
		exports["ngx-resource"] = factory(require("@angular/http"), require("@angular/core"), require("@angular/common"), require("rxjs/Observable"), require("rxjs/add/operator/map"), require("rxjs/add/operator/mergeMap"), require("rxjs/add/operator/publish"));
	else
		root["ngx-resource"] = factory(root["@angular/http"], root["@angular/core"], root["@angular/common"], root["rxjs/Observable"], root["rxjs/add/operator/map"], root["rxjs/add/operator/mergeMap"], root["rxjs/add/operator/publish"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_13__, __WEBPACK_EXTERNAL_MODULE_14__, __WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_16__, __WEBPACK_EXTERNAL_MODULE_17__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_publish__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_publish___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_publish__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__ = __webpack_require__(5);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Resource; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Resource = Resource_1 = (function () {
    function Resource(http) {
        this.http = http;
        this.$url = null;
        this.$path = null;
        this.$headers = null;
        this.$params = null;
        this.$data = null;
    }
    Resource.$cleanData = function (obj) {
        for (var propName in obj) {
            if ((obj[propName] instanceof Function) || this.$cleanDataFields.indexOf(propName) > -1) {
                delete obj[propName];
            }
        }
        return obj;
    };
    /**
     * Get main url of the resource
     * @returns {string|Promise<string>}
     */
    Resource.prototype.$getUrl = function (methodOptions) {
        return this.$url || this.$_getUrl(methodOptions) || __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].url || '';
    };
    /**
     * Set resource url
     * @param url
     */
    Resource.prototype.$setUrl = function (url) {
        this.$url = url;
    };
    /**
     * Get path of the resource
     * @returns {string|Promise<string>}
     */
    Resource.prototype.$getPath = function (methodOptions) {
        return this.$path || this.$_getPath(methodOptions) || __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].path || '';
    };
    /**
     * Set resource path
     * @param path
     */
    Resource.prototype.$setPath = function (path) {
        this.$path = path;
    };
    /**
     * Get headers
     * @returns {any|Promise<any>}
     */
    Resource.prototype.$getHeaders = function (methodOptions) {
        return this.$headers || this.$_getHeaders(methodOptions) || __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].headers || {};
    };
    /**
     * Set resource headers
     * @param headers
     */
    Resource.prototype.$setHeaders = function (headers) {
        this.$headers = headers;
    };
    /**
     * Get default params
     * @returns {any|Promise<any>|{}}
     */
    Resource.prototype.$getParams = function (methodOptions) {
        return this.$params || this.$_getParams(methodOptions) || __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].params || {};
    };
    /**
     * Set default resource params
     * @param params
     */
    Resource.prototype.$setParams = function (params) {
        this.$params = params;
    };
    /**
     * Get default data
     * @returns {any|Promise<any>|{}}
     */
    Resource.prototype.$getData = function (methodOptions) {
        return this.$data || this.$_getData(methodOptions) || __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].data || {};
    };
    /**
     * Set default resource params
     * @param data
     */
    Resource.prototype.$setData = function (data) {
        this.$data = data;
    };
    /**
     * Create the model
     *
     * @returns {any}
     */
    Resource.prototype.$createModel = function () {
        var ret = this.$initResultObject();
        ret.$resource = this;
        return ret;
    };
    /**
     * That is called before executing request
     * @param req
     */
    Resource.prototype.$requestInterceptor = function (req, methodOptions) {
        return req;
    };
    /**
     * Request observable interceptor
     * @param observable
     * @returns {Observable<any>}
     */
    Resource.prototype.$responseInterceptor = function (observable, req, methodOptions) {
        return observable.map(function (res) { return res._body ? res.json() : null; });
    };
    // removeTrailingSlash(): boolean {
    //   return true;
    // }
    Resource.prototype.$initResultObject = function (methodOptions) {
        if (methodOptions === void 0) { methodOptions = null; }
        return {};
    };
    Resource.prototype.$map = function (item) {
        return item;
    };
    Resource.prototype.$filter = function (item) {
        return true;
    };
    Resource.prototype.$getResourceOptions = function () {
        return null;
    };
    Resource.prototype.$request = function (req, methodOptions) {
        if (methodOptions === void 0) { methodOptions = {}; }
        var requestObservable = this.http.request(req);
        // noinspection TypeScriptValidateTypes
        return methodOptions.responseInterceptor ?
            methodOptions.responseInterceptor(requestObservable, req, methodOptions) :
            this.$responseInterceptor(requestObservable, req, methodOptions);
    };
    Resource.prototype.$resourceAction = function (data, params, callback, methodOptions) {
        var shell = {
            returnInternal: this.$_createReturnData(data, methodOptions),
            data: data,
            params: params,
            options: methodOptions,
            callback: callback
        };
        shell.returnExternal = methodOptions.lean ? this.$_createReturnData(data, methodOptions) : shell.returnInternal;
        this.$_cleanData(shell);
        this.$_fillInternal(shell);
        this.$_mainRequest(shell);
        return shell.returnExternal;
    };
    Resource.prototype.$_createReturnData = function (data, methodOptions) {
        if (methodOptions.isLazy) {
            return {};
        }
        if (methodOptions.isArray) {
            return [];
        }
        if (methodOptions.lean || !data || data.$resource !== this) {
            return this.$_initResultObject(methodOptions);
        }
        return data;
    };
    Resource.prototype.$_initResultObject = function (methodOptions) {
        return methodOptions.initResultObject ? methodOptions.initResultObject() : this.$initResultObject(methodOptions);
    };
    Resource.prototype.$_map = function (methodOptions) {
        return methodOptions.map ? methodOptions.map : this.$map;
    };
    Resource.prototype.$_filter = function (methodOptions) {
        return methodOptions.filter ? methodOptions.filter : this.$filter;
    };
    Resource.prototype.$_cleanData = function (shell) {
        if (shell.data && !shell.options.skipDataCleaning) {
            shell.data = shell.data.toJSON ? shell.data.toJSON() : Resource_1.$cleanData(shell.data);
        }
    };
    Resource.prototype.$_fillInternal = function (shell) {
        var returnInternal = shell.returnInternal;
        returnInternal.$resolved = false;
        returnInternal.$observable = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (subscriber) {
            shell.mainDeferredSubscriber = subscriber;
        }).flatMap(function () { return shell.mainObservable; });
        returnInternal.$abortRequest = function () {
            returnInternal.$resolved = true;
        };
        returnInternal.$resource = this;
        if (!shell.options.isLazy) {
            returnInternal.$observable = returnInternal.$observable.publish();
            returnInternal.$observable.connect();
        }
    };
    Resource.prototype.$_mainRequest = function (shell) {
        var _this = this;
        this.$_resolveMainOptions(shell)
            .then(function (extraOptions) {
            shell.extraOptions = extraOptions;
            if (shell.returnInternal.$resolved) {
                shell.mainObservable = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (subscriber) {
                    subscriber.next(null);
                });
                _this.$_releaseMainDeferredSubscriber(shell);
                return;
            }
            shell.url = extraOptions.url + extraOptions.path;
            _this.$_prepareDataParams(shell);
            _this.$_prepareBody(shell);
            _this.$_prepareSearch(shell);
            _this.$_createRequestOptions(shell);
            var mainRequest = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Request"](shell.requestOptions);
            mainRequest = shell.options.requestInterceptor ?
                shell.options.requestInterceptor(mainRequest, shell.options) :
                _this.$requestInterceptor(mainRequest, shell.options);
            if (!mainRequest) {
                shell.mainObservable = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
                    observer.error(new Error('Request is null'));
                });
                console.warn('Request is null');
                _this.$_releaseMainDeferredSubscriber(shell);
                return;
            }
            // Doing the request
            var requestObservable = _this.$request(mainRequest, shell.options);
            shell.mainObservable = shell.options.isLazy ? requestObservable : _this.$_createMainObservable(shell, requestObservable);
            _this.$_releaseMainDeferredSubscriber(shell);
        });
    };
    Resource.prototype.$_resolveMainOptions = function (shell) {
        return Promise
            .all([
            Promise.resolve(shell.options.url || this.$getUrl(shell.options)),
            Promise.resolve(shell.options.path || this.$getPath(shell.options)),
            Promise.resolve(shell.options.headers || this.$getHeaders(shell.options)),
            Promise.resolve(shell.options.params || this.$getParams(shell.options)),
            Promise.resolve(shell.options.data || this.$getData(shell.options))
        ])
            .then(function (data) {
            return {
                url: data[0],
                path: data[1],
                headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"](data[2] ? Object.assign({}, data[2]) : data[2]),
                params: data[3] ? Object.assign({}, data[3]) : data[3],
                data: data[4]
            };
        });
    };
    Resource.prototype.$_releaseMainDeferredSubscriber = function (shell) {
        if (shell.mainDeferredSubscriber) {
            shell.mainDeferredSubscriber.next();
            shell.mainDeferredSubscriber.complete();
            shell.mainDeferredSubscriber = null;
        }
    };
    Resource.prototype.$_prepareDataParams = function (shell) {
        var usedPathParams = {};
        shell.usedPathParams = usedPathParams;
        if (!Array.isArray(shell.data) || shell.params) {
            if (!Array.isArray(shell.data)) {
                shell.data = Object.assign({}, shell.extraOptions.data, shell.data);
            }
            var pathParams = shell.url.match(/{([^}]*)}/g) || [];
            var _loop_1 = function (i) {
                var pathParam = pathParams[i];
                var pathKey = pathParam.substr(1, pathParam.length - 2);
                var isMandatory = pathKey[0] === '!';
                if (isMandatory) {
                    pathKey = pathKey.substr(1);
                }
                var isGetOnly = pathKey[0] === ':';
                if (isGetOnly) {
                    pathKey = pathKey.substr(1);
                }
                var value = this_1.$_getValueForPath(pathKey, shell.extraOptions.params, shell.params || shell.data, usedPathParams);
                if (isGetOnly && !shell.params) {
                    delete shell.data[pathKey];
                }
                if (this_1.$_isNullOrUndefined(value)) {
                    if (isMandatory) {
                        var consoleMsg_1 = "Mandatory " + pathParam + " path parameter is missing";
                        shell.mainObservable = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
                            observer.error(new Error(consoleMsg_1));
                        });
                        console.warn(consoleMsg_1);
                        this_1.$_releaseMainDeferredSubscriber(shell);
                        throw new Error(consoleMsg_1);
                    }
                    shell.url = shell.url.substr(0, shell.url.indexOf(pathParam));
                    return "break";
                }
                // Replacing in the url
                shell.url = shell.url.replace(pathParam, value);
            };
            var this_1 = this;
            for (var i = 0; i < pathParams.length; i++) {
                var state_1 = _loop_1(i);
                if (state_1 === "break")
                    break;
            }
        }
        // Removing double slashed from final url
        shell.url = shell.url.replace(/\/\/+/g, '/');
        if (shell.url.startsWith('http')) {
            shell.url = shell.url.replace(':/', '://');
        }
        // Remove trailing slash
        if (typeof shell.options.removeTrailingSlash === 'undefined') {
            shell.options.removeTrailingSlash = true;
        }
        if (shell.options.removeTrailingSlash) {
            while (shell.url[shell.url.length - 1] === '/') {
                shell.url = shell.url.substr(0, shell.url.length - 1);
            }
        }
        // Remove mapped params
        for (var key in shell.extraOptions.params) {
            if (shell.extraOptions.params[key][0] === '@') {
                delete shell.extraOptions.params[key];
            }
        }
    };
    Resource.prototype.$_prepareBody = function (shell) {
        if (shell.options.method === __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestMethod"].Get) {
            // GET
            shell.searchParams = Object.assign({}, shell.extraOptions.params, shell.data);
        }
        else {
            // NON GET
            if (shell.data) {
                var _body = {};
                if (shell.options.rootNode) {
                    _body["" + shell.options.rootNode] = shell.data;
                }
                else {
                    _body = shell.data;
                }
                shell.body = JSON.stringify(_body);
            }
            shell.searchParams = shell.params;
        }
        if (!shell.body) {
            shell.extraOptions.headers.delete('content-type');
        }
    };
    Resource.prototype.$_prepareSearch = function (shell) {
        shell.search = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["URLSearchParams"]();
        if (!shell.params) {
            for (var key in shell.searchParams) {
                if (shell.searchParams.hasOwnProperty(key) && !shell.usedPathParams[key]) {
                    var value = shell.searchParams[key];
                    this.$_appendSearchParams(shell.search, key, value);
                }
            }
        }
        var tsName = shell.options.addTimestamp;
        if (tsName) {
            if (tsName === true) {
                tsName = 'ts';
            }
            shell.search.append(tsName, '' + Date.now());
        }
    };
    Resource.prototype.$_createRequestOptions = function (shell) {
        shell.requestOptions = {
            method: shell.options.method,
            headers: shell.extraOptions.headers,
            body: shell.body,
            url: shell.url,
            withCredentials: shell.options.withCredentials
        };
        if (shell.options.angularV2) {
            shell.requestOptions.search = shell.search;
        }
        else {
            shell.requestOptions.params = shell.search;
        }
    };
    Resource.prototype.$_createMainObservable = function (shell, requestObservable) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (subscriber) {
            var reqSubscr = requestObservable.subscribe(function (resp) {
                var filter = _this.$_filter(shell.options);
                var map = _this.$_map(shell.options);
                if (resp !== null) {
                    if (shell.options.isArray) {
                        // Expecting array
                        if (!Array.isArray(resp)) {
                            console.error('Returned data should be an array. Received', resp);
                        }
                        else {
                            (_a = shell.returnExternal).push.apply(_a, resp
                                .filter(filter)
                                .map(map)
                                .map(function (respItem) {
                                if (!shell.options.lean) {
                                    respItem.$resource = _this;
                                }
                                return _this.$_setDataToObject(_this.$_initResultObject(shell.options), respItem);
                            }));
                        }
                    }
                    else {
                        // Expecting object
                        if (Array.isArray(resp)) {
                            console.error('Returned data should be an object. Received', resp);
                        }
                        else {
                            if (filter(resp)) {
                                _this.$_setDataToObject(shell.returnExternal, map(resp));
                            }
                        }
                    }
                }
                shell.returnInternal.$resolved = true;
                subscriber.next(shell.returnExternal);
                var _a;
            }, function (err) { return subscriber.error(err); }, function () {
                shell.returnInternal.$resolved = true;
                subscriber.complete();
                if (shell.callback) {
                    shell.callback(shell.returnExternal);
                }
            });
            shell.returnInternal.$abortRequest = function () {
                if (shell.returnInternal.$resolved) {
                    return;
                }
                reqSubscr.unsubscribe();
                shell.returnInternal.$resolved = true;
            };
        });
    };
    Resource.prototype.$_setDataToObject = function (ret, resp) {
        if (ret.$setData) {
            ret.$setData(resp);
        }
        else {
            if (Array.isArray(resp)) {
                ret = resp;
            }
            else {
                Object.assign(ret, resp);
            }
        }
        return ret;
    };
    Resource.prototype.$_getValueForPath = function (key, params, data, usedPathParams) {
        if (!this.$_isNullOrUndefined(data[key]) && typeof data[key] !== 'object') {
            usedPathParams[key] = true;
            return data[key];
        }
        if (this.$_isNullOrUndefined(params[key])) {
            return null;
        }
        if (params[key][0] === '@') {
            return this.$_getValueForPath(params[key].substr(1), params, data, usedPathParams);
        }
        usedPathParams[key] = true;
        return params[key];
    };
    Resource.prototype.$_isNullOrUndefined = function (value) {
        return value === null || value === undefined;
    };
    Resource.prototype.$_appendSearchParams = function (search, key, value) {
        /// Convert dates to ISO format string
        if (value instanceof Date) {
            search.append(key, value.toISOString());
            return;
        }
        if (typeof value === 'object') {
            switch (__WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["b" /* ResourceGlobalConfig */].getParamsMappingType) {
                case __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["a" /* TGetParamsMappingType */].Plain:
                    if (Array.isArray(value)) {
                        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
                            var arr_value = value_1[_i];
                            search.append(key, arr_value);
                        }
                    }
                    else {
                        if (value && typeof value === 'object') {
                            /// Convert dates to ISO format string
                            if (value instanceof Date) {
                                value = value.toISOString();
                            }
                            else {
                                value = JSON.stringify(value);
                            }
                        }
                        search.append(key, value);
                    }
                    break;
                case __WEBPACK_IMPORTED_MODULE_6__ResourceGlobalConfig__["a" /* TGetParamsMappingType */].Bracket:
                    /// Convert object and arrays to query params
                    for (var k in value) {
                        if (value.hasOwnProperty(k)) {
                            this.$_appendSearchParams(search, key + '[' + k + ']', value[k]);
                        }
                    }
                    break;
            }
            return;
        }
        search.append(key, value);
    };
    /**
     * Get url to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    Resource.prototype.$_getUrl = function (methodOptions) {
        return null;
    };
    /**
     * Get get path to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    Resource.prototype.$_getPath = function (methodOptions) {
        return null;
    };
    /**
     * Get headers to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    Resource.prototype.$_getHeaders = function (methodOptions) {
        return null;
    };
    /**
     * Get params to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    Resource.prototype.$_getParams = function (methodOptions) {
        return null;
    };
    /**
     * Get data to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    Resource.prototype.$_getData = function (methodOptions) {
        return null;
    };
    return Resource;
}());
Resource.$cleanDataFields = [
    '$cleanDataFields',
    '$resolved',
    '$observable',
    '$abortRequest',
    '$resource'
];
Resource = Resource_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], Resource);

var Resource_1;


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_http__);
/* harmony export (immutable) */ __webpack_exports__["a"] = ResourceAction;

function ResourceAction(methodOptions) {
    methodOptions = methodOptions || {};
    if (methodOptions.method === undefined) {
        methodOptions.method = __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Get;
    }
    return function (target, propertyKey) {
        target[propertyKey] = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var data = args.length ? args[0] : null;
            var params = args.length > 1 ? args[1] : null;
            var callback = args.length > 2 ? args[2] : null;
            if (typeof data === 'function') {
                callback = data;
                data = null;
            }
            else if (typeof params === 'function') {
                callback = params;
                params = null;
            }
            var options = Object.assign({}, this.getResourceOptions(), methodOptions);
            return this.$resourceAction(data, params, callback, options);
            //   let resourceOptions = this.getResourceOptions();
            //
            //   let isGetRequest = methodOptions.method === RequestMethod.Get;
            //
            //   let ret: ResourceResult<any> | ResourceModel<Resource> = null;
            //
            //   let map: ResourceResponseMap = methodOptions.map ? methodOptions.map : this.map;
            //   let filter: ResourceResponseFilter = methodOptions.filter ? methodOptions.filter : this.filter;
            //   let initObject: ResourceResponseInitResult = methodOptions.initResultObject ?
            //     methodOptions.initResultObject : this.$initResultObject;
            //
            //   if (methodOptions.isLazy) {
            //     ret = {};
            //   } else {
            //
            //     if (methodOptions.isArray) {
            //       ret = [];
            //     } else {
            //
            //       if (data && data.$resource === this) {
            //         // Setting data to ret
            //         ret = data;
            //       } else {
            //         ret = initObject();
            //       }
            //
            //     }
            //   }
            //
            //   if (data && !methodOptions.skipDataCleaning) {
            //     data = data.toJSON ? data.toJSON() : this.cleanData(data);
            //   }
            //
            //   let mainDeferredSubscriber: Subscriber<any> = null;
            //   let mainObservable: Observable<Response> = null;
            //
            //   ret.$resolved = false;
            //   ret.$observable = Observable.create((subscriber: Subscriber<any>) => {
            //     mainDeferredSubscriber = subscriber;
            //   }).flatMap(() => mainObservable);
            //   ret.$abortRequest = () => {
            //     ret.$resolved = true;
            //   };
            //   ret.$resource = this;
            //
            //   let returnData = ret;
            //
            //   if (methodOptions.lean) {
            //
            //   }
            //
            //
            //   function releaseMainDeferredSubscriber() {
            //     if (mainDeferredSubscriber) {
            //       mainDeferredSubscriber.next();
            //       mainDeferredSubscriber.complete();
            //       mainDeferredSubscriber = null;
            //     }
            //   }
            //
            //   if (!methodOptions.isLazy) {
            //     ret.$observable = ret.$observable.publish();
            //     (<ConnectableObservable<any>>ret.$observable).connect();
            //   }
            //
            //   Promise.all([
            //     Promise.resolve(methodOptions.url || this.$getUrl(methodOptions)),
            //     Promise.resolve(methodOptions.path || this.$getPath(methodOptions)),
            //     Promise.resolve(methodOptions.headers || this.$getHeaders(methodOptions)),
            //     Promise.resolve(methodOptions.params || this.$getParams(methodOptions)),
            //     Promise.resolve(methodOptions.data || this.getData(methodOptions))
            //   ])
            //     .then((dataAll: any[]) => {
            //
            //       if (ret.$resolved) {
            //         mainObservable = Observable.create((observer: any) => {
            //           observer.next(null);
            //         });
            //
            //         releaseMainDeferredSubscriber();
            //       }
            //
            //       let url: string = dataAll[0] + dataAll[1];
            //       let headers = new Headers(dataAll[2]);
            //       let defPathParams = dataAll[3];
            //
            //       let usedPathParams: any = {};
            //
            //       if (!Array.isArray(data) || params) {
            //
            //         if (!Array.isArray(data)) {
            //           data = Object.assign({}, dataAll[4], data);
            //         }
            //
            //         let pathParams = url.match(/{([^}]*)}/g) || [];
            //
            //         for (let i = 0; i < pathParams.length; i++) {
            //
            //           let pathParam = pathParams[i];
            //
            //           let pathKey = pathParam.substr(1, pathParam.length - 2);
            //           let isMandatory = pathKey[0] === '!';
            //           if (isMandatory) {
            //             pathKey = pathKey.substr(1);
            //           }
            //
            //           let isGetOnly = pathKey[0] === ':';
            //           if (isGetOnly) {
            //             pathKey = pathKey.substr(1);
            //           }
            //
            //           let value = getValueForPath(pathKey, defPathParams, params || data, usedPathParams);
            //           if (isGetOnly && !params) {
            //             delete data[pathKey];
            //           }
            //
            //           if (isNullOrUndefined(value)) {
            //             if (isMandatory) {
            //
            //               let consoleMsg = `Mandatory ${pathParam} path parameter is missing`;
            //
            //               mainObservable = Observable.create((observer: any) => {
            //                 observer.error(new Error(consoleMsg));
            //               });
            //
            //               console.warn(consoleMsg);
            //
            //               releaseMainDeferredSubscriber();
            //               return;
            //
            //             }
            //             url = url.substr(0, url.indexOf(pathParam));
            //             break;
            //           }
            //
            //           // Replacing in the url
            //           url = url.replace(pathParam, value);
            //         }
            //
            //       }
            //
            //       // Removing double slashed from final url
            //       url = url.replace(/\/\/+/g, '/');
            //       if (url.startsWith('http')) {
            //         url = url.replace(':/', '://');
            //       }
            //
            //       // Remove trailing slash
            //       if (typeof methodOptions.removeTrailingSlash === 'undefined') {
            //         methodOptions.removeTrailingSlash = this.removeTrailingSlash();
            //       }
            //       if (methodOptions.removeTrailingSlash) {
            //         while (url[url.length - 1] === '/') {
            //           url = url.substr(0, url.length - 1);
            //         }
            //       }
            //
            //
            //       // Remove mapped params
            //       for (let key in defPathParams) {
            //         if (defPathParams[key][0] === '@') {
            //           delete defPathParams[key];
            //         }
            //       }
            //
            //
            //       // Default search params or data
            //       let body: string = null;
            //
            //       let searchParams: any;
            //       if (isGetRequest) {
            //         // GET
            //         searchParams = Object.assign({}, defPathParams, data);
            //       } else {
            //         // NON GET
            //         if (data) {
            //           let _body: any = {};
            //           if (methodOptions.rootNode) {
            //             _body[`${methodOptions.rootNode}`] = data;
            //           } else {
            //             _body = data;
            //           }
            //           body = JSON.stringify(_body);
            //         }
            //         searchParams = defPathParams;
            //       }
            //
            //
            //       // Setting search params
            //       let search: URLSearchParams = new URLSearchParams();
            //
            //       if (!params) {
            //         for (let key in searchParams) {
            //           if (searchParams.hasOwnProperty(key) && !usedPathParams[key]) {
            //             let value: any = searchParams[key];
            //             appendSearchParams(search, key, value);
            //           }
            //         }
            //       }
            //
            //       // Adding TS if needed
            //       let tsName = methodOptions.addTimestamp || resourceOptions.addTimestamp;
            //       if (tsName) {
            //         if (tsName === true) {
            //           tsName = 'ts';
            //         }
            //         search.append(tsName, '' + new Date().getTime());
            //       }
            //
            //       // Removing Content-Type header if no body
            //       if (!body) {
            //         headers.delete('content-type');
            //       }
            //
            //
            //       // Creating request options
            //       let requestOptions = new RequestOptions({
            //         method: methodOptions.method,
            //         headers: headers,
            //         body: body,
            //         url: url,
            //         search: search,
            //         withCredentials: methodOptions.withCredentials || resourceOptions.withCredentials
            //       });
            //
            //
            //       // Creating request object
            //       let req = new Request(requestOptions);
            //
            //       req = methodOptions.requestInterceptor ?
            //         methodOptions.requestInterceptor(req, methodOptions) :
            //         this.$requestInterceptor(req, methodOptions);
            //
            //       if (!req) {
            //         mainObservable = Observable.create((observer: any) => {
            //           observer.error(new Error('Request is null'));
            //         });
            //
            //         console.warn('Request is null');
            //
            //         releaseMainDeferredSubscriber();
            //         return;
            //       }
            //
            //       // Doing the request
            //       let requestObservable = this._request(req, methodOptions);
            //
            //
            //       if (methodOptions.isLazy) {
            //
            //         mainObservable = requestObservable;
            //
            //       } else {
            //
            //         mainObservable = Observable.create((subscriber: Subscriber<any>) => {
            //
            //           let reqSubscr: Subscription = requestObservable.subscribe(
            //             (resp: any) => {
            //
            //               if (resp !== null) {
            //
            //                 if (methodOptions.isArray) {
            //
            //                   // Expecting array
            //
            //                   if (!Array.isArray(resp)) {
            //                     console.error('Returned data should be an array. Received', resp);
            //                   } else {
            //
            //                     returnData.push(
            //                       ...resp
            //                         .filter(filter)
            //                         .map(map)
            //                         .map((respItem: any) => {
            //                           if (!methodOptions.lean) {
            //                             respItem.$resource = this;
            //                           }
            //                           return setDataToObject(initObject(), respItem);
            //                         })
            //                     );
            //
            //                   }
            //
            //                 } else {
            //
            //                   // Expecting object
            //
            //                   if (Array.isArray(resp)) {
            //                     console.error('Returned data should be an object. Received', resp);
            //                   } else {
            //
            //                     if (filter(resp)) {
            //
            //                       setDataToObject(returnData, map(resp));
            //
            //                     }
            //
            //                   }
            //                 }
            //               }
            //
            //               ret.$resolved = true;
            //               subscriber.next(returnData);
            //
            //             },
            //             (err: any) => subscriber.error(err),
            //             () => {
            //               ret.$resolved = true;
            //               subscriber.complete();
            //               if (callback) {
            //                 callback(returnData);
            //               }
            //             }
            //           );
            //
            //           ret.$abortRequest = () => {
            //             if (ret.$resolved) {
            //               return;
            //             }
            //             reqSubscr.unsubscribe();
            //             ret.$resolved = true;
            //           };
            //
            //         });
            //
            //       }
            //
            //       releaseMainDeferredSubscriber();
            //
            //     });
            //
            //   return returnData;
        };
    };
}
// export function setDataToObject(ret: any, resp: any): any {
//
//   if (ret.$setData) {
//     ret.$setData(resp);
//   } else {
//     Object.assign(ret, resp);
//   }
//
//   return ret;
//
// }
// export function appendSearchParams(search: URLSearchParams, key: string, value: any): void {
//   /// Convert dates to ISO format string
//   if (value instanceof Date) {
//     search.append(key, value.toISOString());
//     return;
//   }
//
//   if (typeof value === 'object') {
//
//     switch (ResourceGlobalConfig.getParamsMappingType) {
//
//       case TGetParamsMappingType.Plain:
//
//         if (Array.isArray(value)) {
//           for (let arr_value of value) {
//             search.append(key, arr_value);
//           }
//         } else {
//
//           if (value && typeof value === 'object') {
//             /// Convert dates to ISO format string
//             if (value instanceof Date) {
//               value = value.toISOString();
//             } else {
//               value = JSON.stringify(value);
//             }
//           }
//           search.append(key, value);
//
//         }
//         break;
//
//       case TGetParamsMappingType.Bracket:
//         /// Convert object and arrays to query params
//         for (let k in value) {
//           if (value.hasOwnProperty(k)) {
//             appendSearchParams(search, key + '[' + k + ']', value[k]);
//           }
//         }
//         break;
//     }
//
//     return;
//   }
//
//
//   search.append(key, value);
//
// }


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_http__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceProviders; });

var ResourceProviders = (function () {
    function ResourceProviders() {
    }
    ResourceProviders.add = function (resource, subSet) {
        if (subSet === void 0) { subSet = null; }
        if (!subSet) {
            subSet = this.mainProvidersName;
        }
        if (!this.providers[subSet]) {
            this.providers[subSet] = [];
        }
        var deps = Reflect.getMetadata('design:paramtypes', resource);
        if (!deps || deps.length === 0) {
            deps = [__WEBPACK_IMPORTED_MODULE_0__angular_http__["Http"]];
        }
        this.providers[subSet].push({
            provide: resource,
            useFactory: function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return new (resource.bind.apply(resource, [void 0].concat(args)))();
            },
            deps: deps
        });
    };
    ResourceProviders.get = function (subSet) {
        if (subSet === void 0) { subSet = null; }
        if (!subSet) {
            subSet = this.mainProvidersName;
        }
        return this.providers[subSet] || [];
    };
    return ResourceProviders;
}());

ResourceProviders.mainProvidersName = '__mainProviders';
ResourceProviders.providers = {
    __mainProviders: []
};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TGetParamsMappingType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ResourceGlobalConfig; });
var TGetParamsMappingType;
(function (TGetParamsMappingType) {
    TGetParamsMappingType[TGetParamsMappingType["Plain"] = 0] = "Plain";
    TGetParamsMappingType[TGetParamsMappingType["Bracket"] = 1] = "Bracket";
})(TGetParamsMappingType || (TGetParamsMappingType = {}));
var ResourceGlobalConfig = (function () {
    function ResourceGlobalConfig() {
    }
    return ResourceGlobalConfig;
}());

ResourceGlobalConfig.url = null;
ResourceGlobalConfig.path = null;
ResourceGlobalConfig.headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};
ResourceGlobalConfig.params = null;
ResourceGlobalConfig.data = null;
ResourceGlobalConfig.getParamsMappingType = TGetParamsMappingType.Plain;


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ResourceProviders__ = __webpack_require__(3);
/* harmony export (immutable) */ __webpack_exports__["a"] = ResourceParams;

function ResourceParams(params) {
    if (params === void 0) { params = {}; }
    return function (target) {
        target.prototype.getResourceOptions = function () {
            return params;
        };
        if (params.add2Provides !== false) {
            __WEBPACK_IMPORTED_MODULE_0__ResourceProviders__["a" /* ResourceProviders */].add(target, params.providersSubSet);
        }
        if (typeof params.removeTrailingSlash !== 'undefined') {
            target.prototype.removeTrailingSlash = function () {
                return !!params.removeTrailingSlash;
            };
        }
        if (params.url) {
            target.prototype._getUrl = function () {
                return params.url;
            };
        }
        if (params.path) {
            target.prototype._getPath = function () {
                return params.path;
            };
        }
        if (params.headers) {
            target.prototype._getHeaders = function () {
                return params.headers;
            };
        }
        if (params.params) {
            target.prototype._getParams = function () {
                return params.params;
            };
        }
        if (params.data) {
            target.prototype._getData = function () {
                return params.data;
            };
        }
    };
}


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_common__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__angular_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_Resource__ = __webpack_require__(1);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_4__src_Resource__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__src_ResourceAction__ = __webpack_require__(2);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_5__src_ResourceAction__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__src_ResourceCRUD__ = __webpack_require__(9);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_6__src_ResourceCRUD__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__src_ResourceCRUDBase__ = __webpack_require__(10);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_7__src_ResourceCRUDBase__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__src_ResourceGlobalConfig__ = __webpack_require__(5);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_8__src_ResourceGlobalConfig__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_8__src_ResourceGlobalConfig__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__src_ResourceModel__ = __webpack_require__(11);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_9__src_ResourceModel__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__src_ResourceODATA__ = __webpack_require__(12);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_10__src_ResourceODATA__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_10__src_ResourceODATA__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__src_ResourceParams__ = __webpack_require__(6);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_11__src_ResourceParams__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__["a"]; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var ResourceModule = ResourceModule_1 = (function () {
    function ResourceModule() {
    }
    ResourceModule.forRoot = function () {
        return {
            ngModule: ResourceModule_1,
            providers: __WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__["a" /* ResourceProviders */].providers[__WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__["a" /* ResourceProviders */].mainProvidersName]
        };
    };
    ResourceModule.forChild = function (subSet) {
        return {
            ngModule: ResourceModule_1,
            providers: __WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__["a" /* ResourceProviders */].providers[subSet] ? __WEBPACK_IMPORTED_MODULE_3__src_ResourceProviders__["a" /* ResourceProviders */].providers[subSet] : []
        };
    };
    return ResourceModule;
}());
ResourceModule = ResourceModule_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_2__angular_http__["HttpModule"]]
    })
], ResourceModule);

var ResourceModule_1;


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index__ = __webpack_require__(7);
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceModule", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["a"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "Resource", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["b"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceAction", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["c"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceCRUD", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["d"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceCRUDBase", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["e"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "TGetParamsMappingType", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["f"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceGlobalConfig", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["g"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceModel", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["h"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceODATA", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["i"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceODATAParams", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["j"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceParams", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["k"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "ResourceProviders", function() { return __WEBPACK_IMPORTED_MODULE_0__index__["l"]; });



/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Resource__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ResourceAction__ = __webpack_require__(2);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceCRUD; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResourceCRUD = (function (_super) {
    __extends(ResourceCRUD, _super);
    function ResourceCRUD() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // Alias to save
    ResourceCRUD.prototype.create = function (data, callback) {
        return this.save(data, callback);
    };
    return ResourceCRUD;
}(__WEBPACK_IMPORTED_MODULE_1__Resource__["a" /* Resource */]));

__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        isArray: true
    }),
    __metadata("design:type", Function)
], ResourceCRUD.prototype, "query", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        path: '/{!id}'
    }),
    __metadata("design:type", Function)
], ResourceCRUD.prototype, "get", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Post
    }),
    __metadata("design:type", Function)
], ResourceCRUD.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Put,
        path: '/{!id}'
    }),
    __metadata("design:type", Function)
], ResourceCRUD.prototype, "update", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Delete,
        path: '/{!id}'
    }),
    __metadata("design:type", Function)
], ResourceCRUD.prototype, "remove", void 0);


/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Resource__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ResourceAction__ = __webpack_require__(2);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceCRUDBase; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResourceCRUDBase = (function (_super) {
    __extends(ResourceCRUDBase, _super);
    function ResourceCRUDBase() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // Alias to save
    ResourceCRUDBase.prototype.create = function (data, callback) {
        return this.save(data, callback);
    };
    return ResourceCRUDBase;
}(__WEBPACK_IMPORTED_MODULE_1__Resource__["a" /* Resource */]));

__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        isArray: true
    }),
    __metadata("design:type", Function)
], ResourceCRUDBase.prototype, "query", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])(),
    __metadata("design:type", Function)
], ResourceCRUDBase.prototype, "get", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Post
    }),
    __metadata("design:type", Function)
], ResourceCRUDBase.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Put
    }),
    __metadata("design:type", Function)
], ResourceCRUDBase.prototype, "update", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_0__angular_http__["RequestMethod"].Delete
    }),
    __metadata("design:type", Function)
], ResourceCRUDBase.prototype, "remove", void 0);


/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Resource__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceModel; });

var ResourceModel = (function () {
    function ResourceModel() {
    }
    ResourceModel.prototype.$setData = function (data) {
        Object.assign(this, data);
        return this;
    };
    ResourceModel.prototype.$save = function () {
        if (this.isNew()) {
            return this.$create();
        }
        else {
            return this.$update();
        }
    };
    ResourceModel.prototype.$create = function () {
        return this.$resource_method('create');
    };
    ResourceModel.prototype.$update = function () {
        return this.$resource_method('update');
    };
    ResourceModel.prototype.$remove = function () {
        return this.$resource_method('remove');
    };
    ResourceModel.prototype.toJSON = function () {
        return __WEBPACK_IMPORTED_MODULE_0__Resource__["a" /* Resource */].$cleanData(this);
    };
    ResourceModel.prototype.isNew = function () {
        return !this['id'];
    };
    ResourceModel.prototype.$resource_method = function (methodName) {
        if (!this.$resource) {
            console.error("Your Resource is not set. Please use resource.createModel() method to create model.");
            return this;
        }
        if (!this.$resource[methodName]) {
            console.error("Your Resource has no implemented " + methodName + " method.");
            return this;
        }
        this.$resource[methodName](this);
        return this;
    };
    return ResourceModel;
}());



/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Resource__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ResourceAction__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ResourceParams__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResourceODATA; });
/* harmony export (immutable) */ __webpack_exports__["b"] = ResourceODATAParams;
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/** A Resource base class for ODATA entities. To create a resource is just
 * enough to extend this class and all the base ODATA functionalities will be present.
 */
var ResourceODATA = (function (_super) {
    __extends(ResourceODATA, _super);
    function ResourceODATA() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ResourceODATA.prototype.$getUrl = function () {
        return _super.prototype.$getUrl.call(this) + "/" + this.getEntitySetName();
    };
    ResourceODATA.prototype.getEntityName = function () {
        return null;
    };
    ResourceODATA.prototype.getEntitySetName = function () {
        return this.getEntityName() + "s";
    };
    return ResourceODATA;
}(__WEBPACK_IMPORTED_MODULE_2__Resource__["a" /* Resource */]));

__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__ResourceAction__["a" /* ResourceAction */])({
        path: '/{!id}'
    }),
    __metadata("design:type", Function)
], ResourceODATA.prototype, "get", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__ResourceAction__["a" /* ResourceAction */])({
        method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestMethod"].Post
    }),
    __metadata("design:type", Function)
], ResourceODATA.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__ResourceAction__["a" /* ResourceAction */])({
        params: {
            "$filter": "@$filter",
            "$search": "@$search",
            "$expand": "@$expand",
            "$limit": "@$limit",
            "query": "@query"
        },
        isArray: true
    }),
    __metadata("design:type", Function)
], ResourceODATA.prototype, "search", void 0);
/**
 * A ODATA annotation for a resource for a ODATA entity resource extending {@link ResourceODATA}.
 */
function ResourceODATAParams(params) {
    var injectable = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])();
    var zuper = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4__ResourceParams__["a" /* ResourceParams */])(params);
    return function (target) {
        injectable(target);
        zuper(target);
        target.prototype.getEntityName = function () {
            if (params.name) {
                return params.name;
            }
            return typeof params.entity === "string" ? params.entity : params.entity.name;
        };
    };
}


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_14__;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_16__;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCBiMzdiMTE5MjAwYjYyOGU2MzVlOSIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAYW5ndWxhci9odHRwXCIiLCJ3ZWJwYWNrOi8vLy4vc3JjL1Jlc291cmNlLnRzIiwid2VicGFjazovLy8uL3NyYy9SZXNvdXJjZUFjdGlvbi50cyIsIndlYnBhY2s6Ly8vLi9zcmMvUmVzb3VyY2VQcm92aWRlcnMudHMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQGFuZ3VsYXIvY29yZVwiIiwid2VicGFjazovLy8uL3NyYy9SZXNvdXJjZUdsb2JhbENvbmZpZy50cyIsIndlYnBhY2s6Ly8vLi9zcmMvUmVzb3VyY2VQYXJhbXMudHMiLCJ3ZWJwYWNrOi8vLy4vaW5kZXgudHMiLCJ3ZWJwYWNrOi8vLy4vbmd4LXJlc291cmNlLnRzIiwid2VicGFjazovLy8uL3NyYy9SZXNvdXJjZUNSVUQudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL1Jlc291cmNlQ1JVREJhc2UudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL1Jlc291cmNlTW9kZWwudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL1Jlc291cmNlT0RBVEEudHMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQGFuZ3VsYXIvY29tbW9uXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicnhqcy9PYnNlcnZhYmxlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyeGpzL2FkZC9vcGVyYXRvci9wdWJsaXNoXCIiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87QUNWQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7O0FDaEVBLCtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBMkM7QUFDc0Q7QUFFcEQ7QUFLVDtBQUNMO0FBQ0k7QUFHa0Q7QUFhckYsSUFBYSxRQUFRO0lBZ0JuQixrQkFBc0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07UUFOeEIsU0FBSSxHQUFXLElBQUksQ0FBQztRQUNwQixVQUFLLEdBQVcsSUFBSSxDQUFDO1FBQ3JCLGFBQVEsR0FBUSxJQUFJLENBQUM7UUFDckIsWUFBTyxHQUFRLElBQUksQ0FBQztRQUNwQixVQUFLLEdBQVEsSUFBSSxDQUFDO0lBRzFCLENBQUM7SUFFTSxtQkFBVSxHQUFqQixVQUFrQixHQUF3QjtRQUV4QyxHQUFHLENBQUMsQ0FBQyxJQUFJLFFBQVEsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBRXpCLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2QixDQUFDO1FBRUgsQ0FBQztRQUVELE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDYixDQUFDO0lBR0Q7OztPQUdHO0lBQ0gsMEJBQU8sR0FBUCxVQUFRLGFBQWtDO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksbUZBQW9CLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQztJQUNyRixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsMEJBQU8sR0FBUCxVQUFRLEdBQVc7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILDJCQUFRLEdBQVIsVUFBUyxhQUFrQztRQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxJQUFJLG1GQUFvQixDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7SUFDeEYsQ0FBQztJQUVEOzs7T0FHRztJQUNILDJCQUFRLEdBQVIsVUFBUyxJQUFZO1FBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4QkFBVyxHQUFYLFVBQVksYUFBa0M7UUFDNUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxtRkFBb0IsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO0lBQ2pHLENBQUM7SUFFRDs7O09BR0c7SUFDSCw4QkFBVyxHQUFYLFVBQVksT0FBWTtRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUMxQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsNkJBQVUsR0FBVixVQUFXLGFBQWtDO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksbUZBQW9CLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQztJQUM5RixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsNkJBQVUsR0FBVixVQUFXLE1BQVc7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7SUFDeEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILDJCQUFRLEdBQVIsVUFBUyxhQUFrQztRQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxJQUFJLG1GQUFvQixDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7SUFDeEYsQ0FBQztJQUVEOzs7T0FHRztJQUNILDJCQUFRLEdBQVIsVUFBUyxJQUFTO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsK0JBQVksR0FBWjtRQUNFLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ25DLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDYixDQUFDO0lBR0Q7OztPQUdHO0lBQ08sc0NBQW1CLEdBQTdCLFVBQThCLEdBQVksRUFBRSxhQUFrQztRQUM1RSxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVEOzs7O09BSUc7SUFDTyx1Q0FBb0IsR0FBOUIsVUFBK0IsVUFBMkIsRUFBRSxHQUFZLEVBQUUsYUFBa0M7UUFDMUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsYUFBRyxJQUFJLFVBQUcsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLElBQUksRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxtQ0FBbUM7SUFDbkMsaUJBQWlCO0lBQ2pCLElBQUk7SUFFTSxvQ0FBaUIsR0FBM0IsVUFBNEIsYUFBd0M7UUFBeEMsb0RBQXdDO1FBQ2xFLE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRVMsdUJBQUksR0FBZCxVQUFlLElBQVM7UUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFUywwQkFBTyxHQUFqQixVQUFrQixJQUFTO1FBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRVMsc0NBQW1CLEdBQTdCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFUywyQkFBUSxHQUFsQixVQUFtQixHQUFZLEVBQUUsYUFBc0M7UUFBdEMsa0RBQXNDO1FBRXJFLElBQUksaUJBQWlCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFL0MsdUNBQXVDO1FBQ3ZDLE1BQU0sQ0FBQyxhQUFhLENBQUMsbUJBQW1CO1lBQ3RDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFFckUsQ0FBQztJQUVTLGtDQUFlLEdBQXpCLFVBQTBCLElBQVMsRUFBRSxNQUFXLEVBQUUsUUFBbUIsRUFBRSxhQUFpQztRQUV0RyxJQUFNLEtBQUssR0FBK0M7WUFDeEQsY0FBYyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDO1lBQzVELElBQUksRUFBRSxJQUFJO1lBQ1YsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsYUFBYTtZQUN0QixRQUFRLEVBQUUsUUFBUTtTQUNuQixDQUFDO1FBRUYsS0FBSyxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztRQUVoSCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUcxQixNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQztJQUU5QixDQUFDO0lBRU8scUNBQWtCLEdBQTFCLFVBQTJCLElBQVMsRUFBRSxhQUFpQztRQUVyRSxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN6QixNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ1osQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxFQUFFLENBQUM7UUFDWixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDM0QsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUVkLENBQUM7SUFFTyxxQ0FBa0IsR0FBMUIsVUFBMkIsYUFBaUM7UUFDMUQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkgsQ0FBQztJQUVPLHdCQUFLLEdBQWIsVUFBYyxhQUFpQztRQUM3QyxNQUFNLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDM0QsQ0FBQztJQUVPLDJCQUFRLEdBQWhCLFVBQWlCLGFBQWlDO1FBQ2hELE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUNwRSxDQUFDO0lBRU8sOEJBQVcsR0FBbkIsVUFBb0IsS0FBMkI7UUFFN0MsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ2xELEtBQUssQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxVQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6RixDQUFDO0lBRUgsQ0FBQztJQUVPLGlDQUFjLEdBQXRCLFVBQXVCLEtBQTJCO1FBRWhELElBQU0sY0FBYyxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7UUFFNUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFFakMsY0FBYyxDQUFDLFdBQVcsR0FBRywyREFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFDLFVBQTJCO1lBQ3pFLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxVQUFVLENBQUM7UUFDNUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQU0sWUFBSyxDQUFDLGNBQWMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1FBRXZDLGNBQWMsQ0FBQyxhQUFhLEdBQUc7WUFDN0IsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDbEMsQ0FBQyxDQUFDO1FBRUYsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFFaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDMUIsY0FBYyxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3JDLGNBQWMsQ0FBQyxXQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDckUsQ0FBQztJQUVILENBQUM7SUFFTyxnQ0FBYSxHQUFyQixVQUFzQixLQUEyQjtRQUFqRCxpQkFpREM7UUEvQ0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQzthQUM3QixJQUFJLENBQUMsVUFBQyxZQUF3QztZQUU3QyxLQUFLLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztZQUVsQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLEtBQUssQ0FBQyxjQUFjLEdBQUcsMkRBQVUsQ0FBQyxNQUFNLENBQUMsVUFBQyxVQUEyQjtvQkFDbkUsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsS0FBSSxDQUFDLCtCQUErQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM1QyxNQUFNLENBQUM7WUFDVCxDQUFDO1lBRUQsS0FBSyxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFFakQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFbkMsSUFBSSxXQUFXLEdBQUcsSUFBSSxzREFBTyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUVwRCxXQUFXLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0I7Z0JBQzVDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUM7Z0JBQzVELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXZELEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDakIsS0FBSyxDQUFDLGNBQWMsR0FBRywyREFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFDLFFBQWE7b0JBQ3JELFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBRWhDLEtBQUksQ0FBQywrQkFBK0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDNUMsTUFBTSxDQUFDO1lBQ1QsQ0FBQztZQUVELG9CQUFvQjtZQUNwQixJQUFNLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVwRSxLQUFLLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztZQUV4SCxLQUFJLENBQUMsK0JBQStCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFOUMsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRU8sdUNBQW9CLEdBQTVCLFVBQTZCLEtBQTJCO1FBQ3RELE1BQU0sQ0FBQyxPQUFPO2FBQ1gsR0FBRyxDQUFDO1lBQ0gsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNqRSxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25FLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RSxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3BFLENBQUM7YUFDRCxJQUFJLENBQUMsVUFBQyxJQUFXO1lBRWhCLE1BQU0sQ0FBOEI7Z0JBQ2xDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNaLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNiLE9BQU8sRUFBRSxJQUFJLHNEQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUNkLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxrREFBK0IsR0FBdkMsVUFBd0MsS0FBMkI7UUFDakUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztZQUNqQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDcEMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hDLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDdEMsQ0FBQztJQUNILENBQUM7SUFFTyxzQ0FBbUIsR0FBM0IsVUFBNEIsS0FBMkI7UUFFckQsSUFBTSxjQUFjLEdBQVEsRUFBRSxDQUFDO1FBQy9CLEtBQUssQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1FBRXRDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFFL0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLEtBQUssQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RFLENBQUM7WUFFRCxJQUFNLFVBQVUsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7b0NBRTlDLENBQUM7Z0JBQ1IsSUFBSSxTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUU5QixJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDO2dCQUNyQyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNoQixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsQ0FBQztnQkFFRCxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDO2dCQUNuQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNkLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixDQUFDO2dCQUVELElBQUksS0FBSyxHQUFHLE9BQUssaUJBQWlCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztnQkFDbkgsRUFBRSxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQy9CLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDN0IsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxPQUFLLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzt3QkFFaEIsSUFBSSxZQUFVLEdBQUcsZUFBYSxTQUFTLCtCQUE0QixDQUFDO3dCQUVwRSxLQUFLLENBQUMsY0FBYyxHQUFHLDJEQUFVLENBQUMsTUFBTSxDQUFDLFVBQUMsUUFBYTs0QkFDckQsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxZQUFVLENBQUMsQ0FBQyxDQUFDO3dCQUN4QyxDQUFDLENBQUMsQ0FBQzt3QkFFSCxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVUsQ0FBQyxDQUFDO3dCQUV6QixPQUFLLCtCQUErQixDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM1QyxNQUFNLElBQUksS0FBSyxDQUFDLFlBQVUsQ0FBQyxDQUFDO29CQUU5QixDQUFDO29CQUNELEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7O2dCQUVoRSxDQUFDO2dCQUVELHVCQUF1QjtnQkFDdkIsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQzs7WUF4Q0QsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtzQ0FBakMsQ0FBQzs7O2FBd0NUO1FBRUgsQ0FBQztRQUVELHlDQUF5QztRQUN6QyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM3QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0MsQ0FBQztRQUVELHdCQUF3QjtRQUN4QixFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztZQUM3RCxLQUFLLENBQUMsT0FBTyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQyxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDdEMsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUMvQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN4RCxDQUFDO1FBQ0gsQ0FBQztRQUVELHVCQUF1QjtRQUN2QixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDMUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDOUMsT0FBTyxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4QyxDQUFDO1FBQ0gsQ0FBQztJQUVILENBQUM7SUFFTyxnQ0FBYSxHQUFyQixVQUFzQixLQUEyQjtRQUUvQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyw0REFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTTtZQUNOLEtBQUssQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hGLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLFVBQVU7WUFDVixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDZixJQUFJLEtBQUssR0FBUSxFQUFFLENBQUM7Z0JBQ3BCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDM0IsS0FBSyxDQUFDLEtBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFVLENBQUMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNsRCxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNOLEtBQUssR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNyQixDQUFDO2dCQUNELEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQyxDQUFDO1lBQ0QsS0FBSyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQ3BDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRCxDQUFDO0lBRUgsQ0FBQztJQUVPLGtDQUFlLEdBQXZCLFVBQXdCLEtBQTJCO1FBQ2pELEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSw4REFBZSxFQUFFLENBQUM7UUFFckMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNsQixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDbkMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDekUsSUFBSSxLQUFLLEdBQVEsS0FBSyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDekMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN0RCxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUM7UUFFRCxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztRQUN4QyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ1gsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDL0MsQ0FBQztJQUVILENBQUM7SUFFTyx5Q0FBc0IsR0FBOUIsVUFBK0IsS0FBMkI7UUFDeEQsS0FBSyxDQUFDLGNBQWMsR0FBZ0I7WUFDbEMsTUFBTSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTTtZQUM1QixPQUFPLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPO1lBQ25DLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSTtZQUNoQixHQUFHLEVBQUUsS0FBSyxDQUFDLEdBQUc7WUFDZCxlQUFlLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxlQUFlO1NBQy9DLENBQUM7UUFFRixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixLQUFLLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzdDLENBQUM7SUFFSCxDQUFDO0lBRU8seUNBQXNCLEdBQTlCLFVBQStCLEtBQTJCLEVBQUUsaUJBQXVDO1FBQW5HLGlCQTRFQztRQTFFQyxNQUFNLENBQUMsMkRBQVUsQ0FBQyxNQUFNLENBQUMsVUFBQyxVQUEyQjtZQUVuRCxJQUFJLFNBQVMsR0FBaUIsaUJBQWlCLENBQUMsU0FBUyxDQUN2RCxVQUFDLElBQVM7Z0JBRVIsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzVDLElBQU0sR0FBRyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUV0QyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFFbEIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUUxQixrQkFBa0I7d0JBRWxCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3pCLE9BQU8sQ0FBQyxLQUFLLENBQUMsNENBQTRDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBQ3BFLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBRU4sV0FBSyxDQUFDLGNBQWMsRUFBQyxJQUFJLFdBQ3BCLElBQUk7aUNBQ0osTUFBTSxDQUFDLE1BQU0sQ0FBQztpQ0FDZCxHQUFHLENBQUMsR0FBRyxDQUFDO2lDQUNSLEdBQUcsQ0FBQyxVQUFDLFFBQWE7Z0NBQ2pCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUN4QixRQUFRLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQztnQ0FDNUIsQ0FBQztnQ0FDRCxNQUFNLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7NEJBQ2xGLENBQUMsQ0FBQyxFQUNKO3dCQUVKLENBQUM7b0JBRUgsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFFTixtQkFBbUI7d0JBRW5CLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN4QixPQUFPLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUNyRSxDQUFDO3dCQUFDLElBQUksQ0FBQyxDQUFDOzRCQUVOLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBRWpCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzRCQUUxRCxDQUFDO3dCQUVILENBQUM7b0JBQ0gsQ0FBQztnQkFDSCxDQUFDO2dCQUVELEtBQUssQ0FBQyxjQUFjLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7O1lBRXhDLENBQUMsRUFDRCxVQUFDLEdBQVEsSUFBSyxpQkFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDbkM7Z0JBQ0UsS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUN0QyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNuQixLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDdkMsQ0FBQztZQUNILENBQUMsQ0FDRixDQUFDO1lBRUYsS0FBSyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEdBQUc7Z0JBQ25DLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDbkMsTUFBTSxDQUFDO2dCQUNULENBQUM7Z0JBQ0QsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN4QixLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDeEMsQ0FBQyxDQUFDO1FBRUosQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDO0lBRU8sb0NBQWlCLEdBQXpCLFVBQTBCLEdBQVEsRUFBRSxJQUFTO1FBRTNDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLEdBQUcsR0FBRyxJQUFJLENBQUM7WUFDYixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDM0IsQ0FBQztRQUNILENBQUM7UUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDO0lBRWIsQ0FBQztJQUVPLG9DQUFpQixHQUF6QixVQUEwQixHQUFXLEVBQUUsTUFBVyxFQUFFLElBQVMsRUFBRSxjQUFtQjtRQUVoRixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzFFLGNBQWMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ3JGLENBQUM7UUFFRCxjQUFjLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFFckIsQ0FBQztJQUVPLHNDQUFtQixHQUEzQixVQUE0QixLQUFVO1FBQ3BDLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssS0FBSyxTQUFTLENBQUM7SUFDL0MsQ0FBQztJQUVPLHVDQUFvQixHQUE1QixVQUE2QixNQUF1QixFQUFFLEdBQVcsRUFBRSxLQUFVO1FBQzNFLHNDQUFzQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztZQUU5QixNQUFNLENBQUMsQ0FBQyxtRkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7Z0JBRWxELEtBQUssb0ZBQXFCLENBQUMsS0FBSztvQkFFOUIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLEdBQUcsQ0FBQyxDQUFrQixVQUFLLEVBQUwsZUFBSyxFQUFMLG1CQUFLLEVBQUwsSUFBSzs0QkFBdEIsSUFBSSxTQUFTOzRCQUNoQixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQzt5QkFDL0I7b0JBQ0gsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFFTixFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDdkMsc0NBQXNDOzRCQUN0QyxFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDMUIsS0FBSyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQzs0QkFDOUIsQ0FBQzs0QkFBQyxJQUFJLENBQUMsQ0FBQztnQ0FDTixLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDaEMsQ0FBQzt3QkFDSCxDQUFDO3dCQUNELE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUU1QixDQUFDO29CQUNELEtBQUssQ0FBQztnQkFFUixLQUFLLG9GQUFxQixDQUFDLE9BQU87b0JBQ2hDLDZDQUE2QztvQkFDN0MsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQzVCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuRSxDQUFDO29CQUNILENBQUM7b0JBQ0QsS0FBSyxDQUFDO1lBQ1YsQ0FBQztZQUVELE1BQU0sQ0FBQztRQUNULENBQUM7UUFHRCxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUU1QixDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0ssMkJBQVEsR0FBaEIsVUFBaUIsYUFBa0M7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSyw0QkFBUyxHQUFqQixVQUFrQixhQUFrQztRQUNsRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNLLCtCQUFZLEdBQXBCLFVBQXFCLGFBQWtDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0ssOEJBQVcsR0FBbkIsVUFBb0IsYUFBa0M7UUFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSyw0QkFBUyxHQUFqQixVQUFrQixhQUFrQztRQUNsRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUdILGVBQUM7QUFBRCxDQUFDO0FBaHNCUSx5QkFBZ0IsR0FBYTtJQUNsQyxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGFBQWE7SUFDYixlQUFlO0lBQ2YsV0FBVztDQUNaLENBQUM7QUFSUyxRQUFRO0lBRHBCLGdGQUFVLEVBQUU7cUNBaUJpQixtREFBSTtHQWhCckIsUUFBUSxDQWtzQnBCO0FBbHNCb0I7Ozs7Ozs7Ozs7OztBQzFCeUI7QUFVeEMsd0JBQXlCLGFBQWtDO0lBRS9ELGFBQWEsR0FBRyxhQUFhLElBQUksRUFBRSxDQUFDO0lBRXBDLEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN2QyxhQUFhLENBQUMsTUFBTSxHQUFHLDREQUFhLENBQUMsR0FBRyxDQUFDO0lBQzNDLENBQUM7SUFFRCxNQUFNLENBQUMsVUFBVSxNQUFnQixFQUFFLFdBQW1CO1FBRTlDLE1BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRztZQUFVLGNBQWM7aUJBQWQsVUFBYyxFQUFkLHFCQUFjLEVBQWQsSUFBYztnQkFBZCx5QkFBYzs7WUFFbkQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ3hDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDOUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztZQUVoRCxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2QsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxRQUFRLEdBQUcsTUFBTSxDQUFDO2dCQUNsQixNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFFRCxJQUFNLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUU1RSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUUvRCxxREFBcUQ7WUFDdkQsRUFBRTtZQUNBLG1FQUFtRTtZQUNyRSxFQUFFO1lBQ0EsbUVBQW1FO1lBQ3JFLEVBQUU7WUFDQSxxRkFBcUY7WUFDckYsb0dBQW9HO1lBQ3BHLGtGQUFrRjtZQUNsRiwrREFBK0Q7WUFDakUsRUFBRTtZQUNBLGdDQUFnQztZQUNoQyxnQkFBZ0I7WUFDaEIsYUFBYTtZQUNmLEVBQUU7WUFDQSxtQ0FBbUM7WUFDbkMsa0JBQWtCO1lBQ2xCLGVBQWU7WUFDakIsRUFBRTtZQUNBLCtDQUErQztZQUMvQyxpQ0FBaUM7WUFDakMsc0JBQXNCO1lBQ3RCLGlCQUFpQjtZQUNqQiw4QkFBOEI7WUFDOUIsVUFBVTtZQUNaLEVBQUU7WUFDQSxRQUFRO1lBQ1IsTUFBTTtZQUNSLEVBQUU7WUFDQSxtREFBbUQ7WUFDbkQsaUVBQWlFO1lBQ2pFLE1BQU07WUFDUixFQUFFO1lBQ0Esd0RBQXdEO1lBQ3hELHFEQUFxRDtZQUN2RCxFQUFFO1lBQ0EsMkJBQTJCO1lBQzNCLDJFQUEyRTtZQUMzRSwyQ0FBMkM7WUFDM0Msc0NBQXNDO1lBQ3RDLGdDQUFnQztZQUNoQyw0QkFBNEI7WUFDNUIsT0FBTztZQUNQLDBCQUEwQjtZQUM1QixFQUFFO1lBQ0EsMEJBQTBCO1lBQzVCLEVBQUU7WUFDQSw4QkFBOEI7WUFDaEMsRUFBRTtZQUNBLE1BQU07WUFDUixFQUFFO1lBQ0YsRUFBRTtZQUNBLCtDQUErQztZQUMvQyxvQ0FBb0M7WUFDcEMsdUNBQXVDO1lBQ3ZDLDJDQUEyQztZQUMzQyx1Q0FBdUM7WUFDdkMsUUFBUTtZQUNSLE1BQU07WUFDUixFQUFFO1lBQ0EsaUNBQWlDO1lBQ2pDLG1EQUFtRDtZQUNuRCwrREFBK0Q7WUFDL0QsTUFBTTtZQUNSLEVBQUU7WUFDQSxrQkFBa0I7WUFDbEIseUVBQXlFO1lBQ3pFLDJFQUEyRTtZQUMzRSxpRkFBaUY7WUFDakYsK0VBQStFO1lBQy9FLHlFQUF5RTtZQUN6RSxPQUFPO1lBQ1Asa0NBQWtDO1lBQ3BDLEVBQUU7WUFDQSw2QkFBNkI7WUFDN0Isa0VBQWtFO1lBQ2xFLGlDQUFpQztZQUNqQyxjQUFjO1lBQ2hCLEVBQUU7WUFDQSwyQ0FBMkM7WUFDM0MsVUFBVTtZQUNaLEVBQUU7WUFDQSxtREFBbUQ7WUFDbkQsK0NBQStDO1lBQy9DLHdDQUF3QztZQUMxQyxFQUFFO1lBQ0Esc0NBQXNDO1lBQ3hDLEVBQUU7WUFDQSw4Q0FBOEM7WUFDaEQsRUFBRTtZQUNBLHNDQUFzQztZQUN0Qyx3REFBd0Q7WUFDeEQsWUFBWTtZQUNkLEVBQUU7WUFDQSwwREFBMEQ7WUFDNUQsRUFBRTtZQUNBLHdEQUF3RDtZQUMxRCxFQUFFO1lBQ0EsMkNBQTJDO1lBQzdDLEVBQUU7WUFDQSxxRUFBcUU7WUFDckUsa0RBQWtEO1lBQ2xELCtCQUErQjtZQUMvQiwyQ0FBMkM7WUFDM0MsY0FBYztZQUNoQixFQUFFO1lBQ0EsZ0RBQWdEO1lBQ2hELDZCQUE2QjtZQUM3QiwyQ0FBMkM7WUFDM0MsY0FBYztZQUNoQixFQUFFO1lBQ0EsaUdBQWlHO1lBQ2pHLHdDQUF3QztZQUN4QyxvQ0FBb0M7WUFDcEMsY0FBYztZQUNoQixFQUFFO1lBQ0EsNENBQTRDO1lBQzVDLGlDQUFpQztZQUNuQyxFQUFFO1lBQ0EscUZBQXFGO1lBQ3ZGLEVBQUU7WUFDQSx3RUFBd0U7WUFDeEUseURBQXlEO1lBQ3pELG9CQUFvQjtZQUN0QixFQUFFO1lBQ0EsMENBQTBDO1lBQzVDLEVBQUU7WUFDQSxpREFBaUQ7WUFDakQsd0JBQXdCO1lBQzFCLEVBQUU7WUFDQSxnQkFBZ0I7WUFDaEIsMkRBQTJEO1lBQzNELHFCQUFxQjtZQUNyQixjQUFjO1lBQ2hCLEVBQUU7WUFDQSxvQ0FBb0M7WUFDcEMsaURBQWlEO1lBQ2pELFlBQVk7WUFDZCxFQUFFO1lBQ0EsVUFBVTtZQUNaLEVBQUU7WUFDQSxrREFBa0Q7WUFDbEQsMENBQTBDO1lBQzFDLHNDQUFzQztZQUN0QywwQ0FBMEM7WUFDMUMsVUFBVTtZQUNaLEVBQUU7WUFDQSxpQ0FBaUM7WUFDakMsd0VBQXdFO1lBQ3hFLDBFQUEwRTtZQUMxRSxVQUFVO1lBQ1YsaURBQWlEO1lBQ2pELGdEQUFnRDtZQUNoRCxpREFBaUQ7WUFDakQsWUFBWTtZQUNaLFVBQVU7WUFDWixFQUFFO1lBQ0YsRUFBRTtZQUNBLGdDQUFnQztZQUNoQyx5Q0FBeUM7WUFDekMsK0NBQStDO1lBQy9DLHVDQUF1QztZQUN2QyxZQUFZO1lBQ1osVUFBVTtZQUNaLEVBQUU7WUFDRixFQUFFO1lBQ0EseUNBQXlDO1lBQ3pDLGlDQUFpQztZQUNuQyxFQUFFO1lBQ0EsK0JBQStCO1lBQy9CLDRCQUE0QjtZQUM1QixpQkFBaUI7WUFDakIsaUVBQWlFO1lBQ2pFLGlCQUFpQjtZQUNqQixxQkFBcUI7WUFDckIsc0JBQXNCO1lBQ3RCLGlDQUFpQztZQUNqQywwQ0FBMEM7WUFDMUMseURBQXlEO1lBQ3pELHFCQUFxQjtZQUNyQiw0QkFBNEI7WUFDNUIsY0FBYztZQUNkLDBDQUEwQztZQUMxQyxZQUFZO1lBQ1osd0NBQXdDO1lBQ3hDLFVBQVU7WUFDWixFQUFFO1lBQ0YsRUFBRTtZQUNBLGlDQUFpQztZQUNqQyw2REFBNkQ7WUFDL0QsRUFBRTtZQUNBLHVCQUF1QjtZQUN2QiwwQ0FBMEM7WUFDMUMsNEVBQTRFO1lBQzVFLGtEQUFrRDtZQUNsRCxzREFBc0Q7WUFDdEQsY0FBYztZQUNkLFlBQVk7WUFDWixVQUFVO1lBQ1osRUFBRTtZQUNBLCtCQUErQjtZQUMvQixpRkFBaUY7WUFDakYsc0JBQXNCO1lBQ3RCLGlDQUFpQztZQUNqQywyQkFBMkI7WUFDM0IsWUFBWTtZQUNaLDREQUE0RDtZQUM1RCxVQUFVO1lBQ1osRUFBRTtZQUNBLG1EQUFtRDtZQUNuRCxxQkFBcUI7WUFDckIsMENBQTBDO1lBQzFDLFVBQVU7WUFDWixFQUFFO1lBQ0YsRUFBRTtZQUNBLG9DQUFvQztZQUNwQyxrREFBa0Q7WUFDbEQsd0NBQXdDO1lBQ3hDLDRCQUE0QjtZQUM1QixzQkFBc0I7WUFDdEIsb0JBQW9CO1lBQ3BCLDBCQUEwQjtZQUMxQiw0RkFBNEY7WUFDNUYsWUFBWTtZQUNkLEVBQUU7WUFDRixFQUFFO1lBQ0EsbUNBQW1DO1lBQ25DLCtDQUErQztZQUNqRCxFQUFFO1lBQ0EsaURBQWlEO1lBQ2pELGlFQUFpRTtZQUNqRSx3REFBd0Q7WUFDMUQsRUFBRTtZQUNBLG9CQUFvQjtZQUNwQixrRUFBa0U7WUFDbEUsMERBQTBEO1lBQzFELGNBQWM7WUFDaEIsRUFBRTtZQUNBLDJDQUEyQztZQUM3QyxFQUFFO1lBQ0EsMkNBQTJDO1lBQzNDLGtCQUFrQjtZQUNsQixVQUFVO1lBQ1osRUFBRTtZQUNBLDZCQUE2QjtZQUM3QixtRUFBbUU7WUFDckUsRUFBRTtZQUNGLEVBQUU7WUFDQSxvQ0FBb0M7WUFDdEMsRUFBRTtZQUNBLDhDQUE4QztZQUNoRCxFQUFFO1lBQ0EsaUJBQWlCO1lBQ25CLEVBQUU7WUFDQSxnRkFBZ0Y7WUFDbEYsRUFBRTtZQUNBLHVFQUF1RTtZQUN2RSwrQkFBK0I7WUFDakMsRUFBRTtZQUNBLHFDQUFxQztZQUN2QyxFQUFFO1lBQ0EsK0NBQStDO1lBQ2pELEVBQUU7WUFDQSx1Q0FBdUM7WUFDekMsRUFBRTtZQUNBLGdEQUFnRDtZQUNoRCx5RkFBeUY7WUFDekYsNkJBQTZCO1lBQy9CLEVBQUU7WUFDQSx1Q0FBdUM7WUFDdkMsZ0NBQWdDO1lBQ2hDLDBDQUEwQztZQUMxQyxvQ0FBb0M7WUFDcEMsb0RBQW9EO1lBQ3BELHVEQUF1RDtZQUN2RCx5REFBeUQ7WUFDekQsOEJBQThCO1lBQzlCLDRFQUE0RTtZQUM1RSw2QkFBNkI7WUFDN0IseUJBQXlCO1lBQzNCLEVBQUU7WUFDQSxzQkFBc0I7WUFDeEIsRUFBRTtZQUNBLDJCQUEyQjtZQUM3QixFQUFFO1lBQ0Esd0NBQXdDO1lBQzFDLEVBQUU7WUFDQSwrQ0FBK0M7WUFDL0MsMEZBQTBGO1lBQzFGLDZCQUE2QjtZQUMvQixFQUFFO1lBQ0EsMENBQTBDO1lBQzVDLEVBQUU7WUFDQSxnRUFBZ0U7WUFDbEUsRUFBRTtZQUNBLHdCQUF3QjtZQUMxQixFQUFFO1lBQ0Esc0JBQXNCO1lBQ3RCLG9CQUFvQjtZQUNwQixrQkFBa0I7WUFDcEIsRUFBRTtZQUNBLHNDQUFzQztZQUN0Qyw2Q0FBNkM7WUFDL0MsRUFBRTtZQUNBLGlCQUFpQjtZQUNqQixtREFBbUQ7WUFDbkQsc0JBQXNCO1lBQ3RCLHNDQUFzQztZQUN0Qyx1Q0FBdUM7WUFDdkMsZ0NBQWdDO1lBQ2hDLHdDQUF3QztZQUN4QyxrQkFBa0I7WUFDbEIsZ0JBQWdCO1lBQ2hCLGVBQWU7WUFDakIsRUFBRTtZQUNBLHdDQUF3QztZQUN4QyxtQ0FBbUM7WUFDbkMsd0JBQXdCO1lBQ3hCLGdCQUFnQjtZQUNoQix1Q0FBdUM7WUFDdkMsb0NBQW9DO1lBQ3BDLGVBQWU7WUFDakIsRUFBRTtZQUNBLGNBQWM7WUFDaEIsRUFBRTtZQUNBLFVBQVU7WUFDWixFQUFFO1lBQ0EseUNBQXlDO1lBQzNDLEVBQUU7WUFDQSxVQUFVO1lBQ1osRUFBRTtZQUNBLHVCQUF1QjtRQUV2QixDQUFDLENBQUM7SUFFSixDQUFDLENBQUM7QUFFSixDQUFDO0FBR0QsOERBQThEO0FBQzlELEVBQUU7QUFDRix3QkFBd0I7QUFDeEIsMEJBQTBCO0FBQzFCLGFBQWE7QUFDYixnQ0FBZ0M7QUFDaEMsTUFBTTtBQUNOLEVBQUU7QUFDRixnQkFBZ0I7QUFDaEIsRUFBRTtBQUNGLElBQUk7QUFFSiwrRkFBK0Y7QUFDL0YsMkNBQTJDO0FBQzNDLGlDQUFpQztBQUNqQywrQ0FBK0M7QUFDL0MsY0FBYztBQUNkLE1BQU07QUFDTixFQUFFO0FBQ0YscUNBQXFDO0FBQ3JDLEVBQUU7QUFDRiwyREFBMkQ7QUFDM0QsRUFBRTtBQUNGLDBDQUEwQztBQUMxQyxFQUFFO0FBQ0Ysc0NBQXNDO0FBQ3RDLDJDQUEyQztBQUMzQyw2Q0FBNkM7QUFDN0MsY0FBYztBQUNkLG1CQUFtQjtBQUNuQixFQUFFO0FBQ0Ysc0RBQXNEO0FBQ3RELHFEQUFxRDtBQUNyRCwyQ0FBMkM7QUFDM0MsNkNBQTZDO0FBQzdDLHVCQUF1QjtBQUN2QiwrQ0FBK0M7QUFDL0MsZ0JBQWdCO0FBQ2hCLGNBQWM7QUFDZCx1Q0FBdUM7QUFDdkMsRUFBRTtBQUNGLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsRUFBRTtBQUNGLDRDQUE0QztBQUM1Qyx3REFBd0Q7QUFDeEQsaUNBQWlDO0FBQ2pDLDJDQUEyQztBQUMzQyx5RUFBeUU7QUFDekUsY0FBYztBQUNkLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsUUFBUTtBQUNSLEVBQUU7QUFDRixjQUFjO0FBQ2QsTUFBTTtBQUNOLEVBQUU7QUFDRixFQUFFO0FBQ0YsK0JBQStCO0FBQy9CLEVBQUU7QUFDRixJQUFJOzs7Ozs7Ozs7OztBQ3JiaUM7QUFJckM7SUFBQTtJQTJDQSxDQUFDO0lBcENRLHFCQUFHLEdBQVYsVUFBVyxRQUF3QixFQUFFLE1BQXFCO1FBQXJCLHNDQUFxQjtRQUV4RCxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDWixNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzlCLENBQUM7UUFFRCxJQUFJLElBQUksR0FBZ0IsT0FBUSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUU1RSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxHQUFHLENBQUMsbURBQUksQ0FBQyxDQUFDO1FBQ2hCLENBQUM7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDekI7WUFDRSxPQUFPLEVBQUUsUUFBUTtZQUNqQixVQUFVLEVBQUU7Z0JBQUMsY0FBYztxQkFBZCxVQUFjLEVBQWQscUJBQWMsRUFBZCxJQUFjO29CQUFkLHlCQUFjOztnQkFBSyxZQUFJLFFBQVEsWUFBUixRQUFRLGtCQUFJLElBQUk7WUFBcEIsQ0FBcUI7WUFDckQsSUFBSSxFQUFFLElBQUk7U0FDWCxDQUNGLENBQUM7SUFFSixDQUFDO0lBRU0scUJBQUcsR0FBVixVQUFXLE1BQXFCO1FBQXJCLHNDQUFxQjtRQUU5QixFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDWixNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xDLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFdEMsQ0FBQztJQUVILHdCQUFDO0FBQUQsQ0FBQzs7QUF6Q1EsbUNBQWlCLEdBQVcsaUJBQWlCLENBQUM7QUFDOUMsMkJBQVMsR0FBaUM7SUFDL0MsZUFBZSxFQUFFLEVBQUU7Q0FDcEIsQ0FBQzs7Ozs7OztBQ1ZKLCtDOzs7Ozs7Ozs7QUNBQSxJQUFZLHFCQUdYO0FBSEQsV0FBWSxxQkFBcUI7SUFDL0IsbUVBQUs7SUFDTCx1RUFBTztBQUNULENBQUMsRUFIVyxxQkFBcUIsS0FBckIscUJBQXFCLFFBR2hDO0FBRUQ7SUFBQTtJQVdBLENBQUM7SUFBRCwyQkFBQztBQUFELENBQUM7O0FBVlEsd0JBQUcsR0FBNkIsSUFBSSxDQUFDO0FBQ3JDLHlCQUFJLEdBQTZCLElBQUksQ0FBQztBQUN0Qyw0QkFBTyxHQUF1QjtJQUNuQyxRQUFRLEVBQUUsa0JBQWtCO0lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7Q0FDbkMsQ0FBQztBQUNLLDJCQUFNLEdBQXVCLElBQUksQ0FBQztBQUNsQyx5QkFBSSxHQUF1QixJQUFJLENBQUM7QUFFaEMseUNBQW9CLEdBQVEscUJBQXFCLENBQUMsS0FBSyxDQUFDOzs7Ozs7Ozs7O0FDWlQ7QUFJbEQsd0JBQXlCLE1BQStCO0lBQS9CLG9DQUErQjtJQUU1RCxNQUFNLENBQUMsVUFBVSxNQUFzQjtRQUdyQyxNQUFNLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHO1lBQ3BDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDaEIsQ0FBQyxDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLDZFQUFpQixDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hELENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLE1BQU0sQ0FBQyxtQkFBbUIsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3RELE1BQU0sQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEdBQUc7Z0JBQ3JDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO1lBQ3RDLENBQUMsQ0FBQztRQUNKLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNmLE1BQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHO2dCQUN6QixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNwQixDQUFDLENBQUM7UUFDSixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUc7Z0JBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ3JCLENBQUMsQ0FBQztRQUNKLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNuQixNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRztnQkFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFDeEIsQ0FBQyxDQUFDO1FBQ0osQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHO2dCQUM1QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUN2QixDQUFDLENBQUM7UUFDSixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUc7Z0JBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ3JCLENBQUMsQ0FBQztRQUNKLENBQUM7SUFFSCxDQUFDLENBQUM7QUFDSixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pENkQ7QUFDZjtBQUNKO0FBRWlCO0FBRTdCO0FBQ007QUFDRjtBQUNJO0FBQ0k7QUFDUDtBQUNBO0FBQ0M7QUFDRztBQU14QyxJQUFhLGNBQWM7SUFBM0I7SUFlQSxDQUFDO0lBZFEsc0JBQU8sR0FBZDtRQUNFLE1BQU0sQ0FBQztZQUNMLFFBQVEsRUFBRSxnQkFBYztZQUN4QixTQUFTLEVBQUUsaUZBQWlCLENBQUMsU0FBUyxDQUFDLGlGQUFpQixDQUFDLGlCQUFpQixDQUFDO1NBQzVFLENBQUM7SUFDSixDQUFDO0lBRU0sdUJBQVEsR0FBZixVQUFnQixNQUFjO1FBQzVCLE1BQU0sQ0FBQztZQUNMLFFBQVEsRUFBRSxnQkFBYztZQUN4QixTQUFTLEVBQUUsaUZBQWlCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLGlGQUFpQixDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFO1NBQzFGLENBQUM7SUFDSixDQUFDO0lBRUgscUJBQUM7QUFBRCxDQUFDO0FBZlksY0FBYztJQUgxQiw4RUFBUSxDQUFDO1FBQ1IsT0FBTyxFQUFFLENBQUMsNkRBQVksRUFBRSx5REFBVSxDQUFDO0tBQ3BDLENBQUM7R0FDVyxjQUFjLENBZTFCO0FBZjBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBc0I7QUFFUjtBQUVZO0FBRWxEO0lBQXlELGdDQUFRO0lBQWpFOztJQWtDQSxDQUFDO0lBTEMsZ0JBQWdCO0lBQ2hCLDZCQUFNLEdBQU4sVUFBTyxJQUFXLEVBQUUsUUFBOEI7UUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFSCxtQkFBQztBQUFELENBQUMsQ0FsQ3dELDJEQUFRLEdBa0NoRTs7QUE3QkM7SUFIQyw4RkFBYyxDQUFDO1FBQ2QsT0FBTyxFQUFFLElBQUk7S0FDZCxDQUFDOzsyQ0FDc0M7QUFLeEM7SUFIQyw4RkFBYyxDQUFDO1FBQ2QsSUFBSSxFQUFFLFFBQVE7S0FDZixDQUFDOzt5Q0FDc0M7QUFLeEM7SUFIQyw4RkFBYyxDQUFDO1FBQ2QsTUFBTSxFQUFFLDREQUFhLENBQUMsSUFBSTtLQUMzQixDQUFDOzswQ0FDaUM7QUFNbkM7SUFKQyw4RkFBYyxDQUFDO1FBQ2QsTUFBTSxFQUFFLDREQUFhLENBQUMsR0FBRztRQUN6QixJQUFJLEVBQUUsUUFBUTtLQUNmLENBQUM7OzRDQUNtQztBQU1yQztJQUpDLDhGQUFjLENBQUM7UUFDZCxNQUFNLEVBQUUsNERBQWEsQ0FBQyxNQUFNO1FBQzVCLElBQUksRUFBRSxRQUFRO0tBQ2YsQ0FBQzs7NENBQ3VDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pDRztBQUVSO0FBRVk7QUFFbEQ7SUFBb0Usb0NBQVE7SUFBNUU7O0lBOEJBLENBQUM7SUFMQyxnQkFBZ0I7SUFDaEIsaUNBQU0sR0FBTixVQUFPLElBQVcsRUFBRSxRQUE4QjtRQUNoRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVILHVCQUFDO0FBQUQsQ0FBQyxDQTlCbUUsMkRBQVEsR0E4QjNFOztBQXpCQztJQUhDLDhGQUFjLENBQUM7UUFDZCxPQUFPLEVBQUUsSUFBSTtLQUNkLENBQUM7OytDQUNzQztBQUd4QztJQURDLDhGQUFjLEVBQUU7OzZDQUNpQjtBQUtsQztJQUhDLDhGQUFjLENBQUM7UUFDZCxNQUFNLEVBQUUsNERBQWEsQ0FBQyxJQUFJO0tBQzNCLENBQUM7OzhDQUNpQztBQUtuQztJQUhDLDhGQUFjLENBQUM7UUFDZCxNQUFNLEVBQUUsNERBQWEsQ0FBQyxHQUFHO0tBQzFCLENBQUM7O2dEQUNtQztBQUtyQztJQUhDLDhGQUFjLENBQUM7UUFDZCxNQUFNLEVBQUUsNERBQWEsQ0FBQyxNQUFNO0tBQzdCLENBQUM7O2dEQUNpQzs7Ozs7Ozs7OztBQzVCQztBQUd0QztJQUFBO0lBNkRBLENBQUM7SUF0RFEsZ0NBQVEsR0FBZixVQUFnQixJQUFTO1FBQ3ZCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRU0sNkJBQUssR0FBWjtRQUVFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDakIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3hCLENBQUM7SUFFSCxDQUFDO0lBRU0sK0JBQU8sR0FBZDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVNLCtCQUFPLEdBQWQ7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTSwrQkFBTyxHQUFkO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU0sOEJBQU0sR0FBYjtRQUNFLE1BQU0sQ0FBQywyREFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMsNkJBQUssR0FBZjtRQUNFLE1BQU0sQ0FBQyxDQUFPLElBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU8sd0NBQWdCLEdBQXhCLFVBQXlCLFVBQWtCO1FBRXpDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDcEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxxRkFBcUYsQ0FBQyxDQUFDO1lBQ3JHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsQ0FBTyxJQUFJLENBQUMsU0FBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QyxPQUFPLENBQUMsS0FBSyxDQUFDLHNDQUFvQyxVQUFVLGFBQVUsQ0FBQyxDQUFDO1lBQ3hFLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBRUssSUFBSSxDQUFDLFNBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV4QyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUlILG9CQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pFMEM7QUFDRztBQUdSO0FBRVk7QUFDQTtBQWlCbEQ7O0dBRUc7QUFDSDtJQUErQyxpQ0FBUTtJQUF2RDs7SUFrQ0EsQ0FBQztJQVhDLCtCQUFPLEdBQVA7UUFDRSxNQUFNLENBQUMsaUJBQU0sT0FBTyxXQUFFLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3pELENBQUM7SUFFRCxxQ0FBYSxHQUFiO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyx3Q0FBZ0IsR0FBeEI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLEdBQUcsQ0FBQztJQUNwQyxDQUFDO0lBQ0gsb0JBQUM7QUFBRCxDQUFDLENBbEM4QywyREFBUSxHQWtDdEQ7O0FBOUJDO0lBSEMsOEZBQWMsQ0FBQztRQUNkLElBQUksRUFBRSxRQUFRO0tBQ2YsQ0FBQzs7MENBQ2dEO0FBS2xEO0lBSEMsOEZBQWMsQ0FBQztRQUNkLE1BQU0sRUFBRSw0REFBYSxDQUFDLElBQUk7S0FDM0IsQ0FBQzs7MkNBQ3lCO0FBWTNCO0lBVkMsOEZBQWMsQ0FBQztRQUNkLE1BQU0sRUFBRTtZQUNOLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLFFBQVEsRUFBRSxTQUFTO1lBQ25CLE9BQU8sRUFBRSxRQUFRO1NBQ2xCO1FBQ0QsT0FBTyxFQUFFLElBQUk7S0FDZCxDQUFDOzs2Q0FDK0M7QUF3Qm5EOztHQUVHO0FBQ0csNkJBQThCLE1BQStCO0lBQ2pFLElBQU0sVUFBVSxHQUFHLGdGQUFVLEVBQUUsQ0FBQztJQUNoQyxJQUFNLEtBQUssR0FBRyw4RkFBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXJDLE1BQU0sQ0FBQyxVQUFVLE1BQXNCO1FBQ3JDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQixLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDZCxNQUFNLENBQUMsU0FBUyxDQUFDLGFBQWEsR0FBRztZQUMvQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDaEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDckIsQ0FBQztZQUNELE1BQU0sQ0FBQyxPQUFPLE1BQU0sQ0FBQyxNQUFNLEtBQUssUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEYsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDO0FBQ0osQ0FBQzs7Ozs7OztBQ3pGRCxnRDs7Ozs7O0FDQUEsZ0Q7Ozs7OztBQ0FBLGdEOzs7Ozs7QUNBQSxnRDs7Ozs7O0FDQUEsZ0QiLCJmaWxlIjoibmd4LXJlc291cmNlLnVtZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZShcIkBhbmd1bGFyL2h0dHBcIiksIHJlcXVpcmUoXCJAYW5ndWxhci9jb3JlXCIpLCByZXF1aXJlKFwiQGFuZ3VsYXIvY29tbW9uXCIpLCByZXF1aXJlKFwicnhqcy9PYnNlcnZhYmxlXCIpLCByZXF1aXJlKFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCIpLCByZXF1aXJlKFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIiksIHJlcXVpcmUoXCJyeGpzL2FkZC9vcGVyYXRvci9wdWJsaXNoXCIpKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtcIkBhbmd1bGFyL2h0dHBcIiwgXCJAYW5ndWxhci9jb3JlXCIsIFwiQGFuZ3VsYXIvY29tbW9uXCIsIFwicnhqcy9PYnNlcnZhYmxlXCIsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCIsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIiwgXCJyeGpzL2FkZC9vcGVyYXRvci9wdWJsaXNoXCJdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcIm5neC1yZXNvdXJjZVwiXSA9IGZhY3RvcnkocmVxdWlyZShcIkBhbmd1bGFyL2h0dHBcIiksIHJlcXVpcmUoXCJAYW5ndWxhci9jb3JlXCIpLCByZXF1aXJlKFwiQGFuZ3VsYXIvY29tbW9uXCIpLCByZXF1aXJlKFwicnhqcy9PYnNlcnZhYmxlXCIpLCByZXF1aXJlKFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCIpLCByZXF1aXJlKFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIiksIHJlcXVpcmUoXCJyeGpzL2FkZC9vcGVyYXRvci9wdWJsaXNoXCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJuZ3gtcmVzb3VyY2VcIl0gPSBmYWN0b3J5KHJvb3RbXCJAYW5ndWxhci9odHRwXCJdLCByb290W1wiQGFuZ3VsYXIvY29yZVwiXSwgcm9vdFtcIkBhbmd1bGFyL2NvbW1vblwiXSwgcm9vdFtcInJ4anMvT2JzZXJ2YWJsZVwiXSwgcm9vdFtcInJ4anMvYWRkL29wZXJhdG9yL21hcFwiXSwgcm9vdFtcInJ4anMvYWRkL29wZXJhdG9yL21lcmdlTWFwXCJdLCByb290W1wicnhqcy9hZGQvb3BlcmF0b3IvcHVibGlzaFwiXSk7XG59KSh0aGlzLCBmdW5jdGlvbihfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFXzBfXywgX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV80X18sIF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfMTNfXywgX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xNF9fLCBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFXzE1X18sIF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfMTZfXywgX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xN19fKSB7XG5yZXR1cm4gXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9cIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA4KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBiMzdiMTE5MjAwYjYyOGU2MzVlOSIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8wX187XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZXh0ZXJuYWwgXCJAYW5ndWxhci9odHRwXCJcbi8vIG1vZHVsZSBpZCA9IDBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSGVhZGVycywgSHR0cCwgUmVxdWVzdCwgUmVxdWVzdE1ldGhvZCwgUmVzcG9uc2UsIFVSTFNlYXJjaFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgUmVxdWVzdEFyZ3MgfSBmcm9tICdAYW5ndWxhci9odHRwL3NyYy9pbnRlcmZhY2VzJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0IHsgQ29ubmVjdGFibGVPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9vYnNlcnZhYmxlL0Nvbm5lY3RhYmxlT2JzZXJ2YWJsZSc7XG5pbXBvcnQgeyBTdWJzY3JpYmVyIH0gZnJvbSAncnhqcy9TdWJzY3JpYmVyJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvU3Vic2NyaXB0aW9uJztcblxuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9tZXJnZU1hcCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL3B1Ymxpc2gnO1xuXG5cbmltcG9ydCB7IFJlc291cmNlR2xvYmFsQ29uZmlnLCBUR2V0UGFyYW1zTWFwcGluZ1R5cGUgfSBmcm9tICcuL1Jlc291cmNlR2xvYmFsQ29uZmlnJztcbmltcG9ydCB7IFJlc291cmNlTW9kZWwgfSBmcm9tICcuL1Jlc291cmNlTW9kZWwnO1xuaW1wb3J0IHtcbiAgUmVzb3VyY2VBY3Rpb25CYXNlLFxuICBSZXNvdXJjZVBhcmFtc0Jhc2UsXG4gIFJlc291cmNlUmVzcG9uc2VGaWx0ZXIsXG4gIFJlc291cmNlUmVzcG9uc2VJbml0UmVzdWx0LFxuICBSZXNvdXJjZVJlc3BvbnNlTWFwLFxuICBSZXNvdXJjZVJlc3VsdFxufSBmcm9tICcuL0ludGVyZmFjZXMnO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZXNvdXJjZSB7XG5cbiAgc3RhdGljICRjbGVhbkRhdGFGaWVsZHM6IHN0cmluZ1tdID0gW1xuICAgICckY2xlYW5EYXRhRmllbGRzJyxcbiAgICAnJHJlc29sdmVkJyxcbiAgICAnJG9ic2VydmFibGUnLFxuICAgICckYWJvcnRSZXF1ZXN0JyxcbiAgICAnJHJlc291cmNlJ1xuICBdO1xuXG4gIHByaXZhdGUgJHVybDogc3RyaW5nID0gbnVsbDtcbiAgcHJpdmF0ZSAkcGF0aDogc3RyaW5nID0gbnVsbDtcbiAgcHJpdmF0ZSAkaGVhZGVyczogYW55ID0gbnVsbDtcbiAgcHJpdmF0ZSAkcGFyYW1zOiBhbnkgPSBudWxsO1xuICBwcml2YXRlICRkYXRhOiBhbnkgPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBodHRwOiBIdHRwKSB7XG4gIH1cblxuICBzdGF0aWMgJGNsZWFuRGF0YShvYmo6IFJlc291cmNlUmVzdWx0PGFueT4pOiBhbnkge1xuXG4gICAgZm9yIChsZXQgcHJvcE5hbWUgaW4gb2JqKSB7XG5cbiAgICAgIGlmICgob2JqW3Byb3BOYW1lXSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB8fCB0aGlzLiRjbGVhbkRhdGFGaWVsZHMuaW5kZXhPZihwcm9wTmFtZSkgPiAtMSkge1xuICAgICAgICBkZWxldGUgb2JqW3Byb3BOYW1lXTtcbiAgICAgIH1cblxuICAgIH1cblxuICAgIHJldHVybiBvYmo7XG4gIH1cblxuXG4gIC8qKlxuICAgKiBHZXQgbWFpbiB1cmwgb2YgdGhlIHJlc291cmNlXG4gICAqIEByZXR1cm5zIHtzdHJpbmd8UHJvbWlzZTxzdHJpbmc+fVxuICAgKi9cbiAgJGdldFVybChtZXRob2RPcHRpb25zPzogUmVzb3VyY2VBY3Rpb25CYXNlKTogc3RyaW5nIHwgUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy4kdXJsIHx8IHRoaXMuJF9nZXRVcmwobWV0aG9kT3B0aW9ucykgfHwgUmVzb3VyY2VHbG9iYWxDb25maWcudXJsIHx8ICcnO1xuICB9XG5cbiAgLyoqXG4gICAqIFNldCByZXNvdXJjZSB1cmxcbiAgICogQHBhcmFtIHVybFxuICAgKi9cbiAgJHNldFVybCh1cmw6IHN0cmluZykge1xuICAgIHRoaXMuJHVybCA9IHVybDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgcGF0aCBvZiB0aGUgcmVzb3VyY2VcbiAgICogQHJldHVybnMge3N0cmluZ3xQcm9taXNlPHN0cmluZz59XG4gICAqL1xuICAkZ2V0UGF0aChtZXRob2RPcHRpb25zPzogUmVzb3VyY2VBY3Rpb25CYXNlKTogc3RyaW5nIHwgUHJvbWlzZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpcy4kcGF0aCB8fCB0aGlzLiRfZ2V0UGF0aChtZXRob2RPcHRpb25zKSB8fCBSZXNvdXJjZUdsb2JhbENvbmZpZy5wYXRoIHx8ICcnO1xuICB9XG5cbiAgLyoqXG4gICAqIFNldCByZXNvdXJjZSBwYXRoXG4gICAqIEBwYXJhbSBwYXRoXG4gICAqL1xuICAkc2V0UGF0aChwYXRoOiBzdHJpbmcpIHtcbiAgICB0aGlzLiRwYXRoID0gcGF0aDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgaGVhZGVyc1xuICAgKiBAcmV0dXJucyB7YW55fFByb21pc2U8YW55Pn1cbiAgICovXG4gICRnZXRIZWFkZXJzKG1ldGhvZE9wdGlvbnM/OiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBhbnkgfCBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLiRoZWFkZXJzIHx8IHRoaXMuJF9nZXRIZWFkZXJzKG1ldGhvZE9wdGlvbnMpIHx8IFJlc291cmNlR2xvYmFsQ29uZmlnLmhlYWRlcnMgfHwge307XG4gIH1cblxuICAvKipcbiAgICogU2V0IHJlc291cmNlIGhlYWRlcnNcbiAgICogQHBhcmFtIGhlYWRlcnNcbiAgICovXG4gICRzZXRIZWFkZXJzKGhlYWRlcnM6IGFueSkge1xuICAgIHRoaXMuJGhlYWRlcnMgPSBoZWFkZXJzO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBkZWZhdWx0IHBhcmFtc1xuICAgKiBAcmV0dXJucyB7YW55fFByb21pc2U8YW55Pnx7fX1cbiAgICovXG4gICRnZXRQYXJhbXMobWV0aG9kT3B0aW9ucz86IFJlc291cmNlQWN0aW9uQmFzZSk6IGFueSB8IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuJHBhcmFtcyB8fCB0aGlzLiRfZ2V0UGFyYW1zKG1ldGhvZE9wdGlvbnMpIHx8IFJlc291cmNlR2xvYmFsQ29uZmlnLnBhcmFtcyB8fCB7fTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgZGVmYXVsdCByZXNvdXJjZSBwYXJhbXNcbiAgICogQHBhcmFtIHBhcmFtc1xuICAgKi9cbiAgJHNldFBhcmFtcyhwYXJhbXM6IGFueSkge1xuICAgIHRoaXMuJHBhcmFtcyA9IHBhcmFtcztcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgZGVmYXVsdCBkYXRhXG4gICAqIEByZXR1cm5zIHthbnl8UHJvbWlzZTxhbnk+fHt9fVxuICAgKi9cbiAgJGdldERhdGEobWV0aG9kT3B0aW9ucz86IFJlc291cmNlQWN0aW9uQmFzZSk6IGFueSB8IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuJGRhdGEgfHwgdGhpcy4kX2dldERhdGEobWV0aG9kT3B0aW9ucykgfHwgUmVzb3VyY2VHbG9iYWxDb25maWcuZGF0YSB8fCB7fTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgZGVmYXVsdCByZXNvdXJjZSBwYXJhbXNcbiAgICogQHBhcmFtIGRhdGFcbiAgICovXG4gICRzZXREYXRhKGRhdGE6IGFueSkge1xuICAgIHRoaXMuJGRhdGEgPSBkYXRhO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSB0aGUgbW9kZWxcbiAgICpcbiAgICogQHJldHVybnMge2FueX1cbiAgICovXG4gICRjcmVhdGVNb2RlbCgpOiBSZXNvdXJjZU1vZGVsPGFueT4ge1xuICAgIGxldCByZXQgPSB0aGlzLiRpbml0UmVzdWx0T2JqZWN0KCk7XG4gICAgcmV0LiRyZXNvdXJjZSA9IHRoaXM7XG4gICAgcmV0dXJuIHJldDtcbiAgfVxuXG5cbiAgLyoqXG4gICAqIFRoYXQgaXMgY2FsbGVkIGJlZm9yZSBleGVjdXRpbmcgcmVxdWVzdFxuICAgKiBAcGFyYW0gcmVxXG4gICAqL1xuICBwcm90ZWN0ZWQgJHJlcXVlc3RJbnRlcmNlcHRvcihyZXE6IFJlcXVlc3QsIG1ldGhvZE9wdGlvbnM/OiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBSZXF1ZXN0IHtcbiAgICByZXR1cm4gcmVxO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlcXVlc3Qgb2JzZXJ2YWJsZSBpbnRlcmNlcHRvclxuICAgKiBAcGFyYW0gb2JzZXJ2YWJsZVxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxuICAgKi9cbiAgcHJvdGVjdGVkICRyZXNwb25zZUludGVyY2VwdG9yKG9ic2VydmFibGU6IE9ic2VydmFibGU8YW55PiwgcmVxOiBSZXF1ZXN0LCBtZXRob2RPcHRpb25zPzogUmVzb3VyY2VBY3Rpb25CYXNlKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gb2JzZXJ2YWJsZS5tYXAocmVzID0+IHJlcy5fYm9keSA/IHJlcy5qc29uKCkgOiBudWxsKTtcbiAgfVxuXG4gIC8vIHJlbW92ZVRyYWlsaW5nU2xhc2goKTogYm9vbGVhbiB7XG4gIC8vICAgcmV0dXJuIHRydWU7XG4gIC8vIH1cblxuICBwcm90ZWN0ZWQgJGluaXRSZXN1bHRPYmplY3QobWV0aG9kT3B0aW9uczogUmVzb3VyY2VBY3Rpb25CYXNlID0gbnVsbCk6IGFueSB7XG4gICAgcmV0dXJuIHt9O1xuICB9XG5cbiAgcHJvdGVjdGVkICRtYXAoaXRlbTogYW55KTogYW55IHtcbiAgICByZXR1cm4gaXRlbTtcbiAgfVxuXG4gIHByb3RlY3RlZCAkZmlsdGVyKGl0ZW06IGFueSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcHJvdGVjdGVkICRnZXRSZXNvdXJjZU9wdGlvbnMoKTogUmVzb3VyY2VQYXJhbXNCYXNlIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByb3RlY3RlZCAkcmVxdWVzdChyZXE6IFJlcXVlc3QsIG1ldGhvZE9wdGlvbnM6IFJlc291cmNlQWN0aW9uQmFzZSA9IHt9KTogT2JzZXJ2YWJsZTxhbnk+IHtcblxuICAgIGxldCByZXF1ZXN0T2JzZXJ2YWJsZSA9IHRoaXMuaHR0cC5yZXF1ZXN0KHJlcSk7XG5cbiAgICAvLyBub2luc3BlY3Rpb24gVHlwZVNjcmlwdFZhbGlkYXRlVHlwZXNcbiAgICByZXR1cm4gbWV0aG9kT3B0aW9ucy5yZXNwb25zZUludGVyY2VwdG9yID9cbiAgICAgIG1ldGhvZE9wdGlvbnMucmVzcG9uc2VJbnRlcmNlcHRvcihyZXF1ZXN0T2JzZXJ2YWJsZSwgcmVxLCBtZXRob2RPcHRpb25zKSA6XG4gICAgICB0aGlzLiRyZXNwb25zZUludGVyY2VwdG9yKHJlcXVlc3RPYnNlcnZhYmxlLCByZXEsIG1ldGhvZE9wdGlvbnMpO1xuXG4gIH1cblxuICBwcm90ZWN0ZWQgJHJlc291cmNlQWN0aW9uKGRhdGE6IGFueSwgcGFyYW1zOiBhbnksIGNhbGxiYWNrOiAoKSA9PiBhbnksIG1ldGhvZE9wdGlvbnM6IFJlc291cmNlQWN0aW9uQmFzZSk6IFJlc291cmNlUmVzdWx0PGFueT4gfCBSZXNvdXJjZU1vZGVsPFJlc291cmNlPiB7XG5cbiAgICBjb25zdCBzaGVsbDogSVJlc291cmNlQWN0aW9uU2hlbGwgPSA8SVJlc291cmNlQWN0aW9uU2hlbGw+e1xuICAgICAgcmV0dXJuSW50ZXJuYWw6IHRoaXMuJF9jcmVhdGVSZXR1cm5EYXRhKGRhdGEsIG1ldGhvZE9wdGlvbnMpLFxuICAgICAgZGF0YTogZGF0YSxcbiAgICAgIHBhcmFtczogcGFyYW1zLFxuICAgICAgb3B0aW9uczogbWV0aG9kT3B0aW9ucyxcbiAgICAgIGNhbGxiYWNrOiBjYWxsYmFja1xuICAgIH07XG5cbiAgICBzaGVsbC5yZXR1cm5FeHRlcm5hbCA9IG1ldGhvZE9wdGlvbnMubGVhbiA/IHRoaXMuJF9jcmVhdGVSZXR1cm5EYXRhKGRhdGEsIG1ldGhvZE9wdGlvbnMpIDogc2hlbGwucmV0dXJuSW50ZXJuYWw7XG5cbiAgICB0aGlzLiRfY2xlYW5EYXRhKHNoZWxsKTtcbiAgICB0aGlzLiRfZmlsbEludGVybmFsKHNoZWxsKTtcblxuICAgIHRoaXMuJF9tYWluUmVxdWVzdChzaGVsbCk7XG5cblxuICAgIHJldHVybiBzaGVsbC5yZXR1cm5FeHRlcm5hbDtcblxuICB9XG5cbiAgcHJpdmF0ZSAkX2NyZWF0ZVJldHVybkRhdGEoZGF0YTogYW55LCBtZXRob2RPcHRpb25zOiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBSZXNvdXJjZVJlc3VsdDxhbnk+IHwgUmVzb3VyY2VNb2RlbDxSZXNvdXJjZT4ge1xuXG4gICAgaWYgKG1ldGhvZE9wdGlvbnMuaXNMYXp5KSB7XG4gICAgICByZXR1cm4ge307XG4gICAgfVxuXG4gICAgaWYgKG1ldGhvZE9wdGlvbnMuaXNBcnJheSkge1xuICAgICAgcmV0dXJuIFtdO1xuICAgIH1cblxuICAgIGlmIChtZXRob2RPcHRpb25zLmxlYW4gfHwgIWRhdGEgfHwgZGF0YS4kcmVzb3VyY2UgIT09IHRoaXMpIHtcbiAgICAgIHJldHVybiB0aGlzLiRfaW5pdFJlc3VsdE9iamVjdChtZXRob2RPcHRpb25zKTtcbiAgICB9XG5cbiAgICByZXR1cm4gZGF0YTtcblxuICB9XG5cbiAgcHJpdmF0ZSAkX2luaXRSZXN1bHRPYmplY3QobWV0aG9kT3B0aW9uczogUmVzb3VyY2VBY3Rpb25CYXNlKTogUmVzb3VyY2VSZXNwb25zZUluaXRSZXN1bHQge1xuICAgIHJldHVybiBtZXRob2RPcHRpb25zLmluaXRSZXN1bHRPYmplY3QgPyBtZXRob2RPcHRpb25zLmluaXRSZXN1bHRPYmplY3QoKSA6IHRoaXMuJGluaXRSZXN1bHRPYmplY3QobWV0aG9kT3B0aW9ucyk7XG4gIH1cblxuICBwcml2YXRlICRfbWFwKG1ldGhvZE9wdGlvbnM6IFJlc291cmNlQWN0aW9uQmFzZSk6IFJlc291cmNlUmVzcG9uc2VNYXAge1xuICAgIHJldHVybiBtZXRob2RPcHRpb25zLm1hcCA/IG1ldGhvZE9wdGlvbnMubWFwIDogdGhpcy4kbWFwO1xuICB9XG5cbiAgcHJpdmF0ZSAkX2ZpbHRlcihtZXRob2RPcHRpb25zOiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBSZXNvdXJjZVJlc3BvbnNlRmlsdGVyIHtcbiAgICByZXR1cm4gbWV0aG9kT3B0aW9ucy5maWx0ZXIgPyBtZXRob2RPcHRpb25zLmZpbHRlciA6IHRoaXMuJGZpbHRlcjtcbiAgfVxuXG4gIHByaXZhdGUgJF9jbGVhbkRhdGEoc2hlbGw6IElSZXNvdXJjZUFjdGlvblNoZWxsKSB7XG5cbiAgICBpZiAoc2hlbGwuZGF0YSAmJiAhc2hlbGwub3B0aW9ucy5za2lwRGF0YUNsZWFuaW5nKSB7XG4gICAgICBzaGVsbC5kYXRhID0gc2hlbGwuZGF0YS50b0pTT04gPyBzaGVsbC5kYXRhLnRvSlNPTigpIDogUmVzb3VyY2UuJGNsZWFuRGF0YShzaGVsbC5kYXRhKTtcbiAgICB9XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9maWxsSW50ZXJuYWwoc2hlbGw6IElSZXNvdXJjZUFjdGlvblNoZWxsKSB7XG5cbiAgICBjb25zdCByZXR1cm5JbnRlcm5hbCA9IHNoZWxsLnJldHVybkludGVybmFsO1xuXG4gICAgcmV0dXJuSW50ZXJuYWwuJHJlc29sdmVkID0gZmFsc2U7XG5cbiAgICByZXR1cm5JbnRlcm5hbC4kb2JzZXJ2YWJsZSA9IE9ic2VydmFibGUuY3JlYXRlKChzdWJzY3JpYmVyOiBTdWJzY3JpYmVyPGFueT4pID0+IHtcbiAgICAgIHNoZWxsLm1haW5EZWZlcnJlZFN1YnNjcmliZXIgPSBzdWJzY3JpYmVyO1xuICAgIH0pLmZsYXRNYXAoKCkgPT4gc2hlbGwubWFpbk9ic2VydmFibGUpO1xuXG4gICAgcmV0dXJuSW50ZXJuYWwuJGFib3J0UmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIHJldHVybkludGVybmFsLiRyZXNvbHZlZCA9IHRydWU7XG4gICAgfTtcblxuICAgIHJldHVybkludGVybmFsLiRyZXNvdXJjZSA9IHRoaXM7XG5cbiAgICBpZiAoIXNoZWxsLm9wdGlvbnMuaXNMYXp5KSB7XG4gICAgICByZXR1cm5JbnRlcm5hbC4kb2JzZXJ2YWJsZSA9IHJldHVybkludGVybmFsLiRvYnNlcnZhYmxlLnB1Ymxpc2goKTtcbiAgICAgICg8Q29ubmVjdGFibGVPYnNlcnZhYmxlPGFueT4+cmV0dXJuSW50ZXJuYWwuJG9ic2VydmFibGUpLmNvbm5lY3QoKTtcbiAgICB9XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9tYWluUmVxdWVzdChzaGVsbDogSVJlc291cmNlQWN0aW9uU2hlbGwpIHtcblxuICAgIHRoaXMuJF9yZXNvbHZlTWFpbk9wdGlvbnMoc2hlbGwpXG4gICAgICAudGhlbigoZXh0cmFPcHRpb25zOiBJUmVzb3VyY2VBY3Rpb25NYWluT3B0aW9ucykgPT4ge1xuXG4gICAgICAgIHNoZWxsLmV4dHJhT3B0aW9ucyA9IGV4dHJhT3B0aW9ucztcblxuICAgICAgICBpZiAoc2hlbGwucmV0dXJuSW50ZXJuYWwuJHJlc29sdmVkKSB7XG4gICAgICAgICAgc2hlbGwubWFpbk9ic2VydmFibGUgPSBPYnNlcnZhYmxlLmNyZWF0ZSgoc3Vic2NyaWJlcjogU3Vic2NyaWJlcjxhbnk+KSA9PiB7XG4gICAgICAgICAgICBzdWJzY3JpYmVyLm5leHQobnVsbCk7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICB0aGlzLiRfcmVsZWFzZU1haW5EZWZlcnJlZFN1YnNjcmliZXIoc2hlbGwpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHNoZWxsLnVybCA9IGV4dHJhT3B0aW9ucy51cmwgKyBleHRyYU9wdGlvbnMucGF0aDtcblxuICAgICAgICB0aGlzLiRfcHJlcGFyZURhdGFQYXJhbXMoc2hlbGwpO1xuICAgICAgICB0aGlzLiRfcHJlcGFyZUJvZHkoc2hlbGwpO1xuICAgICAgICB0aGlzLiRfcHJlcGFyZVNlYXJjaChzaGVsbCk7XG4gICAgICAgIHRoaXMuJF9jcmVhdGVSZXF1ZXN0T3B0aW9ucyhzaGVsbCk7XG5cbiAgICAgICAgbGV0IG1haW5SZXF1ZXN0ID0gbmV3IFJlcXVlc3Qoc2hlbGwucmVxdWVzdE9wdGlvbnMpO1xuXG4gICAgICAgIG1haW5SZXF1ZXN0ID0gc2hlbGwub3B0aW9ucy5yZXF1ZXN0SW50ZXJjZXB0b3IgP1xuICAgICAgICAgIHNoZWxsLm9wdGlvbnMucmVxdWVzdEludGVyY2VwdG9yKG1haW5SZXF1ZXN0LCBzaGVsbC5vcHRpb25zKSA6XG4gICAgICAgICAgdGhpcy4kcmVxdWVzdEludGVyY2VwdG9yKG1haW5SZXF1ZXN0LCBzaGVsbC5vcHRpb25zKTtcblxuICAgICAgICBpZiAoIW1haW5SZXF1ZXN0KSB7XG4gICAgICAgICAgc2hlbGwubWFpbk9ic2VydmFibGUgPSBPYnNlcnZhYmxlLmNyZWF0ZSgob2JzZXJ2ZXI6IGFueSkgPT4ge1xuICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKCdSZXF1ZXN0IGlzIG51bGwnKSk7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICBjb25zb2xlLndhcm4oJ1JlcXVlc3QgaXMgbnVsbCcpO1xuXG4gICAgICAgICAgdGhpcy4kX3JlbGVhc2VNYWluRGVmZXJyZWRTdWJzY3JpYmVyKHNoZWxsKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAvLyBEb2luZyB0aGUgcmVxdWVzdFxuICAgICAgICBjb25zdCByZXF1ZXN0T2JzZXJ2YWJsZSA9IHRoaXMuJHJlcXVlc3QobWFpblJlcXVlc3QsIHNoZWxsLm9wdGlvbnMpO1xuXG4gICAgICAgIHNoZWxsLm1haW5PYnNlcnZhYmxlID0gc2hlbGwub3B0aW9ucy5pc0xhenkgPyByZXF1ZXN0T2JzZXJ2YWJsZSA6IHRoaXMuJF9jcmVhdGVNYWluT2JzZXJ2YWJsZShzaGVsbCwgcmVxdWVzdE9ic2VydmFibGUpO1xuXG4gICAgICAgIHRoaXMuJF9yZWxlYXNlTWFpbkRlZmVycmVkU3Vic2NyaWJlcihzaGVsbCk7XG5cbiAgICAgIH0pO1xuXG4gIH1cblxuICBwcml2YXRlICRfcmVzb2x2ZU1haW5PcHRpb25zKHNoZWxsOiBJUmVzb3VyY2VBY3Rpb25TaGVsbCk6IFByb21pc2U8SVJlc291cmNlQWN0aW9uTWFpbk9wdGlvbnM+IHtcbiAgICByZXR1cm4gUHJvbWlzZVxuICAgICAgLmFsbChbXG4gICAgICAgIFByb21pc2UucmVzb2x2ZShzaGVsbC5vcHRpb25zLnVybCB8fCB0aGlzLiRnZXRVcmwoc2hlbGwub3B0aW9ucykpLFxuICAgICAgICBQcm9taXNlLnJlc29sdmUoc2hlbGwub3B0aW9ucy5wYXRoIHx8IHRoaXMuJGdldFBhdGgoc2hlbGwub3B0aW9ucykpLFxuICAgICAgICBQcm9taXNlLnJlc29sdmUoc2hlbGwub3B0aW9ucy5oZWFkZXJzIHx8IHRoaXMuJGdldEhlYWRlcnMoc2hlbGwub3B0aW9ucykpLFxuICAgICAgICBQcm9taXNlLnJlc29sdmUoc2hlbGwub3B0aW9ucy5wYXJhbXMgfHwgdGhpcy4kZ2V0UGFyYW1zKHNoZWxsLm9wdGlvbnMpKSxcbiAgICAgICAgUHJvbWlzZS5yZXNvbHZlKHNoZWxsLm9wdGlvbnMuZGF0YSB8fCB0aGlzLiRnZXREYXRhKHNoZWxsLm9wdGlvbnMpKVxuICAgICAgXSlcbiAgICAgIC50aGVuKChkYXRhOiBhbnlbXSkgPT4ge1xuXG4gICAgICAgIHJldHVybiA8SVJlc291cmNlQWN0aW9uTWFpbk9wdGlvbnM+IHtcbiAgICAgICAgICB1cmw6IGRhdGFbMF0sXG4gICAgICAgICAgcGF0aDogZGF0YVsxXSxcbiAgICAgICAgICBoZWFkZXJzOiBuZXcgSGVhZGVycyhkYXRhWzJdID8gT2JqZWN0LmFzc2lnbih7fSwgZGF0YVsyXSkgOiBkYXRhWzJdKSxcbiAgICAgICAgICBwYXJhbXM6IGRhdGFbM10gPyBPYmplY3QuYXNzaWduKHt9LCBkYXRhWzNdKSA6IGRhdGFbM10sXG4gICAgICAgICAgZGF0YTogZGF0YVs0XVxuICAgICAgICB9O1xuICAgICAgfSk7XG4gIH1cblxuICBwcml2YXRlICRfcmVsZWFzZU1haW5EZWZlcnJlZFN1YnNjcmliZXIoc2hlbGw6IElSZXNvdXJjZUFjdGlvblNoZWxsKSB7XG4gICAgaWYgKHNoZWxsLm1haW5EZWZlcnJlZFN1YnNjcmliZXIpIHtcbiAgICAgIHNoZWxsLm1haW5EZWZlcnJlZFN1YnNjcmliZXIubmV4dCgpO1xuICAgICAgc2hlbGwubWFpbkRlZmVycmVkU3Vic2NyaWJlci5jb21wbGV0ZSgpO1xuICAgICAgc2hlbGwubWFpbkRlZmVycmVkU3Vic2NyaWJlciA9IG51bGw7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSAkX3ByZXBhcmVEYXRhUGFyYW1zKHNoZWxsOiBJUmVzb3VyY2VBY3Rpb25TaGVsbCkge1xuXG4gICAgY29uc3QgdXNlZFBhdGhQYXJhbXM6IGFueSA9IHt9O1xuICAgIHNoZWxsLnVzZWRQYXRoUGFyYW1zID0gdXNlZFBhdGhQYXJhbXM7XG5cbiAgICBpZiAoIUFycmF5LmlzQXJyYXkoc2hlbGwuZGF0YSkgfHwgc2hlbGwucGFyYW1zKSB7XG5cbiAgICAgIGlmICghQXJyYXkuaXNBcnJheShzaGVsbC5kYXRhKSkge1xuICAgICAgICBzaGVsbC5kYXRhID0gT2JqZWN0LmFzc2lnbih7fSwgc2hlbGwuZXh0cmFPcHRpb25zLmRhdGEsIHNoZWxsLmRhdGEpO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBwYXRoUGFyYW1zID0gc2hlbGwudXJsLm1hdGNoKC97KFtefV0qKX0vZykgfHwgW107XG5cbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGF0aFBhcmFtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBsZXQgcGF0aFBhcmFtID0gcGF0aFBhcmFtc1tpXTtcblxuICAgICAgICBsZXQgcGF0aEtleSA9IHBhdGhQYXJhbS5zdWJzdHIoMSwgcGF0aFBhcmFtLmxlbmd0aCAtIDIpO1xuICAgICAgICBsZXQgaXNNYW5kYXRvcnkgPSBwYXRoS2V5WzBdID09PSAnISc7XG4gICAgICAgIGlmIChpc01hbmRhdG9yeSkge1xuICAgICAgICAgIHBhdGhLZXkgPSBwYXRoS2V5LnN1YnN0cigxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBpc0dldE9ubHkgPSBwYXRoS2V5WzBdID09PSAnOic7XG4gICAgICAgIGlmIChpc0dldE9ubHkpIHtcbiAgICAgICAgICBwYXRoS2V5ID0gcGF0aEtleS5zdWJzdHIoMSk7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgdmFsdWUgPSB0aGlzLiRfZ2V0VmFsdWVGb3JQYXRoKHBhdGhLZXksIHNoZWxsLmV4dHJhT3B0aW9ucy5wYXJhbXMsIHNoZWxsLnBhcmFtcyB8fCBzaGVsbC5kYXRhLCB1c2VkUGF0aFBhcmFtcyk7XG4gICAgICAgIGlmIChpc0dldE9ubHkgJiYgIXNoZWxsLnBhcmFtcykge1xuICAgICAgICAgIGRlbGV0ZSBzaGVsbC5kYXRhW3BhdGhLZXldO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuJF9pc051bGxPclVuZGVmaW5lZCh2YWx1ZSkpIHtcbiAgICAgICAgICBpZiAoaXNNYW5kYXRvcnkpIHtcblxuICAgICAgICAgICAgbGV0IGNvbnNvbGVNc2cgPSBgTWFuZGF0b3J5ICR7cGF0aFBhcmFtfSBwYXRoIHBhcmFtZXRlciBpcyBtaXNzaW5nYDtcblxuICAgICAgICAgICAgc2hlbGwubWFpbk9ic2VydmFibGUgPSBPYnNlcnZhYmxlLmNyZWF0ZSgob2JzZXJ2ZXI6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoY29uc29sZU1zZykpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGNvbnNvbGUud2Fybihjb25zb2xlTXNnKTtcblxuICAgICAgICAgICAgdGhpcy4kX3JlbGVhc2VNYWluRGVmZXJyZWRTdWJzY3JpYmVyKHNoZWxsKTtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihjb25zb2xlTXNnKTtcblxuICAgICAgICAgIH1cbiAgICAgICAgICBzaGVsbC51cmwgPSBzaGVsbC51cmwuc3Vic3RyKDAsIHNoZWxsLnVybC5pbmRleE9mKHBhdGhQYXJhbSkpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gUmVwbGFjaW5nIGluIHRoZSB1cmxcbiAgICAgICAgc2hlbGwudXJsID0gc2hlbGwudXJsLnJlcGxhY2UocGF0aFBhcmFtLCB2YWx1ZSk7XG4gICAgICB9XG5cbiAgICB9XG5cbiAgICAvLyBSZW1vdmluZyBkb3VibGUgc2xhc2hlZCBmcm9tIGZpbmFsIHVybFxuICAgIHNoZWxsLnVybCA9IHNoZWxsLnVybC5yZXBsYWNlKC9cXC9cXC8rL2csICcvJyk7XG4gICAgaWYgKHNoZWxsLnVybC5zdGFydHNXaXRoKCdodHRwJykpIHtcbiAgICAgIHNoZWxsLnVybCA9IHNoZWxsLnVybC5yZXBsYWNlKCc6LycsICc6Ly8nKTtcbiAgICB9XG5cbiAgICAvLyBSZW1vdmUgdHJhaWxpbmcgc2xhc2hcbiAgICBpZiAodHlwZW9mIHNoZWxsLm9wdGlvbnMucmVtb3ZlVHJhaWxpbmdTbGFzaCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHNoZWxsLm9wdGlvbnMucmVtb3ZlVHJhaWxpbmdTbGFzaCA9IHRydWU7XG4gICAgfVxuICAgIGlmIChzaGVsbC5vcHRpb25zLnJlbW92ZVRyYWlsaW5nU2xhc2gpIHtcbiAgICAgIHdoaWxlIChzaGVsbC51cmxbc2hlbGwudXJsLmxlbmd0aCAtIDFdID09PSAnLycpIHtcbiAgICAgICAgc2hlbGwudXJsID0gc2hlbGwudXJsLnN1YnN0cigwLCBzaGVsbC51cmwubGVuZ3RoIC0gMSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gUmVtb3ZlIG1hcHBlZCBwYXJhbXNcbiAgICBmb3IgKGxldCBrZXkgaW4gc2hlbGwuZXh0cmFPcHRpb25zLnBhcmFtcykge1xuICAgICAgaWYgKHNoZWxsLmV4dHJhT3B0aW9ucy5wYXJhbXNba2V5XVswXSA9PT0gJ0AnKSB7XG4gICAgICAgIGRlbGV0ZSBzaGVsbC5leHRyYU9wdGlvbnMucGFyYW1zW2tleV07XG4gICAgICB9XG4gICAgfVxuXG4gIH1cblxuICBwcml2YXRlICRfcHJlcGFyZUJvZHkoc2hlbGw6IElSZXNvdXJjZUFjdGlvblNoZWxsKSB7XG5cbiAgICBpZiAoc2hlbGwub3B0aW9ucy5tZXRob2QgPT09IFJlcXVlc3RNZXRob2QuR2V0KSB7XG4gICAgICAvLyBHRVRcbiAgICAgIHNoZWxsLnNlYXJjaFBhcmFtcyA9IE9iamVjdC5hc3NpZ24oe30sIHNoZWxsLmV4dHJhT3B0aW9ucy5wYXJhbXMsIHNoZWxsLmRhdGEpO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBOT04gR0VUXG4gICAgICBpZiAoc2hlbGwuZGF0YSkge1xuICAgICAgICBsZXQgX2JvZHk6IGFueSA9IHt9O1xuICAgICAgICBpZiAoc2hlbGwub3B0aW9ucy5yb290Tm9kZSkge1xuICAgICAgICAgIF9ib2R5W2Ake3NoZWxsLm9wdGlvbnMucm9vdE5vZGV9YF0gPSBzaGVsbC5kYXRhO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF9ib2R5ID0gc2hlbGwuZGF0YTtcbiAgICAgICAgfVxuICAgICAgICBzaGVsbC5ib2R5ID0gSlNPTi5zdHJpbmdpZnkoX2JvZHkpO1xuICAgICAgfVxuICAgICAgc2hlbGwuc2VhcmNoUGFyYW1zID0gc2hlbGwucGFyYW1zO1xuICAgIH1cblxuICAgIGlmICghc2hlbGwuYm9keSkge1xuICAgICAgc2hlbGwuZXh0cmFPcHRpb25zLmhlYWRlcnMuZGVsZXRlKCdjb250ZW50LXR5cGUnKTtcbiAgICB9XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9wcmVwYXJlU2VhcmNoKHNoZWxsOiBJUmVzb3VyY2VBY3Rpb25TaGVsbCkge1xuICAgIHNoZWxsLnNlYXJjaCA9IG5ldyBVUkxTZWFyY2hQYXJhbXMoKTtcblxuICAgIGlmICghc2hlbGwucGFyYW1zKSB7XG4gICAgICBmb3IgKGxldCBrZXkgaW4gc2hlbGwuc2VhcmNoUGFyYW1zKSB7XG4gICAgICAgIGlmIChzaGVsbC5zZWFyY2hQYXJhbXMuaGFzT3duUHJvcGVydHkoa2V5KSAmJiAhc2hlbGwudXNlZFBhdGhQYXJhbXNba2V5XSkge1xuICAgICAgICAgIGxldCB2YWx1ZTogYW55ID0gc2hlbGwuc2VhcmNoUGFyYW1zW2tleV07XG4gICAgICAgICAgdGhpcy4kX2FwcGVuZFNlYXJjaFBhcmFtcyhzaGVsbC5zZWFyY2gsIGtleSwgdmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IHRzTmFtZSA9IHNoZWxsLm9wdGlvbnMuYWRkVGltZXN0YW1wO1xuICAgIGlmICh0c05hbWUpIHtcbiAgICAgIGlmICh0c05hbWUgPT09IHRydWUpIHtcbiAgICAgICAgdHNOYW1lID0gJ3RzJztcbiAgICAgIH1cbiAgICAgIHNoZWxsLnNlYXJjaC5hcHBlbmQodHNOYW1lLCAnJyArIERhdGUubm93KCkpO1xuICAgIH1cblxuICB9XG5cbiAgcHJpdmF0ZSAkX2NyZWF0ZVJlcXVlc3RPcHRpb25zKHNoZWxsOiBJUmVzb3VyY2VBY3Rpb25TaGVsbCkge1xuICAgIHNoZWxsLnJlcXVlc3RPcHRpb25zID0gPFJlcXVlc3RBcmdzPntcbiAgICAgIG1ldGhvZDogc2hlbGwub3B0aW9ucy5tZXRob2QsXG4gICAgICBoZWFkZXJzOiBzaGVsbC5leHRyYU9wdGlvbnMuaGVhZGVycyxcbiAgICAgIGJvZHk6IHNoZWxsLmJvZHksXG4gICAgICB1cmw6IHNoZWxsLnVybCxcbiAgICAgIHdpdGhDcmVkZW50aWFsczogc2hlbGwub3B0aW9ucy53aXRoQ3JlZGVudGlhbHNcbiAgICB9O1xuXG4gICAgaWYgKHNoZWxsLm9wdGlvbnMuYW5ndWxhclYyKSB7XG4gICAgICBzaGVsbC5yZXF1ZXN0T3B0aW9ucy5zZWFyY2ggPSBzaGVsbC5zZWFyY2g7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNoZWxsLnJlcXVlc3RPcHRpb25zLnBhcmFtcyA9IHNoZWxsLnNlYXJjaDtcbiAgICB9XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9jcmVhdGVNYWluT2JzZXJ2YWJsZShzaGVsbDogSVJlc291cmNlQWN0aW9uU2hlbGwsIHJlcXVlc3RPYnNlcnZhYmxlOiBPYnNlcnZhYmxlPFJlc3BvbnNlPik6IE9ic2VydmFibGU8YW55PiB7XG5cbiAgICByZXR1cm4gT2JzZXJ2YWJsZS5jcmVhdGUoKHN1YnNjcmliZXI6IFN1YnNjcmliZXI8YW55PikgPT4ge1xuXG4gICAgICBsZXQgcmVxU3Vic2NyOiBTdWJzY3JpcHRpb24gPSByZXF1ZXN0T2JzZXJ2YWJsZS5zdWJzY3JpYmUoXG4gICAgICAgIChyZXNwOiBhbnkpID0+IHtcblxuICAgICAgICAgIGNvbnN0IGZpbHRlciA9IHRoaXMuJF9maWx0ZXIoc2hlbGwub3B0aW9ucyk7XG4gICAgICAgICAgY29uc3QgbWFwID0gdGhpcy4kX21hcChzaGVsbC5vcHRpb25zKTtcblxuICAgICAgICAgIGlmIChyZXNwICE9PSBudWxsKSB7XG5cbiAgICAgICAgICAgIGlmIChzaGVsbC5vcHRpb25zLmlzQXJyYXkpIHtcblxuICAgICAgICAgICAgICAvLyBFeHBlY3RpbmcgYXJyYXlcblxuICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcCkpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdSZXR1cm5lZCBkYXRhIHNob3VsZCBiZSBhbiBhcnJheS4gUmVjZWl2ZWQnLCByZXNwKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIHNoZWxsLnJldHVybkV4dGVybmFsLnB1c2goXG4gICAgICAgICAgICAgICAgICAuLi5yZXNwXG4gICAgICAgICAgICAgICAgICAgIC5maWx0ZXIoZmlsdGVyKVxuICAgICAgICAgICAgICAgICAgICAubWFwKG1hcClcbiAgICAgICAgICAgICAgICAgICAgLm1hcCgocmVzcEl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIGlmICghc2hlbGwub3B0aW9ucy5sZWFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwSXRlbS4kcmVzb3VyY2UgPSB0aGlzO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy4kX3NldERhdGFUb09iamVjdCh0aGlzLiRfaW5pdFJlc3VsdE9iamVjdChzaGVsbC5vcHRpb25zKSwgcmVzcEl0ZW0pO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgIC8vIEV4cGVjdGluZyBvYmplY3RcblxuICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShyZXNwKSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1JldHVybmVkIGRhdGEgc2hvdWxkIGJlIGFuIG9iamVjdC4gUmVjZWl2ZWQnLCByZXNwKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGlmIChmaWx0ZXIocmVzcCkpIHtcblxuICAgICAgICAgICAgICAgICAgdGhpcy4kX3NldERhdGFUb09iamVjdChzaGVsbC5yZXR1cm5FeHRlcm5hbCwgbWFwKHJlc3ApKTtcblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgc2hlbGwucmV0dXJuSW50ZXJuYWwuJHJlc29sdmVkID0gdHJ1ZTtcbiAgICAgICAgICBzdWJzY3JpYmVyLm5leHQoc2hlbGwucmV0dXJuRXh0ZXJuYWwpO1xuXG4gICAgICAgIH0sXG4gICAgICAgIChlcnI6IGFueSkgPT4gc3Vic2NyaWJlci5lcnJvcihlcnIpLFxuICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgc2hlbGwucmV0dXJuSW50ZXJuYWwuJHJlc29sdmVkID0gdHJ1ZTtcbiAgICAgICAgICBzdWJzY3JpYmVyLmNvbXBsZXRlKCk7XG4gICAgICAgICAgaWYgKHNoZWxsLmNhbGxiYWNrKSB7XG4gICAgICAgICAgICBzaGVsbC5jYWxsYmFjayhzaGVsbC5yZXR1cm5FeHRlcm5hbCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICApO1xuXG4gICAgICBzaGVsbC5yZXR1cm5JbnRlcm5hbC4kYWJvcnRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgICAgICBpZiAoc2hlbGwucmV0dXJuSW50ZXJuYWwuJHJlc29sdmVkKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHJlcVN1YnNjci51bnN1YnNjcmliZSgpO1xuICAgICAgICBzaGVsbC5yZXR1cm5JbnRlcm5hbC4kcmVzb2x2ZWQgPSB0cnVlO1xuICAgICAgfTtcblxuICAgIH0pO1xuXG4gIH1cblxuICBwcml2YXRlICRfc2V0RGF0YVRvT2JqZWN0KHJldDogYW55LCByZXNwOiBhbnkpOiBhbnkge1xuXG4gICAgaWYgKHJldC4kc2V0RGF0YSkge1xuICAgICAgcmV0LiRzZXREYXRhKHJlc3ApO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShyZXNwKSkge1xuICAgICAgICByZXQgPSByZXNwO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgT2JqZWN0LmFzc2lnbihyZXQsIHJlc3ApO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZXQ7XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9nZXRWYWx1ZUZvclBhdGgoa2V5OiBzdHJpbmcsIHBhcmFtczogYW55LCBkYXRhOiBhbnksIHVzZWRQYXRoUGFyYW1zOiBhbnkpOiBzdHJpbmcge1xuXG4gICAgaWYgKCF0aGlzLiRfaXNOdWxsT3JVbmRlZmluZWQoZGF0YVtrZXldKSAmJiB0eXBlb2YgZGF0YVtrZXldICE9PSAnb2JqZWN0Jykge1xuICAgICAgdXNlZFBhdGhQYXJhbXNba2V5XSA9IHRydWU7XG4gICAgICByZXR1cm4gZGF0YVtrZXldO1xuICAgIH1cblxuICAgIGlmICh0aGlzLiRfaXNOdWxsT3JVbmRlZmluZWQocGFyYW1zW2tleV0pKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBpZiAocGFyYW1zW2tleV1bMF0gPT09ICdAJykge1xuICAgICAgcmV0dXJuIHRoaXMuJF9nZXRWYWx1ZUZvclBhdGgocGFyYW1zW2tleV0uc3Vic3RyKDEpLCBwYXJhbXMsIGRhdGEsIHVzZWRQYXRoUGFyYW1zKTtcbiAgICB9XG5cbiAgICB1c2VkUGF0aFBhcmFtc1trZXldID0gdHJ1ZTtcbiAgICByZXR1cm4gcGFyYW1zW2tleV07XG5cbiAgfVxuXG4gIHByaXZhdGUgJF9pc051bGxPclVuZGVmaW5lZCh2YWx1ZTogYW55KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHZhbHVlID09PSBudWxsIHx8IHZhbHVlID09PSB1bmRlZmluZWQ7XG4gIH1cblxuICBwcml2YXRlICRfYXBwZW5kU2VhcmNoUGFyYW1zKHNlYXJjaDogVVJMU2VhcmNoUGFyYW1zLCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuICAgIC8vLyBDb252ZXJ0IGRhdGVzIHRvIElTTyBmb3JtYXQgc3RyaW5nXG4gICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgRGF0ZSkge1xuICAgICAgc2VhcmNoLmFwcGVuZChrZXksIHZhbHVlLnRvSVNPU3RyaW5nKCkpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG5cbiAgICAgIHN3aXRjaCAoUmVzb3VyY2VHbG9iYWxDb25maWcuZ2V0UGFyYW1zTWFwcGluZ1R5cGUpIHtcblxuICAgICAgICBjYXNlIFRHZXRQYXJhbXNNYXBwaW5nVHlwZS5QbGFpbjpcblxuICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xuICAgICAgICAgICAgZm9yIChsZXQgYXJyX3ZhbHVlIG9mIHZhbHVlKSB7XG4gICAgICAgICAgICAgIHNlYXJjaC5hcHBlbmQoa2V5LCBhcnJfdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgICAgIC8vLyBDb252ZXJ0IGRhdGVzIHRvIElTTyBmb3JtYXQgc3RyaW5nXG4gICAgICAgICAgICAgIGlmICh2YWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnRvSVNPU3RyaW5nKCk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdmFsdWUgPSBKU09OLnN0cmluZ2lmeSh2YWx1ZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlYXJjaC5hcHBlbmQoa2V5LCB2YWx1ZSk7XG5cbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBUR2V0UGFyYW1zTWFwcGluZ1R5cGUuQnJhY2tldDpcbiAgICAgICAgICAvLy8gQ29udmVydCBvYmplY3QgYW5kIGFycmF5cyB0byBxdWVyeSBwYXJhbXNcbiAgICAgICAgICBmb3IgKGxldCBrIGluIHZhbHVlKSB7XG4gICAgICAgICAgICBpZiAodmFsdWUuaGFzT3duUHJvcGVydHkoaykpIHtcbiAgICAgICAgICAgICAgdGhpcy4kX2FwcGVuZFNlYXJjaFBhcmFtcyhzZWFyY2gsIGtleSArICdbJyArIGsgKyAnXScsIHZhbHVlW2tdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cblxuICAgIHNlYXJjaC5hcHBlbmQoa2V5LCB2YWx1ZSk7XG5cbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdXJsIHRvIGJlIHJlcGxhY2VkIGJ5IFJlc291cmNlUGFyYW1zQmFzZVxuICAgKlxuICAgKiBAcGFyYW0gbWV0aG9kT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7IGFueSB8IFByb21pc2U8YW55Pn1cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByaXZhdGUgJF9nZXRVcmwobWV0aG9kT3B0aW9ucz86IFJlc291cmNlQWN0aW9uQmFzZSk6IHN0cmluZyB8IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICAvKipcbiAgICogR2V0IGdldCBwYXRoIHRvIGJlIHJlcGxhY2VkIGJ5IFJlc291cmNlUGFyYW1zQmFzZVxuICAgKlxuICAgKiBAcGFyYW0gbWV0aG9kT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7IGFueSB8IFByb21pc2U8YW55Pn1cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByaXZhdGUgJF9nZXRQYXRoKG1ldGhvZE9wdGlvbnM/OiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBzdHJpbmcgfCBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBoZWFkZXJzIHRvIGJlIHJlcGxhY2VkIGJ5IFJlc291cmNlUGFyYW1zQmFzZVxuICAgKlxuICAgKiBAcGFyYW0gbWV0aG9kT3B0aW9uc1xuICAgKiBAcmV0dXJucyB7IGFueSB8IFByb21pc2U8YW55Pn1cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByaXZhdGUgJF9nZXRIZWFkZXJzKG1ldGhvZE9wdGlvbnM/OiBSZXNvdXJjZUFjdGlvbkJhc2UpOiBhbnkgfCBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBwYXJhbXMgdG8gYmUgcmVwbGFjZWQgYnkgUmVzb3VyY2VQYXJhbXNCYXNlXG4gICAqXG4gICAqIEBwYXJhbSBtZXRob2RPcHRpb25zXG4gICAqIEByZXR1cm5zIHsgYW55IHwgUHJvbWlzZTxhbnk+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJpdmF0ZSAkX2dldFBhcmFtcyhtZXRob2RPcHRpb25zPzogUmVzb3VyY2VBY3Rpb25CYXNlKTogYW55IHwgUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgZGF0YSB0byBiZSByZXBsYWNlZCBieSBSZXNvdXJjZVBhcmFtc0Jhc2VcbiAgICpcbiAgICogQHBhcmFtIG1ldGhvZE9wdGlvbnNcbiAgICogQHJldHVybnMgeyBhbnkgfCBQcm9taXNlPGFueT59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcml2YXRlICRfZ2V0RGF0YShtZXRob2RPcHRpb25zPzogUmVzb3VyY2VBY3Rpb25CYXNlKTogYW55IHwgUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG5cbn1cblxuXG5pbnRlcmZhY2UgSVJlc291cmNlQWN0aW9uU2hlbGwge1xuICB1cmw/OiBzdHJpbmc7XG4gIGJvZHk/OiBzdHJpbmc7XG4gIHNlYXJjaD86IFVSTFNlYXJjaFBhcmFtcztcbiAgc2VhcmNoUGFyYW1zPzogYW55O1xuICB1c2VkUGF0aFBhcmFtcz86IGFueTtcbiAgcmV0dXJuSW50ZXJuYWw/OiBSZXNvdXJjZVJlc3VsdDxhbnk+IHwgUmVzb3VyY2VNb2RlbDxSZXNvdXJjZT47XG4gIHJldHVybkV4dGVybmFsPzogUmVzb3VyY2VSZXN1bHQ8YW55PiB8IFJlc291cmNlTW9kZWw8UmVzb3VyY2U+O1xuICBkYXRhPzogYW55O1xuICBwYXJhbXM/OiBhbnk7XG4gIG9wdGlvbnM/OiBSZXNvdXJjZUFjdGlvbkJhc2U7XG4gIG1haW5EZWZlcnJlZFN1YnNjcmliZXI/OiBTdWJzY3JpYmVyPGFueT47XG4gIG1haW5PYnNlcnZhYmxlPzogT2JzZXJ2YWJsZTxSZXNwb25zZT47XG4gIGV4dHJhT3B0aW9ucz86IElSZXNvdXJjZUFjdGlvbk1haW5PcHRpb25zO1xuICByZXF1ZXN0T3B0aW9ucz86IFJlcXVlc3RBcmdzO1xuICBjYWxsYmFjaz86IChkYXRhPzogYW55KSA9PiBhbnk7XG59XG5cbmludGVyZmFjZSBJUmVzb3VyY2VBY3Rpb25NYWluT3B0aW9ucyB7XG4gIHVybDogc3RyaW5nO1xuICBwYXRoOiBzdHJpbmc7XG4gIGhlYWRlcnM6IEhlYWRlcnM7XG4gIHBhcmFtczogYW55O1xuICBkYXRhOiBhbnk7XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2UudHMiLCJpbXBvcnQgeyBSZXF1ZXN0TWV0aG9kIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5cbmltcG9ydCB7XG4gIFJlc291cmNlQWN0aW9uQmFzZSxcbiAgUmVzb3VyY2VSZXN1bHRcbn0gZnJvbSAnLi9JbnRlcmZhY2VzJztcbmltcG9ydCB7IFJlc291cmNlIH0gZnJvbSAnLi9SZXNvdXJjZSc7XG5pbXBvcnQgeyBSZXNvdXJjZU1vZGVsIH0gZnJvbSAnLi9SZXNvdXJjZU1vZGVsJztcblxuXG5leHBvcnQgZnVuY3Rpb24gUmVzb3VyY2VBY3Rpb24obWV0aG9kT3B0aW9ucz86IFJlc291cmNlQWN0aW9uQmFzZSkge1xuXG4gIG1ldGhvZE9wdGlvbnMgPSBtZXRob2RPcHRpb25zIHx8IHt9O1xuXG4gIGlmIChtZXRob2RPcHRpb25zLm1ldGhvZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgbWV0aG9kT3B0aW9ucy5tZXRob2QgPSBSZXF1ZXN0TWV0aG9kLkdldDtcbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0OiBSZXNvdXJjZSwgcHJvcGVydHlLZXk6IHN0cmluZykge1xuXG4gICAgKDxhbnk+dGFyZ2V0KVtwcm9wZXJ0eUtleV0gPSBmdW5jdGlvbiAoLi4uYXJnczogYW55W10pOiBSZXNvdXJjZVJlc3VsdDxhbnk+IHwgUmVzb3VyY2VNb2RlbDxSZXNvdXJjZT4ge1xuXG4gICAgICBsZXQgZGF0YSA9IGFyZ3MubGVuZ3RoID8gYXJnc1swXSA6IG51bGw7XG4gICAgICBsZXQgcGFyYW1zID0gYXJncy5sZW5ndGggPiAxID8gYXJnc1sxXSA6IG51bGw7XG4gICAgICBsZXQgY2FsbGJhY2sgPSBhcmdzLmxlbmd0aCA+IDIgPyBhcmdzWzJdIDogbnVsbDtcblxuICAgICAgaWYgKHR5cGVvZiBkYXRhID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGNhbGxiYWNrID0gZGF0YTtcbiAgICAgICAgZGF0YSA9IG51bGw7XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiBwYXJhbXMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgY2FsbGJhY2sgPSBwYXJhbXM7XG4gICAgICAgIHBhcmFtcyA9IG51bGw7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmdldFJlc291cmNlT3B0aW9ucygpLCBtZXRob2RPcHRpb25zKTtcblxuICAgICAgcmV0dXJuIHRoaXMuJHJlc291cmNlQWN0aW9uKGRhdGEsIHBhcmFtcywgY2FsbGJhY2ssIG9wdGlvbnMpO1xuXG4gICAgLy8gICBsZXQgcmVzb3VyY2VPcHRpb25zID0gdGhpcy5nZXRSZXNvdXJjZU9wdGlvbnMoKTtcblx0XHQvL1xuICAgIC8vICAgbGV0IGlzR2V0UmVxdWVzdCA9IG1ldGhvZE9wdGlvbnMubWV0aG9kID09PSBSZXF1ZXN0TWV0aG9kLkdldDtcblx0XHQvL1xuICAgIC8vICAgbGV0IHJldDogUmVzb3VyY2VSZXN1bHQ8YW55PiB8IFJlc291cmNlTW9kZWw8UmVzb3VyY2U+ID0gbnVsbDtcblx0XHQvL1xuICAgIC8vICAgbGV0IG1hcDogUmVzb3VyY2VSZXNwb25zZU1hcCA9IG1ldGhvZE9wdGlvbnMubWFwID8gbWV0aG9kT3B0aW9ucy5tYXAgOiB0aGlzLm1hcDtcbiAgICAvLyAgIGxldCBmaWx0ZXI6IFJlc291cmNlUmVzcG9uc2VGaWx0ZXIgPSBtZXRob2RPcHRpb25zLmZpbHRlciA/IG1ldGhvZE9wdGlvbnMuZmlsdGVyIDogdGhpcy5maWx0ZXI7XG4gICAgLy8gICBsZXQgaW5pdE9iamVjdDogUmVzb3VyY2VSZXNwb25zZUluaXRSZXN1bHQgPSBtZXRob2RPcHRpb25zLmluaXRSZXN1bHRPYmplY3QgP1xuICAgIC8vICAgICBtZXRob2RPcHRpb25zLmluaXRSZXN1bHRPYmplY3QgOiB0aGlzLiRpbml0UmVzdWx0T2JqZWN0O1xuXHRcdC8vXG4gICAgLy8gICBpZiAobWV0aG9kT3B0aW9ucy5pc0xhenkpIHtcbiAgICAvLyAgICAgcmV0ID0ge307XG4gICAgLy8gICB9IGVsc2Uge1xuXHRcdC8vXG4gICAgLy8gICAgIGlmIChtZXRob2RPcHRpb25zLmlzQXJyYXkpIHtcbiAgICAvLyAgICAgICByZXQgPSBbXTtcbiAgICAvLyAgICAgfSBlbHNlIHtcblx0XHQvL1xuICAgIC8vICAgICAgIGlmIChkYXRhICYmIGRhdGEuJHJlc291cmNlID09PSB0aGlzKSB7XG4gICAgLy8gICAgICAgICAvLyBTZXR0aW5nIGRhdGEgdG8gcmV0XG4gICAgLy8gICAgICAgICByZXQgPSBkYXRhO1xuICAgIC8vICAgICAgIH0gZWxzZSB7XG4gICAgLy8gICAgICAgICByZXQgPSBpbml0T2JqZWN0KCk7XG4gICAgLy8gICAgICAgfVxuXHRcdC8vXG4gICAgLy8gICAgIH1cbiAgICAvLyAgIH1cblx0XHQvL1xuICAgIC8vICAgaWYgKGRhdGEgJiYgIW1ldGhvZE9wdGlvbnMuc2tpcERhdGFDbGVhbmluZykge1xuICAgIC8vICAgICBkYXRhID0gZGF0YS50b0pTT04gPyBkYXRhLnRvSlNPTigpIDogdGhpcy5jbGVhbkRhdGEoZGF0YSk7XG4gICAgLy8gICB9XG5cdFx0Ly9cbiAgICAvLyAgIGxldCBtYWluRGVmZXJyZWRTdWJzY3JpYmVyOiBTdWJzY3JpYmVyPGFueT4gPSBudWxsO1xuICAgIC8vICAgbGV0IG1haW5PYnNlcnZhYmxlOiBPYnNlcnZhYmxlPFJlc3BvbnNlPiA9IG51bGw7XG5cdFx0Ly9cbiAgICAvLyAgIHJldC4kcmVzb2x2ZWQgPSBmYWxzZTtcbiAgICAvLyAgIHJldC4kb2JzZXJ2YWJsZSA9IE9ic2VydmFibGUuY3JlYXRlKChzdWJzY3JpYmVyOiBTdWJzY3JpYmVyPGFueT4pID0+IHtcbiAgICAvLyAgICAgbWFpbkRlZmVycmVkU3Vic2NyaWJlciA9IHN1YnNjcmliZXI7XG4gICAgLy8gICB9KS5mbGF0TWFwKCgpID0+IG1haW5PYnNlcnZhYmxlKTtcbiAgICAvLyAgIHJldC4kYWJvcnRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgIC8vICAgICByZXQuJHJlc29sdmVkID0gdHJ1ZTtcbiAgICAvLyAgIH07XG4gICAgLy8gICByZXQuJHJlc291cmNlID0gdGhpcztcblx0XHQvL1xuICAgIC8vICAgbGV0IHJldHVybkRhdGEgPSByZXQ7XG5cdFx0Ly9cbiAgICAvLyAgIGlmIChtZXRob2RPcHRpb25zLmxlYW4pIHtcblx0XHQvL1xuICAgIC8vICAgfVxuXHRcdC8vXG5cdFx0Ly9cbiAgICAvLyAgIGZ1bmN0aW9uIHJlbGVhc2VNYWluRGVmZXJyZWRTdWJzY3JpYmVyKCkge1xuICAgIC8vICAgICBpZiAobWFpbkRlZmVycmVkU3Vic2NyaWJlcikge1xuICAgIC8vICAgICAgIG1haW5EZWZlcnJlZFN1YnNjcmliZXIubmV4dCgpO1xuICAgIC8vICAgICAgIG1haW5EZWZlcnJlZFN1YnNjcmliZXIuY29tcGxldGUoKTtcbiAgICAvLyAgICAgICBtYWluRGVmZXJyZWRTdWJzY3JpYmVyID0gbnVsbDtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgfVxuXHRcdC8vXG4gICAgLy8gICBpZiAoIW1ldGhvZE9wdGlvbnMuaXNMYXp5KSB7XG4gICAgLy8gICAgIHJldC4kb2JzZXJ2YWJsZSA9IHJldC4kb2JzZXJ2YWJsZS5wdWJsaXNoKCk7XG4gICAgLy8gICAgICg8Q29ubmVjdGFibGVPYnNlcnZhYmxlPGFueT4+cmV0LiRvYnNlcnZhYmxlKS5jb25uZWN0KCk7XG4gICAgLy8gICB9XG5cdFx0Ly9cbiAgICAvLyAgIFByb21pc2UuYWxsKFtcbiAgICAvLyAgICAgUHJvbWlzZS5yZXNvbHZlKG1ldGhvZE9wdGlvbnMudXJsIHx8IHRoaXMuJGdldFVybChtZXRob2RPcHRpb25zKSksXG4gICAgLy8gICAgIFByb21pc2UucmVzb2x2ZShtZXRob2RPcHRpb25zLnBhdGggfHwgdGhpcy4kZ2V0UGF0aChtZXRob2RPcHRpb25zKSksXG4gICAgLy8gICAgIFByb21pc2UucmVzb2x2ZShtZXRob2RPcHRpb25zLmhlYWRlcnMgfHwgdGhpcy4kZ2V0SGVhZGVycyhtZXRob2RPcHRpb25zKSksXG4gICAgLy8gICAgIFByb21pc2UucmVzb2x2ZShtZXRob2RPcHRpb25zLnBhcmFtcyB8fCB0aGlzLiRnZXRQYXJhbXMobWV0aG9kT3B0aW9ucykpLFxuICAgIC8vICAgICBQcm9taXNlLnJlc29sdmUobWV0aG9kT3B0aW9ucy5kYXRhIHx8IHRoaXMuZ2V0RGF0YShtZXRob2RPcHRpb25zKSlcbiAgICAvLyAgIF0pXG4gICAgLy8gICAgIC50aGVuKChkYXRhQWxsOiBhbnlbXSkgPT4ge1xuXHRcdC8vXG4gICAgLy8gICAgICAgaWYgKHJldC4kcmVzb2x2ZWQpIHtcbiAgICAvLyAgICAgICAgIG1haW5PYnNlcnZhYmxlID0gT2JzZXJ2YWJsZS5jcmVhdGUoKG9ic2VydmVyOiBhbnkpID0+IHtcbiAgICAvLyAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChudWxsKTtcbiAgICAvLyAgICAgICAgIH0pO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICByZWxlYXNlTWFpbkRlZmVycmVkU3Vic2NyaWJlcigpO1xuICAgIC8vICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgIGxldCB1cmw6IHN0cmluZyA9IGRhdGFBbGxbMF0gKyBkYXRhQWxsWzFdO1xuICAgIC8vICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoZGF0YUFsbFsyXSk7XG4gICAgLy8gICAgICAgbGV0IGRlZlBhdGhQYXJhbXMgPSBkYXRhQWxsWzNdO1xuXHRcdC8vXG4gICAgLy8gICAgICAgbGV0IHVzZWRQYXRoUGFyYW1zOiBhbnkgPSB7fTtcblx0XHQvL1xuICAgIC8vICAgICAgIGlmICghQXJyYXkuaXNBcnJheShkYXRhKSB8fCBwYXJhbXMpIHtcblx0XHQvL1xuICAgIC8vICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KGRhdGEpKSB7XG4gICAgLy8gICAgICAgICAgIGRhdGEgPSBPYmplY3QuYXNzaWduKHt9LCBkYXRhQWxsWzRdLCBkYXRhKTtcbiAgICAvLyAgICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgICAgbGV0IHBhdGhQYXJhbXMgPSB1cmwubWF0Y2goL3soW159XSopfS9nKSB8fCBbXTtcblx0XHQvL1xuICAgIC8vICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXRoUGFyYW1zLmxlbmd0aDsgaSsrKSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgbGV0IHBhdGhQYXJhbSA9IHBhdGhQYXJhbXNbaV07XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgbGV0IHBhdGhLZXkgPSBwYXRoUGFyYW0uc3Vic3RyKDEsIHBhdGhQYXJhbS5sZW5ndGggLSAyKTtcbiAgICAvLyAgICAgICAgICAgbGV0IGlzTWFuZGF0b3J5ID0gcGF0aEtleVswXSA9PT0gJyEnO1xuICAgIC8vICAgICAgICAgICBpZiAoaXNNYW5kYXRvcnkpIHtcbiAgICAvLyAgICAgICAgICAgICBwYXRoS2V5ID0gcGF0aEtleS5zdWJzdHIoMSk7XG4gICAgLy8gICAgICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgICAgICBsZXQgaXNHZXRPbmx5ID0gcGF0aEtleVswXSA9PT0gJzonO1xuICAgIC8vICAgICAgICAgICBpZiAoaXNHZXRPbmx5KSB7XG4gICAgLy8gICAgICAgICAgICAgcGF0aEtleSA9IHBhdGhLZXkuc3Vic3RyKDEpO1xuICAgIC8vICAgICAgICAgICB9XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgbGV0IHZhbHVlID0gZ2V0VmFsdWVGb3JQYXRoKHBhdGhLZXksIGRlZlBhdGhQYXJhbXMsIHBhcmFtcyB8fCBkYXRhLCB1c2VkUGF0aFBhcmFtcyk7XG4gICAgLy8gICAgICAgICAgIGlmIChpc0dldE9ubHkgJiYgIXBhcmFtcykge1xuICAgIC8vICAgICAgICAgICAgIGRlbGV0ZSBkYXRhW3BhdGhLZXldO1xuICAgIC8vICAgICAgICAgICB9XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgaWYgKGlzTnVsbE9yVW5kZWZpbmVkKHZhbHVlKSkge1xuICAgIC8vICAgICAgICAgICAgIGlmIChpc01hbmRhdG9yeSkge1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICBsZXQgY29uc29sZU1zZyA9IGBNYW5kYXRvcnkgJHtwYXRoUGFyYW19IHBhdGggcGFyYW1ldGVyIGlzIG1pc3NpbmdgO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICBtYWluT2JzZXJ2YWJsZSA9IE9ic2VydmFibGUuY3JlYXRlKChvYnNlcnZlcjogYW55KSA9PiB7XG4gICAgLy8gICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihjb25zb2xlTXNnKSk7XG4gICAgLy8gICAgICAgICAgICAgICB9KTtcblx0XHQvL1xuICAgIC8vICAgICAgICAgICAgICAgY29uc29sZS53YXJuKGNvbnNvbGVNc2cpO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICByZWxlYXNlTWFpbkRlZmVycmVkU3Vic2NyaWJlcigpO1xuICAgIC8vICAgICAgICAgICAgICAgcmV0dXJuO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgfVxuICAgIC8vICAgICAgICAgICAgIHVybCA9IHVybC5zdWJzdHIoMCwgdXJsLmluZGV4T2YocGF0aFBhcmFtKSk7XG4gICAgLy8gICAgICAgICAgICAgYnJlYWs7XG4gICAgLy8gICAgICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgICAgICAvLyBSZXBsYWNpbmcgaW4gdGhlIHVybFxuICAgIC8vICAgICAgICAgICB1cmwgPSB1cmwucmVwbGFjZShwYXRoUGFyYW0sIHZhbHVlKTtcbiAgICAvLyAgICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIFJlbW92aW5nIGRvdWJsZSBzbGFzaGVkIGZyb20gZmluYWwgdXJsXG4gICAgLy8gICAgICAgdXJsID0gdXJsLnJlcGxhY2UoL1xcL1xcLysvZywgJy8nKTtcbiAgICAvLyAgICAgICBpZiAodXJsLnN0YXJ0c1dpdGgoJ2h0dHAnKSkge1xuICAgIC8vICAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UoJzovJywgJzovLycpO1xuICAgIC8vICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIFJlbW92ZSB0cmFpbGluZyBzbGFzaFxuICAgIC8vICAgICAgIGlmICh0eXBlb2YgbWV0aG9kT3B0aW9ucy5yZW1vdmVUcmFpbGluZ1NsYXNoID09PSAndW5kZWZpbmVkJykge1xuICAgIC8vICAgICAgICAgbWV0aG9kT3B0aW9ucy5yZW1vdmVUcmFpbGluZ1NsYXNoID0gdGhpcy5yZW1vdmVUcmFpbGluZ1NsYXNoKCk7XG4gICAgLy8gICAgICAgfVxuICAgIC8vICAgICAgIGlmIChtZXRob2RPcHRpb25zLnJlbW92ZVRyYWlsaW5nU2xhc2gpIHtcbiAgICAvLyAgICAgICAgIHdoaWxlICh1cmxbdXJsLmxlbmd0aCAtIDFdID09PSAnLycpIHtcbiAgICAvLyAgICAgICAgICAgdXJsID0gdXJsLnN1YnN0cigwLCB1cmwubGVuZ3RoIC0gMSk7XG4gICAgLy8gICAgICAgICB9XG4gICAgLy8gICAgICAgfVxuXHRcdC8vXG5cdFx0Ly9cbiAgICAvLyAgICAgICAvLyBSZW1vdmUgbWFwcGVkIHBhcmFtc1xuICAgIC8vICAgICAgIGZvciAobGV0IGtleSBpbiBkZWZQYXRoUGFyYW1zKSB7XG4gICAgLy8gICAgICAgICBpZiAoZGVmUGF0aFBhcmFtc1trZXldWzBdID09PSAnQCcpIHtcbiAgICAvLyAgICAgICAgICAgZGVsZXRlIGRlZlBhdGhQYXJhbXNba2V5XTtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgICB9XG5cdFx0Ly9cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIERlZmF1bHQgc2VhcmNoIHBhcmFtcyBvciBkYXRhXG4gICAgLy8gICAgICAgbGV0IGJvZHk6IHN0cmluZyA9IG51bGw7XG5cdFx0Ly9cbiAgICAvLyAgICAgICBsZXQgc2VhcmNoUGFyYW1zOiBhbnk7XG4gICAgLy8gICAgICAgaWYgKGlzR2V0UmVxdWVzdCkge1xuICAgIC8vICAgICAgICAgLy8gR0VUXG4gICAgLy8gICAgICAgICBzZWFyY2hQYXJhbXMgPSBPYmplY3QuYXNzaWduKHt9LCBkZWZQYXRoUGFyYW1zLCBkYXRhKTtcbiAgICAvLyAgICAgICB9IGVsc2Uge1xuICAgIC8vICAgICAgICAgLy8gTk9OIEdFVFxuICAgIC8vICAgICAgICAgaWYgKGRhdGEpIHtcbiAgICAvLyAgICAgICAgICAgbGV0IF9ib2R5OiBhbnkgPSB7fTtcbiAgICAvLyAgICAgICAgICAgaWYgKG1ldGhvZE9wdGlvbnMucm9vdE5vZGUpIHtcbiAgICAvLyAgICAgICAgICAgICBfYm9keVtgJHttZXRob2RPcHRpb25zLnJvb3ROb2RlfWBdID0gZGF0YTtcbiAgICAvLyAgICAgICAgICAgfSBlbHNlIHtcbiAgICAvLyAgICAgICAgICAgICBfYm9keSA9IGRhdGE7XG4gICAgLy8gICAgICAgICAgIH1cbiAgICAvLyAgICAgICAgICAgYm9keSA9IEpTT04uc3RyaW5naWZ5KF9ib2R5KTtcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgICAgIHNlYXJjaFBhcmFtcyA9IGRlZlBhdGhQYXJhbXM7XG4gICAgLy8gICAgICAgfVxuXHRcdC8vXG5cdFx0Ly9cbiAgICAvLyAgICAgICAvLyBTZXR0aW5nIHNlYXJjaCBwYXJhbXNcbiAgICAvLyAgICAgICBsZXQgc2VhcmNoOiBVUkxTZWFyY2hQYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKCk7XG5cdFx0Ly9cbiAgICAvLyAgICAgICBpZiAoIXBhcmFtcykge1xuICAgIC8vICAgICAgICAgZm9yIChsZXQga2V5IGluIHNlYXJjaFBhcmFtcykge1xuICAgIC8vICAgICAgICAgICBpZiAoc2VhcmNoUGFyYW1zLmhhc093blByb3BlcnR5KGtleSkgJiYgIXVzZWRQYXRoUGFyYW1zW2tleV0pIHtcbiAgICAvLyAgICAgICAgICAgICBsZXQgdmFsdWU6IGFueSA9IHNlYXJjaFBhcmFtc1trZXldO1xuICAgIC8vICAgICAgICAgICAgIGFwcGVuZFNlYXJjaFBhcmFtcyhzZWFyY2gsIGtleSwgdmFsdWUpO1xuICAgIC8vICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICB9XG4gICAgLy8gICAgICAgfVxuXHRcdC8vXG4gICAgLy8gICAgICAgLy8gQWRkaW5nIFRTIGlmIG5lZWRlZFxuICAgIC8vICAgICAgIGxldCB0c05hbWUgPSBtZXRob2RPcHRpb25zLmFkZFRpbWVzdGFtcCB8fCByZXNvdXJjZU9wdGlvbnMuYWRkVGltZXN0YW1wO1xuICAgIC8vICAgICAgIGlmICh0c05hbWUpIHtcbiAgICAvLyAgICAgICAgIGlmICh0c05hbWUgPT09IHRydWUpIHtcbiAgICAvLyAgICAgICAgICAgdHNOYW1lID0gJ3RzJztcbiAgICAvLyAgICAgICAgIH1cbiAgICAvLyAgICAgICAgIHNlYXJjaC5hcHBlbmQodHNOYW1lLCAnJyArIG5ldyBEYXRlKCkuZ2V0VGltZSgpKTtcbiAgICAvLyAgICAgICB9XG5cdFx0Ly9cbiAgICAvLyAgICAgICAvLyBSZW1vdmluZyBDb250ZW50LVR5cGUgaGVhZGVyIGlmIG5vIGJvZHlcbiAgICAvLyAgICAgICBpZiAoIWJvZHkpIHtcbiAgICAvLyAgICAgICAgIGhlYWRlcnMuZGVsZXRlKCdjb250ZW50LXR5cGUnKTtcbiAgICAvLyAgICAgICB9XG5cdFx0Ly9cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIENyZWF0aW5nIHJlcXVlc3Qgb3B0aW9uc1xuICAgIC8vICAgICAgIGxldCByZXF1ZXN0T3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7XG4gICAgLy8gICAgICAgICBtZXRob2Q6IG1ldGhvZE9wdGlvbnMubWV0aG9kLFxuICAgIC8vICAgICAgICAgaGVhZGVyczogaGVhZGVycyxcbiAgICAvLyAgICAgICAgIGJvZHk6IGJvZHksXG4gICAgLy8gICAgICAgICB1cmw6IHVybCxcbiAgICAvLyAgICAgICAgIHNlYXJjaDogc2VhcmNoLFxuICAgIC8vICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiBtZXRob2RPcHRpb25zLndpdGhDcmVkZW50aWFscyB8fCByZXNvdXJjZU9wdGlvbnMud2l0aENyZWRlbnRpYWxzXG4gICAgLy8gICAgICAgfSk7XG5cdFx0Ly9cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIENyZWF0aW5nIHJlcXVlc3Qgb2JqZWN0XG4gICAgLy8gICAgICAgbGV0IHJlcSA9IG5ldyBSZXF1ZXN0KHJlcXVlc3RPcHRpb25zKTtcblx0XHQvL1xuICAgIC8vICAgICAgIHJlcSA9IG1ldGhvZE9wdGlvbnMucmVxdWVzdEludGVyY2VwdG9yID9cbiAgICAvLyAgICAgICAgIG1ldGhvZE9wdGlvbnMucmVxdWVzdEludGVyY2VwdG9yKHJlcSwgbWV0aG9kT3B0aW9ucykgOlxuICAgIC8vICAgICAgICAgdGhpcy4kcmVxdWVzdEludGVyY2VwdG9yKHJlcSwgbWV0aG9kT3B0aW9ucyk7XG5cdFx0Ly9cbiAgICAvLyAgICAgICBpZiAoIXJlcSkge1xuICAgIC8vICAgICAgICAgbWFpbk9ic2VydmFibGUgPSBPYnNlcnZhYmxlLmNyZWF0ZSgob2JzZXJ2ZXI6IGFueSkgPT4ge1xuICAgIC8vICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ1JlcXVlc3QgaXMgbnVsbCcpKTtcbiAgICAvLyAgICAgICAgIH0pO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICBjb25zb2xlLndhcm4oJ1JlcXVlc3QgaXMgbnVsbCcpO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICByZWxlYXNlTWFpbkRlZmVycmVkU3Vic2NyaWJlcigpO1xuICAgIC8vICAgICAgICAgcmV0dXJuO1xuICAgIC8vICAgICAgIH1cblx0XHQvL1xuICAgIC8vICAgICAgIC8vIERvaW5nIHRoZSByZXF1ZXN0XG4gICAgLy8gICAgICAgbGV0IHJlcXVlc3RPYnNlcnZhYmxlID0gdGhpcy5fcmVxdWVzdChyZXEsIG1ldGhvZE9wdGlvbnMpO1xuXHRcdC8vXG5cdFx0Ly9cbiAgICAvLyAgICAgICBpZiAobWV0aG9kT3B0aW9ucy5pc0xhenkpIHtcblx0XHQvL1xuICAgIC8vICAgICAgICAgbWFpbk9ic2VydmFibGUgPSByZXF1ZXN0T2JzZXJ2YWJsZTtcblx0XHQvL1xuICAgIC8vICAgICAgIH0gZWxzZSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgIG1haW5PYnNlcnZhYmxlID0gT2JzZXJ2YWJsZS5jcmVhdGUoKHN1YnNjcmliZXI6IFN1YnNjcmliZXI8YW55PikgPT4ge1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgIGxldCByZXFTdWJzY3I6IFN1YnNjcmlwdGlvbiA9IHJlcXVlc3RPYnNlcnZhYmxlLnN1YnNjcmliZShcbiAgICAvLyAgICAgICAgICAgICAocmVzcDogYW55KSA9PiB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgIGlmIChyZXNwICE9PSBudWxsKSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgaWYgKG1ldGhvZE9wdGlvbnMuaXNBcnJheSkge1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICAgICAgLy8gRXhwZWN0aW5nIGFycmF5XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcCkpIHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1JldHVybmVkIGRhdGEgc2hvdWxkIGJlIGFuIGFycmF5LiBSZWNlaXZlZCcsIHJlc3ApO1xuICAgIC8vICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHJldHVybkRhdGEucHVzaChcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgLi4ucmVzcFxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIC5maWx0ZXIoZmlsdGVyKVxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAobWFwKVxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKHJlc3BJdGVtOiBhbnkpID0+IHtcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbWV0aG9kT3B0aW9ucy5sZWFuKSB7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BJdGVtLiRyZXNvdXJjZSA9IHRoaXM7XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2V0RGF0YVRvT2JqZWN0KGluaXRPYmplY3QoKSwgcmVzcEl0ZW0pO1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICApO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICAgICAgfVxuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgICAvLyBFeHBlY3Rpbmcgb2JqZWN0XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShyZXNwKSkge1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignUmV0dXJuZWQgZGF0YSBzaG91bGQgYmUgYW4gb2JqZWN0LiBSZWNlaXZlZCcsIHJlc3ApO1xuICAgIC8vICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIGlmIChmaWx0ZXIocmVzcCkpIHtcblx0XHQvL1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgICBzZXREYXRhVG9PYmplY3QocmV0dXJuRGF0YSwgbWFwKHJlc3ApKTtcblx0XHQvL1xuICAgIC8vICAgICAgICAgICAgICAgICAgICAgfVxuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgICAgICAgfVxuICAgIC8vICAgICAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICAgICB9XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgICAgIHJldC4kcmVzb2x2ZWQgPSB0cnVlO1xuICAgIC8vICAgICAgICAgICAgICAgc3Vic2NyaWJlci5uZXh0KHJldHVybkRhdGEpO1xuXHRcdC8vXG4gICAgLy8gICAgICAgICAgICAgfSxcbiAgICAvLyAgICAgICAgICAgICAoZXJyOiBhbnkpID0+IHN1YnNjcmliZXIuZXJyb3IoZXJyKSxcbiAgICAvLyAgICAgICAgICAgICAoKSA9PiB7XG4gICAgLy8gICAgICAgICAgICAgICByZXQuJHJlc29sdmVkID0gdHJ1ZTtcbiAgICAvLyAgICAgICAgICAgICAgIHN1YnNjcmliZXIuY29tcGxldGUoKTtcbiAgICAvLyAgICAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgIC8vICAgICAgICAgICAgICAgICBjYWxsYmFjayhyZXR1cm5EYXRhKTtcbiAgICAvLyAgICAgICAgICAgICAgIH1cbiAgICAvLyAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICk7XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgICAgcmV0LiRhYm9ydFJlcXVlc3QgPSAoKSA9PiB7XG4gICAgLy8gICAgICAgICAgICAgaWYgKHJldC4kcmVzb2x2ZWQpIHtcbiAgICAvLyAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAvLyAgICAgICAgICAgICB9XG4gICAgLy8gICAgICAgICAgICAgcmVxU3Vic2NyLnVuc3Vic2NyaWJlKCk7XG4gICAgLy8gICAgICAgICAgICAgcmV0LiRyZXNvbHZlZCA9IHRydWU7XG4gICAgLy8gICAgICAgICAgIH07XG5cdFx0Ly9cbiAgICAvLyAgICAgICAgIH0pO1xuXHRcdC8vXG4gICAgLy8gICAgICAgfVxuXHRcdC8vXG4gICAgLy8gICAgICAgcmVsZWFzZU1haW5EZWZlcnJlZFN1YnNjcmliZXIoKTtcblx0XHQvL1xuICAgIC8vICAgICB9KTtcblx0XHQvL1xuICAgIC8vICAgcmV0dXJuIHJldHVybkRhdGE7XG5cbiAgICB9O1xuXG4gIH07XG5cbn1cblxuXG4vLyBleHBvcnQgZnVuY3Rpb24gc2V0RGF0YVRvT2JqZWN0KHJldDogYW55LCByZXNwOiBhbnkpOiBhbnkge1xuLy9cbi8vICAgaWYgKHJldC4kc2V0RGF0YSkge1xuLy8gICAgIHJldC4kc2V0RGF0YShyZXNwKTtcbi8vICAgfSBlbHNlIHtcbi8vICAgICBPYmplY3QuYXNzaWduKHJldCwgcmVzcCk7XG4vLyAgIH1cbi8vXG4vLyAgIHJldHVybiByZXQ7XG4vL1xuLy8gfVxuXG4vLyBleHBvcnQgZnVuY3Rpb24gYXBwZW5kU2VhcmNoUGFyYW1zKHNlYXJjaDogVVJMU2VhcmNoUGFyYW1zLCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuLy8gICAvLy8gQ29udmVydCBkYXRlcyB0byBJU08gZm9ybWF0IHN0cmluZ1xuLy8gICBpZiAodmFsdWUgaW5zdGFuY2VvZiBEYXRlKSB7XG4vLyAgICAgc2VhcmNoLmFwcGVuZChrZXksIHZhbHVlLnRvSVNPU3RyaW5nKCkpO1xuLy8gICAgIHJldHVybjtcbi8vICAgfVxuLy9cbi8vICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpIHtcbi8vXG4vLyAgICAgc3dpdGNoIChSZXNvdXJjZUdsb2JhbENvbmZpZy5nZXRQYXJhbXNNYXBwaW5nVHlwZSkge1xuLy9cbi8vICAgICAgIGNhc2UgVEdldFBhcmFtc01hcHBpbmdUeXBlLlBsYWluOlxuLy9cbi8vICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4vLyAgICAgICAgICAgZm9yIChsZXQgYXJyX3ZhbHVlIG9mIHZhbHVlKSB7XG4vLyAgICAgICAgICAgICBzZWFyY2guYXBwZW5kKGtleSwgYXJyX3ZhbHVlKTtcbi8vICAgICAgICAgICB9XG4vLyAgICAgICAgIH0gZWxzZSB7XG4vL1xuLy8gICAgICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4vLyAgICAgICAgICAgICAvLy8gQ29udmVydCBkYXRlcyB0byBJU08gZm9ybWF0IHN0cmluZ1xuLy8gICAgICAgICAgICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgRGF0ZSkge1xuLy8gICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnRvSVNPU3RyaW5nKCk7XG4vLyAgICAgICAgICAgICB9IGVsc2Uge1xuLy8gICAgICAgICAgICAgICB2YWx1ZSA9IEpTT04uc3RyaW5naWZ5KHZhbHVlKTtcbi8vICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICB9XG4vLyAgICAgICAgICAgc2VhcmNoLmFwcGVuZChrZXksIHZhbHVlKTtcbi8vXG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgYnJlYWs7XG4vL1xuLy8gICAgICAgY2FzZSBUR2V0UGFyYW1zTWFwcGluZ1R5cGUuQnJhY2tldDpcbi8vICAgICAgICAgLy8vIENvbnZlcnQgb2JqZWN0IGFuZCBhcnJheXMgdG8gcXVlcnkgcGFyYW1zXG4vLyAgICAgICAgIGZvciAobGV0IGsgaW4gdmFsdWUpIHtcbi8vICAgICAgICAgICBpZiAodmFsdWUuaGFzT3duUHJvcGVydHkoaykpIHtcbi8vICAgICAgICAgICAgIGFwcGVuZFNlYXJjaFBhcmFtcyhzZWFyY2gsIGtleSArICdbJyArIGsgKyAnXScsIHZhbHVlW2tdKTtcbi8vICAgICAgICAgICB9XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgYnJlYWs7XG4vLyAgICAgfVxuLy9cbi8vICAgICByZXR1cm47XG4vLyAgIH1cbi8vXG4vL1xuLy8gICBzZWFyY2guYXBwZW5kKGtleSwgdmFsdWUpO1xuLy9cbi8vIH1cblxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vfi90c2xpbnQtbG9hZGVyIS4vc3JjL1Jlc291cmNlQWN0aW9uLnRzIiwiaW1wb3J0IHsgUHJvdmlkZXIsIFR5cGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAgfSBmcm9tICdAYW5ndWxhci9odHRwJztcblxuaW1wb3J0IHsgUmVzb3VyY2UgfSBmcm9tICcuL1Jlc291cmNlJztcblxuZXhwb3J0IGNsYXNzIFJlc291cmNlUHJvdmlkZXJzIHtcblxuICBzdGF0aWMgbWFpblByb3ZpZGVyc05hbWU6IHN0cmluZyA9ICdfX21haW5Qcm92aWRlcnMnO1xuICBzdGF0aWMgcHJvdmlkZXJzOiB7IFtpZDogc3RyaW5nXTogUHJvdmlkZXJbXSB9ID0ge1xuICAgIF9fbWFpblByb3ZpZGVyczogW11cbiAgfTtcblxuICBzdGF0aWMgYWRkKHJlc291cmNlOiBUeXBlPFJlc291cmNlPiwgc3ViU2V0OiBzdHJpbmcgPSBudWxsKSB7XG5cbiAgICBpZiAoIXN1YlNldCkge1xuICAgICAgc3ViU2V0ID0gdGhpcy5tYWluUHJvdmlkZXJzTmFtZTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMucHJvdmlkZXJzW3N1YlNldF0pIHtcbiAgICAgIHRoaXMucHJvdmlkZXJzW3N1YlNldF0gPSBbXTtcbiAgICB9XG5cbiAgICBsZXQgZGVwczogYW55W10gPSAoPGFueT5SZWZsZWN0KS5nZXRNZXRhZGF0YSgnZGVzaWduOnBhcmFtdHlwZXMnLCByZXNvdXJjZSk7XG5cbiAgICBpZiAoIWRlcHMgfHwgZGVwcy5sZW5ndGggPT09IDApIHtcbiAgICAgIGRlcHMgPSBbSHR0cF07XG4gICAgfVxuXG4gICAgdGhpcy5wcm92aWRlcnNbc3ViU2V0XS5wdXNoKFxuICAgICAge1xuICAgICAgICBwcm92aWRlOiByZXNvdXJjZSxcbiAgICAgICAgdXNlRmFjdG9yeTogKC4uLmFyZ3M6IGFueVtdKSA9PiBuZXcgcmVzb3VyY2UoLi4uYXJncyksXG4gICAgICAgIGRlcHM6IGRlcHNcbiAgICAgIH1cbiAgICApO1xuXG4gIH1cblxuICBzdGF0aWMgZ2V0KHN1YlNldDogc3RyaW5nID0gbnVsbCk6IFByb3ZpZGVyW10ge1xuXG4gICAgaWYgKCFzdWJTZXQpIHtcbiAgICAgIHN1YlNldCA9IHRoaXMubWFpblByb3ZpZGVyc05hbWU7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMucHJvdmlkZXJzW3N1YlNldF0gfHwgW107XG5cbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2VQcm92aWRlcnMudHMiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfNF9fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIFwiQGFuZ3VsYXIvY29yZVwiXG4vLyBtb2R1bGUgaWQgPSA0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImV4cG9ydCBlbnVtIFRHZXRQYXJhbXNNYXBwaW5nVHlwZSB7XG4gIFBsYWluLFxuICBCcmFja2V0XG59XG5cbmV4cG9ydCBjbGFzcyBSZXNvdXJjZUdsb2JhbENvbmZpZyB7XG4gIHN0YXRpYyB1cmw6IHN0cmluZyB8IFByb21pc2U8c3RyaW5nPiA9IG51bGw7XG4gIHN0YXRpYyBwYXRoOiBzdHJpbmcgfCBQcm9taXNlPHN0cmluZz4gPSBudWxsO1xuICBzdGF0aWMgaGVhZGVyczogYW55IHwgUHJvbWlzZTxhbnk+ID0ge1xuICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICB9O1xuICBzdGF0aWMgcGFyYW1zOiBhbnkgfCBQcm9taXNlPGFueT4gPSBudWxsO1xuICBzdGF0aWMgZGF0YTogYW55IHwgUHJvbWlzZTxhbnk+ID0gbnVsbDtcblxuICBzdGF0aWMgZ2V0UGFyYW1zTWFwcGluZ1R5cGU6IGFueSA9IFRHZXRQYXJhbXNNYXBwaW5nVHlwZS5QbGFpbjtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL34vdHNsaW50LWxvYWRlciEuL3NyYy9SZXNvdXJjZUdsb2JhbENvbmZpZy50cyIsImltcG9ydCB7IFR5cGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUmVzb3VyY2VQYXJhbXNCYXNlIH0gZnJvbSAnLi9JbnRlcmZhY2VzJztcbmltcG9ydCB7IFJlc291cmNlUHJvdmlkZXJzIH0gZnJvbSAnLi9SZXNvdXJjZVByb3ZpZGVycyc7XG5pbXBvcnQgeyBSZXNvdXJjZSB9IGZyb20gJy4vUmVzb3VyY2UnO1xuXG5cbmV4cG9ydCBmdW5jdGlvbiBSZXNvdXJjZVBhcmFtcyhwYXJhbXM6IFJlc291cmNlUGFyYW1zQmFzZSA9IHt9KSB7XG5cbiAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQ6IFR5cGU8UmVzb3VyY2U+KSB7XG5cblxuICAgIHRhcmdldC5wcm90b3R5cGUuZ2V0UmVzb3VyY2VPcHRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHBhcmFtcztcbiAgICB9O1xuXG4gICAgaWYgKHBhcmFtcy5hZGQyUHJvdmlkZXMgIT09IGZhbHNlKSB7XG4gICAgICBSZXNvdXJjZVByb3ZpZGVycy5hZGQodGFyZ2V0LCBwYXJhbXMucHJvdmlkZXJzU3ViU2V0KTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIHBhcmFtcy5yZW1vdmVUcmFpbGluZ1NsYXNoICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgdGFyZ2V0LnByb3RvdHlwZS5yZW1vdmVUcmFpbGluZ1NsYXNoID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gISFwYXJhbXMucmVtb3ZlVHJhaWxpbmdTbGFzaDtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgaWYgKHBhcmFtcy51cmwpIHtcbiAgICAgIHRhcmdldC5wcm90b3R5cGUuX2dldFVybCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBhcmFtcy51cmw7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGlmIChwYXJhbXMucGF0aCkge1xuICAgICAgdGFyZ2V0LnByb3RvdHlwZS5fZ2V0UGF0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBhcmFtcy5wYXRoO1xuICAgICAgfTtcbiAgICB9XG5cbiAgICBpZiAocGFyYW1zLmhlYWRlcnMpIHtcbiAgICAgIHRhcmdldC5wcm90b3R5cGUuX2dldEhlYWRlcnMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBwYXJhbXMuaGVhZGVycztcbiAgICAgIH07XG4gICAgfVxuXG4gICAgaWYgKHBhcmFtcy5wYXJhbXMpIHtcbiAgICAgIHRhcmdldC5wcm90b3R5cGUuX2dldFBhcmFtcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBhcmFtcy5wYXJhbXM7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGlmIChwYXJhbXMuZGF0YSkge1xuICAgICAgdGFyZ2V0LnByb3RvdHlwZS5fZ2V0RGF0YSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHBhcmFtcy5kYXRhO1xuICAgICAgfTtcbiAgICB9XG5cbiAgfTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL34vdHNsaW50LWxvYWRlciEuL3NyYy9SZXNvdXJjZVBhcmFtcy50cyIsImltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSHR0cE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuXG5pbXBvcnQgeyBSZXNvdXJjZVByb3ZpZGVycyB9IGZyb20gJy4vc3JjL1Jlc291cmNlUHJvdmlkZXJzJztcblxuZXhwb3J0ICogZnJvbSAnLi9zcmMvUmVzb3VyY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9zcmMvUmVzb3VyY2VBY3Rpb24nO1xuZXhwb3J0ICogZnJvbSAnLi9zcmMvUmVzb3VyY2VDUlVEJztcbmV4cG9ydCAqIGZyb20gJy4vc3JjL1Jlc291cmNlQ1JVREJhc2UnO1xuZXhwb3J0ICogZnJvbSAnLi9zcmMvUmVzb3VyY2VHbG9iYWxDb25maWcnO1xuZXhwb3J0ICogZnJvbSAnLi9zcmMvUmVzb3VyY2VNb2RlbCc7XG5leHBvcnQgKiBmcm9tICcuL3NyYy9SZXNvdXJjZU9EQVRBJztcbmV4cG9ydCAqIGZyb20gJy4vc3JjL1Jlc291cmNlUGFyYW1zJztcbmV4cG9ydCAqIGZyb20gJy4vc3JjL1Jlc291cmNlUHJvdmlkZXJzJztcbmV4cG9ydCAqIGZyb20gJy4vc3JjL0ludGVyZmFjZXMnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBIdHRwTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBSZXNvdXJjZU1vZHVsZSB7XG4gIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogUmVzb3VyY2VNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFJlc291cmNlUHJvdmlkZXJzLnByb3ZpZGVyc1tSZXNvdXJjZVByb3ZpZGVycy5tYWluUHJvdmlkZXJzTmFtZV1cbiAgICB9O1xuICB9XG5cbiAgc3RhdGljIGZvckNoaWxkKHN1YlNldDogc3RyaW5nKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBSZXNvdXJjZU1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogUmVzb3VyY2VQcm92aWRlcnMucHJvdmlkZXJzW3N1YlNldF0gPyBSZXNvdXJjZVByb3ZpZGVycy5wcm92aWRlcnNbc3ViU2V0XSA6IFtdXG4gICAgfTtcbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9pbmRleC50cyIsImV4cG9ydCAqIGZyb20gJy4vaW5kZXgnO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vfi90c2xpbnQtbG9hZGVyIS4vbmd4LXJlc291cmNlLnRzIiwiaW1wb3J0IHsgUmVxdWVzdE1ldGhvZCB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuXG5pbXBvcnQgeyBSZXNvdXJjZSB9IGZyb20gJy4vUmVzb3VyY2UnO1xuaW1wb3J0IHsgUmVzb3VyY2VNZXRob2QgfSBmcm9tICcuL0ludGVyZmFjZXMnO1xuaW1wb3J0IHsgUmVzb3VyY2VBY3Rpb24gfSBmcm9tICcuL1Jlc291cmNlQWN0aW9uJztcblxuZXhwb3J0IGNsYXNzIFJlc291cmNlQ1JVRDxUUXVlcnksIFRTaG9ydCwgVEZ1bGw+IGV4dGVuZHMgUmVzb3VyY2Uge1xuXG4gIEBSZXNvdXJjZUFjdGlvbih7XG4gICAgaXNBcnJheTogdHJ1ZVxuICB9KVxuICBxdWVyeTogUmVzb3VyY2VNZXRob2Q8VFF1ZXJ5LCBUU2hvcnRbXT47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBwYXRoOiAnL3shaWR9J1xuICB9KVxuICBnZXQ6IFJlc291cmNlTWV0aG9kPHsgaWQ6IGFueSB9LCBURnVsbD47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBtZXRob2Q6IFJlcXVlc3RNZXRob2QuUG9zdFxuICB9KVxuICBzYXZlOiBSZXNvdXJjZU1ldGhvZDxURnVsbCwgVEZ1bGw+O1xuXG4gIEBSZXNvdXJjZUFjdGlvbih7XG4gICAgbWV0aG9kOiBSZXF1ZXN0TWV0aG9kLlB1dCxcbiAgICBwYXRoOiAnL3shaWR9J1xuICB9KVxuICB1cGRhdGU6IFJlc291cmNlTWV0aG9kPFRGdWxsLCBURnVsbD47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBtZXRob2Q6IFJlcXVlc3RNZXRob2QuRGVsZXRlLFxuICAgIHBhdGg6ICcveyFpZH0nXG4gIH0pXG4gIHJlbW92ZTogUmVzb3VyY2VNZXRob2Q8eyBpZDogYW55IH0sIGFueT47XG5cbiAgLy8gQWxpYXMgdG8gc2F2ZVxuICBjcmVhdGUoZGF0YTogVEZ1bGwsIGNhbGxiYWNrPzogKHJlczogVEZ1bGwpID0+IGFueSk6IFRGdWxsIHtcbiAgICByZXR1cm4gdGhpcy5zYXZlKGRhdGEsIGNhbGxiYWNrKTtcbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2VDUlVELnRzIiwiaW1wb3J0IHsgUmVxdWVzdE1ldGhvZCB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuXG5pbXBvcnQgeyBSZXNvdXJjZSB9IGZyb20gJy4vUmVzb3VyY2UnO1xuaW1wb3J0IHsgUmVzb3VyY2VNZXRob2QgfSBmcm9tICcuL0ludGVyZmFjZXMnO1xuaW1wb3J0IHsgUmVzb3VyY2VBY3Rpb24gfSBmcm9tICcuL1Jlc291cmNlQWN0aW9uJztcblxuZXhwb3J0IGNsYXNzIFJlc291cmNlQ1JVREJhc2U8VFF1ZXJ5LCBUS2V5cywgVFNob3J0LCBURnVsbD4gZXh0ZW5kcyBSZXNvdXJjZSB7XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBpc0FycmF5OiB0cnVlXG4gIH0pXG4gIHF1ZXJ5OiBSZXNvdXJjZU1ldGhvZDxUUXVlcnksIFRTaG9ydFtdPjtcblxuICBAUmVzb3VyY2VBY3Rpb24oKVxuICBnZXQ6IFJlc291cmNlTWV0aG9kPFRLZXlzLCBURnVsbD47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBtZXRob2Q6IFJlcXVlc3RNZXRob2QuUG9zdFxuICB9KVxuICBzYXZlOiBSZXNvdXJjZU1ldGhvZDxURnVsbCwgVEZ1bGw+O1xuXG4gIEBSZXNvdXJjZUFjdGlvbih7XG4gICAgbWV0aG9kOiBSZXF1ZXN0TWV0aG9kLlB1dFxuICB9KVxuICB1cGRhdGU6IFJlc291cmNlTWV0aG9kPFRGdWxsLCBURnVsbD47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBtZXRob2Q6IFJlcXVlc3RNZXRob2QuRGVsZXRlXG4gIH0pXG4gIHJlbW92ZTogUmVzb3VyY2VNZXRob2Q8VEtleXMsIGFueT47XG5cbiAgLy8gQWxpYXMgdG8gc2F2ZVxuICBjcmVhdGUoZGF0YTogVEZ1bGwsIGNhbGxiYWNrPzogKHJlczogVEZ1bGwpID0+IGFueSk6IFRGdWxsIHtcbiAgICByZXR1cm4gdGhpcy5zYXZlKGRhdGEsIGNhbGxiYWNrKTtcbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2VDUlVEQmFzZS50cyIsImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCB7IFJlc291cmNlIH0gZnJvbSAnLi9SZXNvdXJjZSc7XG5cblxuZXhwb3J0IGNsYXNzIFJlc291cmNlTW9kZWw8Uj4ge1xuXG4gICRyZXNvbHZlZDogYm9vbGVhbjtcbiAgJG9ic2VydmFibGU6IE9ic2VydmFibGU8YW55PjtcbiAgJGFib3J0UmVxdWVzdDogKCkgPT4gdm9pZDtcbiAgJHJlc291cmNlOiBSO1xuXG4gIHB1YmxpYyAkc2V0RGF0YShkYXRhOiBhbnkpIHtcbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIGRhdGEpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgcHVibGljICRzYXZlKCkge1xuXG4gICAgaWYgKHRoaXMuaXNOZXcoKSkge1xuICAgICAgcmV0dXJuIHRoaXMuJGNyZWF0ZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGhpcy4kdXBkYXRlKCk7XG4gICAgfVxuXG4gIH1cblxuICBwdWJsaWMgJGNyZWF0ZSgpIHtcbiAgICByZXR1cm4gdGhpcy4kcmVzb3VyY2VfbWV0aG9kKCdjcmVhdGUnKTtcbiAgfVxuXG4gIHB1YmxpYyAkdXBkYXRlKCkge1xuICAgIHJldHVybiB0aGlzLiRyZXNvdXJjZV9tZXRob2QoJ3VwZGF0ZScpO1xuICB9XG5cbiAgcHVibGljICRyZW1vdmUoKSB7XG4gICAgcmV0dXJuIHRoaXMuJHJlc291cmNlX21ldGhvZCgncmVtb3ZlJyk7XG4gIH1cblxuICBwdWJsaWMgdG9KU09OKCk6YW55IHtcbiAgICByZXR1cm4gUmVzb3VyY2UuJGNsZWFuRGF0YSh0aGlzKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpc05ldygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISg8YW55PnRoaXMpWydpZCddO1xuICB9XG5cbiAgcHJpdmF0ZSAkcmVzb3VyY2VfbWV0aG9kKG1ldGhvZE5hbWU6IHN0cmluZykge1xuXG4gICAgaWYgKCF0aGlzLiRyZXNvdXJjZSkge1xuICAgICAgY29uc29sZS5lcnJvcihgWW91ciBSZXNvdXJjZSBpcyBub3Qgc2V0LiBQbGVhc2UgdXNlIHJlc291cmNlLmNyZWF0ZU1vZGVsKCkgbWV0aG9kIHRvIGNyZWF0ZSBtb2RlbC5gKTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIGlmICghKDxhbnk+dGhpcy4kcmVzb3VyY2UpW21ldGhvZE5hbWVdKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGBZb3VyIFJlc291cmNlIGhhcyBubyBpbXBsZW1lbnRlZCAke21ldGhvZE5hbWV9IG1ldGhvZC5gKTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgICg8YW55PnRoaXMuJHJlc291cmNlKVttZXRob2ROYW1lXSh0aGlzKTtcblxuICAgIHJldHVybiB0aGlzO1xuICB9XG5cblxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2VNb2RlbC50cyIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJlcXVlc3RNZXRob2QgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IFR5cGUgfSBmcm9tICdAYW5ndWxhci9jb3JlL3NyYy90eXBlJztcblxuaW1wb3J0IHsgUmVzb3VyY2UgfSBmcm9tICcuL1Jlc291cmNlJztcbmltcG9ydCB7IFJlc291cmNlTWV0aG9kLCBSZXNvdXJjZVBhcmFtc0Jhc2UgfSBmcm9tICcuL0ludGVyZmFjZXMnO1xuaW1wb3J0IHsgUmVzb3VyY2VBY3Rpb24gfSBmcm9tICcuL1Jlc291cmNlQWN0aW9uJztcbmltcG9ydCB7IFJlc291cmNlUGFyYW1zIH0gZnJvbSAnLi9SZXNvdXJjZVBhcmFtcyc7XG5cbi8qKlxuICogQSBPREFUQSBvYmplY3QgZm9yIHF1ZXJ5aW5nIGVudGl0aWVzLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIElSZXNvdXJjZU9EQVRBUXVlcnkge1xuICAkZmlsdGVyPzogc3RyaW5nO1xuICAkc2VhcmNoPzogc3RyaW5nO1xuICAkZXhwYW5kPzogc3RyaW5nO1xuICAkbGltaXQ/OiBudW1iZXI7XG59XG5cbi8qKiBBIE9EQVRBIG9iamVjdCBmb3IgcXVlcnlpbmcgYSBzaW5nbGUgZW50aXR5LiAqL1xuZXhwb3J0IGludGVyZmFjZSBJUmVzb3VyY2VPREFUQVF1ZXJ5U2luZ2xlIGV4dGVuZHMgSVJlc291cmNlT0RBVEFRdWVyeSB7XG4gIGlkOiBhbnk7XG59XG5cbi8qKiBBIFJlc291cmNlIGJhc2UgY2xhc3MgZm9yIE9EQVRBIGVudGl0aWVzLiBUbyBjcmVhdGUgYSByZXNvdXJjZSBpcyBqdXN0XG4gKiBlbm91Z2ggdG8gZXh0ZW5kIHRoaXMgY2xhc3MgYW5kIGFsbCB0aGUgYmFzZSBPREFUQSBmdW5jdGlvbmFsaXRpZXMgd2lsbCBiZSBwcmVzZW50LlxuICovXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgUmVzb3VyY2VPREFUQTxSPiBleHRlbmRzIFJlc291cmNlIHtcbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBwYXRoOiAnL3shaWR9J1xuICB9KVxuICBnZXQ6IFJlc291cmNlTWV0aG9kPElSZXNvdXJjZU9EQVRBUXVlcnlTaW5nbGUsIFI+O1xuXG4gIEBSZXNvdXJjZUFjdGlvbih7XG4gICAgbWV0aG9kOiBSZXF1ZXN0TWV0aG9kLlBvc3RcbiAgfSlcbiAgc2F2ZTogUmVzb3VyY2VNZXRob2Q8UiwgUj47XG5cbiAgQFJlc291cmNlQWN0aW9uKHtcbiAgICBwYXJhbXM6IHtcbiAgICAgIFwiJGZpbHRlclwiOiBcIkAkZmlsdGVyXCIsXG4gICAgICBcIiRzZWFyY2hcIjogXCJAJHNlYXJjaFwiLFxuICAgICAgXCIkZXhwYW5kXCI6IFwiQCRleHBhbmRcIixcbiAgICAgIFwiJGxpbWl0XCI6IFwiQCRsaW1pdFwiLFxuICAgICAgXCJxdWVyeVwiOiBcIkBxdWVyeVwiXG4gICAgfSxcbiAgICBpc0FycmF5OiB0cnVlXG4gIH0pXG4gIHNlYXJjaDogUmVzb3VyY2VNZXRob2Q8SVJlc291cmNlT0RBVEFRdWVyeSwgUltdPjtcblxuICAkZ2V0VXJsKCk6IHN0cmluZyB8IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuIHN1cGVyLiRnZXRVcmwoKSArIFwiL1wiICsgdGhpcy5nZXRFbnRpdHlTZXROYW1lKCk7XG4gIH1cblxuICBnZXRFbnRpdHlOYW1lKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBwcml2YXRlIGdldEVudGl0eVNldE5hbWUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RW50aXR5TmFtZSgpICsgXCJzXCI7XG4gIH1cbn1cblxuZXhwb3J0IGludGVyZmFjZSBSZXNvdXJjZU9EQVRBUGFyYW1zQmFzZSBleHRlbmRzIFJlc291cmNlUGFyYW1zQmFzZSB7XG4gIC8qKiBUaGUgZW50aXR5IGFzc29jaWF0ZWQgd2l0aCB0aGlzIHJlc291cmNlLiAqL1xuICBlbnRpdHk6IGFueTtcbiAgLyoqIFRoZSBlbnRpdHkgbmFtZSBpbiBjYXNlIGl0IGlzIGRpZmZlcmVudCB0aGFuIHRoZSBlbnRpdHkuXG4gICAqIEl0IGlzIGdvb2QgdG8gc3BlY2lmeSBpdCBhcyB0aGUgZW50aXR5IGNvdWxkIGJlIG1pbmlmaWVkLlxuICAgKi9cbiAgbmFtZT86IHN0cmluZztcbn1cblxuLyoqXG4gKiBBIE9EQVRBIGFubm90YXRpb24gZm9yIGEgcmVzb3VyY2UgZm9yIGEgT0RBVEEgZW50aXR5IHJlc291cmNlIGV4dGVuZGluZyB7QGxpbmsgUmVzb3VyY2VPREFUQX0uXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBSZXNvdXJjZU9EQVRBUGFyYW1zKHBhcmFtczogUmVzb3VyY2VPREFUQVBhcmFtc0Jhc2UpIHtcbiAgY29uc3QgaW5qZWN0YWJsZSA9IEluamVjdGFibGUoKTtcbiAgY29uc3QgenVwZXIgPSBSZXNvdXJjZVBhcmFtcyhwYXJhbXMpO1xuXG4gIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0OiBUeXBlPFJlc291cmNlPikge1xuICAgIGluamVjdGFibGUodGFyZ2V0KTtcbiAgICB6dXBlcih0YXJnZXQpO1xuICAgIHRhcmdldC5wcm90b3R5cGUuZ2V0RW50aXR5TmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChwYXJhbXMubmFtZSkge1xuICAgICAgICByZXR1cm4gcGFyYW1zLm5hbWU7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHlwZW9mIHBhcmFtcy5lbnRpdHkgPT09IFwic3RyaW5nXCIgPyBwYXJhbXMuZW50aXR5IDogcGFyYW1zLmVudGl0eS5uYW1lO1xuICAgIH07XG4gIH07XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9+L3RzbGludC1sb2FkZXIhLi9zcmMvUmVzb3VyY2VPREFUQS50cyIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xM19fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIFwiQGFuZ3VsYXIvY29tbW9uXCJcbi8vIG1vZHVsZSBpZCA9IDEzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xNF9fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIFwicnhqcy9PYnNlcnZhYmxlXCJcbi8vIG1vZHVsZSBpZCA9IDE0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xNV9fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCJcbi8vIG1vZHVsZSBpZCA9IDE1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xNl9fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIlxuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFXzE3X187XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZXh0ZXJuYWwgXCJyeGpzL2FkZC9vcGVyYXRvci9wdWJsaXNoXCJcbi8vIG1vZHVsZSBpZCA9IDE3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJzb3VyY2VSb290IjoiIn0=
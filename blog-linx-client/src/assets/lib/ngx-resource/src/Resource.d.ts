import { Http, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publish';
import { ResourceModel } from './ResourceModel';
import { ResourceActionBase, ResourceParamsBase, ResourceResult } from './Interfaces';
export declare class Resource {
    protected http: Http;
    static $cleanDataFields: string[];
    private $url;
    private $path;
    private $headers;
    private $params;
    private $data;
    constructor(http: Http);
    static $cleanData(obj: ResourceResult<any>): any;
    /**
     * Get main url of the resource
     * @returns {string|Promise<string>}
     */
    $getUrl(methodOptions?: ResourceActionBase): string | Promise<string>;
    /**
     * Set resource url
     * @param url
     */
    $setUrl(url: string): void;
    /**
     * Get path of the resource
     * @returns {string|Promise<string>}
     */
    $getPath(methodOptions?: ResourceActionBase): string | Promise<string>;
    /**
     * Set resource path
     * @param path
     */
    $setPath(path: string): void;
    /**
     * Get headers
     * @returns {any|Promise<any>}
     */
    $getHeaders(methodOptions?: ResourceActionBase): any | Promise<any>;
    /**
     * Set resource headers
     * @param headers
     */
    $setHeaders(headers: any): void;
    /**
     * Get default params
     * @returns {any|Promise<any>|{}}
     */
    $getParams(methodOptions?: ResourceActionBase): any | Promise<any>;
    /**
     * Set default resource params
     * @param params
     */
    $setParams(params: any): void;
    /**
     * Get default data
     * @returns {any|Promise<any>|{}}
     */
    $getData(methodOptions?: ResourceActionBase): any | Promise<any>;
    /**
     * Set default resource params
     * @param data
     */
    $setData(data: any): void;
    /**
     * Create the model
     *
     * @returns {any}
     */
    $createModel(): ResourceModel<any>;
    /**
     * That is called before executing request
     * @param req
     */
    protected $requestInterceptor(req: Request, methodOptions?: ResourceActionBase): Request;
    /**
     * Request observable interceptor
     * @param observable
     * @returns {Observable<any>}
     */
    protected $responseInterceptor(observable: Observable<any>, req: Request, methodOptions?: ResourceActionBase): Observable<any>;
    protected $initResultObject(methodOptions?: ResourceActionBase): any;
    protected $map(item: any): any;
    protected $filter(item: any): boolean;
    protected $getResourceOptions(): ResourceParamsBase;
    protected $request(req: Request, methodOptions?: ResourceActionBase): Observable<any>;
    protected $resourceAction(data: any, params: any, callback: () => any, methodOptions: ResourceActionBase): ResourceResult<any> | ResourceModel<Resource>;
    private $_createReturnData(data, methodOptions);
    private $_initResultObject(methodOptions);
    private $_map(methodOptions);
    private $_filter(methodOptions);
    private $_cleanData(shell);
    private $_fillInternal(shell);
    private $_mainRequest(shell);
    private $_resolveMainOptions(shell);
    private $_releaseMainDeferredSubscriber(shell);
    private $_prepareDataParams(shell);
    private $_prepareBody(shell);
    private $_prepareSearch(shell);
    private $_createRequestOptions(shell);
    private $_createMainObservable(shell, requestObservable);
    private $_setDataToObject(ret, resp);
    private $_getValueForPath(key, params, data, usedPathParams);
    private $_isNullOrUndefined(value);
    private $_appendSearchParams(search, key, value);
    /**
     * Get url to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    private $_getUrl(methodOptions?);
    /**
     * Get get path to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    private $_getPath(methodOptions?);
    /**
     * Get headers to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    private $_getHeaders(methodOptions?);
    /**
     * Get params to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    private $_getParams(methodOptions?);
    /**
     * Get data to be replaced by ResourceParamsBase
     *
     * @param methodOptions
     * @returns { any | Promise<any>}
     * @private
     */
    private $_getData(methodOptions?);
}
